
/****** Object:  Table [dbo].[Incident]    Script Date: 10/11/2015 10:06:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Incident](
	[IncidentId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](200) NULL,
	[Description] [varchar](2000) NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedAt] [datetime] NULL CONSTRAINT [DF_Incident_CreatedAt]  DEFAULT (getdate()),
	[UpdatedBy] [varchar](100) NULL,
	[UpdatedAt] [datetime] NULL CONSTRAINT [DF_Incident_UpdatedAt]  DEFAULT (getdate()),
	[DeletedBy] [varchar](100) NULL,
	[DeletedAt] [datetime] NULL,
 CONSTRAINT [PK_Incident] PRIMARY KEY CLUSTERED 
(
	[IncidentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
