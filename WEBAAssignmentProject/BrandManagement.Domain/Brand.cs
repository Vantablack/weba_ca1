﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WEBAAssignmentProject.CategoryManagement.Domain;

namespace WEBAAssignmentProject.BrandManagement.Domain
{
    public class Brand
    {
        public int BrandId { get; set; }
        public string BrandName { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public string CategoryList { get; set; }
        public bool Deleted { get; set; }
    } // end of Class Brand
} // end of Namespace