﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Ajax.Utilities;
using WEBAAssignmentProject.CategoryManagement.Domain;

namespace WEBAAssignmentProject.BrandManagement.Domain
{
    public class BrandManager
    {
        //NEW
        public int AddOneBrand(Brand inBrand, string inCreatedBy, List<int> inCategoryIdList)
        {
            int returnedId = 0;
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    string insertText = "INSERT INTO Brand (BrandName, CreatedBy, UpdatedBy) " +
                                        " VALUES (@inBrandName, @inCreatedBy,@inUpdatedBy);"
                                        + "SELECT SCOPE_IDENTITY();";
                    string linkingText = "INSERT INTO Category_Brand (FK_CategoryId, FK_BrandId)" +
                                         " VALUES (@inCategoryId, @inReturnedId);";

                    cmd.CommandText = insertText;
                    cmd.Parameters.Add("@inBrandName", SqlDbType.VarChar, 100).Value = inBrand.BrandName;
                    cmd.Parameters.Add("@inCreatedBy", SqlDbType.NVarChar, 128).Value = inCreatedBy;
                    cmd.Parameters.Add("@inUpdatedBy", SqlDbType.NVarChar, 128).Value = inCreatedBy;
                    conn.Open();
                    try
                    {
                        //Execute insertText and get the ID of the Brand
                        returnedId = Int32.Parse(cmd.ExecuteScalar().ToString());
                        cmd.CommandText = linkingText;
                        foreach (int id in inCategoryIdList)
                        {
                            //Foreach category in the Category list, insert into the
                            //Category_Brand table 
                            cmd.Parameters.Clear();
                            cmd.Parameters.Add("@inReturnedId", SqlDbType.Int).Value = returnedId;
                            cmd.Parameters.Add("@inCategoryId", SqlDbType.Int).Value = id;
                            cmd.ExecuteNonQuery();
                        }
                    }
                    catch (SqlException sqlException)
                    {
                        if (sqlException.Message.Contains("UC_BrandName"))
                        {
                            throw new System.ArgumentException("Duplicate brand names found in database");
                        }
                        else throw;
                    }
                    conn.Close();
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
            return returnedId;
        } // end of AddOneBrand

        //NEW
        public bool DeleteOneBrand(int inBrandId, string inOfficerId)
        {
            bool status = false;
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    conn.Open();
                    SqlTransaction transaction = conn.BeginTransaction("transaction");
                    cmd.Connection = conn;
                    cmd.Transaction = transaction;
                    string deleteSqlText = "IF (" +
                                           "(SELECT COUNT(*) AS 'RowCount' FROM Product WHERE FK_BrandId = @inBrandId) = 0" +
                                           ")" +
                                           " BEGIN" +
                                           " UPDATE Brand SET DeleteFlag = 1, DeletedBy = @inDeletedBy, DeletedAt = GETDATE()," +
                                           " UpdatedBy = @inDeletedBy, UpdatedAt = GETDATE() WHERE Brand.BrandId = @inBrandId" +
                                           " DELETE FROM Category_Brand WHERE FK_BrandId = @inBrandId;" +
                                           " END";
                    try
                    {
                        cmd.CommandText = deleteSqlText;
                        cmd.Parameters.Add("@inDeletedBy", SqlDbType.NVarChar).Value = inOfficerId;
                        cmd.Parameters.Add("@inBrandId", SqlDbType.NVarChar).Value = inBrandId;
                        status = (cmd.ExecuteNonQuery() > 0); //true if deletion is successful
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw;
                    }
                    finally
                    {
                        conn.Close();
                    }
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
            return status;
        } // end of DeleteOneBrand

        //NEW
        public bool DestroyOneBrand(int inBrandId)
        {
            bool status = false;
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    string deleteSqlText = "DELETE FROM Brand WHERE BrandId = @inBrandId;";
                    conn.Open();
                    try
                    {
                        cmd.CommandText = deleteSqlText;
                        cmd.Parameters.Add("@inBrandId", SqlDbType.NVarChar).Value = inBrandId;
                        int rowsAffected = cmd.ExecuteNonQuery();
                        status = (rowsAffected == 1);
                    }
                    finally
                    {
                        conn.Close();
                    }
                    return status;
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
        }

        public int AddBrandWithoutImage(Brand inBrand, string inCreatedBy, List<int> inCategoryIdList)
        {
            int returnedId = 0;
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    string sqlText = "INSERT INTO Brand (BrandName, CreatedBy, UpdatedBy) " +
                                     " VALUES (@inBrandName, @inCreatedBy,@inUpdatedBy);"
                                     + "SELECT SCOPE_IDENTITY();";
                    cmd.CommandText = sqlText;
                    cmd.Parameters.Add("@inBrandName", SqlDbType.VarChar, 100).Value = inBrand.BrandName;
                    cmd.Parameters.Add("@inCreatedBy", SqlDbType.NVarChar, 128).Value = inCreatedBy;
                    cmd.Parameters.Add("@inUpdatedBy", SqlDbType.NVarChar, 128).Value = inCreatedBy;
                    conn.Open();
                    try
                    {
                        returnedId = Int32.Parse(cmd.ExecuteScalar().ToString());
                        cmd.CommandText = "INSERT INTO Category_Brand (FK_CategoryId, FK_BrandId)" +
                                          " VALUES (@inCategoryId, @inReturnedId);";
                        foreach (int id in inCategoryIdList)
                        {
                            cmd.Parameters.Clear();
                            cmd.Parameters.Add("@inReturnedId", SqlDbType.Int).Value = returnedId;
                            cmd.Parameters.Add("@inCategoryId", SqlDbType.Int).Value = id;
                            cmd.ExecuteNonQuery();
                        }
                    }
                    catch (SqlException sqlException)
                    {
                        if (sqlException.Message.Contains("UC_BrandName"))
                        {
                            throw new System.ArgumentException("Duplicate brand names found in database");
                        }
                        else throw;
                    }
                    conn.Close();
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
            return returnedId;
        } // end of AddBrandWithoutImage

        //NEW
        public bool UpdateBrandPhoto(BrandPhoto inBrandPhoto, string inUpdatedBy)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    int numOfRecordsAffected = 0;
                    cmd.Connection = conn;
                    //The SQL Insert statement involves one table, Brand. The record to be created requires
                    //4 fields to be filled, BrandName, BrandImage, BrandContentLength, BrandContentType
                    string sqlCommand = "UPDATE BrandPhoto SET";
                    sqlCommand += " PhotoContentLength = @inContentLength, PhotoContentType = @inContentType,";
                    sqlCommand += " PhotoFileName = @inFileName, PhotoImage = @inPhotoImage, UpdatedBy = @inUpdatedBy,";
                    sqlCommand += " UpdatedAt = GETDATE() WHERE FK_BrandId = @inBrandId";

                    cmd.CommandText = sqlCommand;
                    //The SQL statement has 4 parameters, these 4 parameters are provided by the properties which belongs to
                    //the BrandPhoto object which is passed in from the calling 
                    cmd.Parameters.Add("@inContentLength", SqlDbType.Int, 100).Value = inBrandPhoto.PhotoContentLength;
                    cmd.Parameters.Add("@inContentType", SqlDbType.VarChar, 50).Value = inBrandPhoto.PhotoContentType;
                    cmd.Parameters.Add("@inFileName", SqlDbType.VarChar, 200).Value = inBrandPhoto.PhotoFileName;
                    cmd.Parameters.Add("@inPhotoImage", SqlDbType.VarBinary, inBrandPhoto.PhotoImage.Length).Value =
                        inBrandPhoto.PhotoImage;
                    cmd.Parameters.Add("@inUpdatedBy", SqlDbType.NVarChar).Value = inUpdatedBy;
                    cmd.Parameters.Add("@inBrandId", SqlDbType.Int).Value = inBrandPhoto.BrandId;
                    //Open an active connection 
                    conn.Open();
                    //Send the SQL statement to the database
                    try
                    {
                        numOfRecordsAffected = cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.Contains("FK_BrandPhoto_BrandId"))
                        {
                            throw new System.ArgumentException("Unable to insert due to complications in the Brand's ID");
                        }
                        else
                        {
                            throw ex;
                        }
                    }
                    finally
                    {
                        conn.Close();
                    }
                    if (numOfRecordsAffected != 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
        } // end of UpdateBrandPhoto

        //NEW
        public bool AddBrandPhoto(BrandPhoto inBrandPhoto, string inCreatedBy)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    int numOfRecordsAffected = 0;
                    cmd.Connection = conn;
                    //The SQL Insert statement involves one table, Brand. The record to be created requires
                    //4 fields to be filled, BrandName, BrandImage, BrandContentLength, BrandContentType
                    string sqlCommand = "INSERT INTO BrandPhoto";
                    sqlCommand +=
                        " (PhotoContentLength, PhotoContentType, PhotoFileName, PhotoImage, FK_BrandId, CreatedBy, UpdatedBy) ";
                    sqlCommand +=
                        " VALUES (@inContentLength, @inContentType, @inFileName, @inPhotoImage, @inBrandId, @inCreatedBy, @inCreatedBy) ";

                    cmd.CommandText = sqlCommand;
                    //The SQL statement has 4 parameters, these 4 parameters are provided by the properties which belongs to
                    //the BrandPhoto object which is passed in from the calling 
                    cmd.Parameters.Add("@inContentLength", SqlDbType.Int, 100).Value = inBrandPhoto.PhotoContentLength;
                    cmd.Parameters.Add("@inContentType", SqlDbType.VarChar, 50).Value = inBrandPhoto.PhotoContentType;
                    cmd.Parameters.Add("@inFileName", SqlDbType.VarChar, 200).Value = inBrandPhoto.PhotoFileName;
                    cmd.Parameters.Add("@inPhotoImage", SqlDbType.VarBinary, inBrandPhoto.PhotoImage.Length).Value =
                        inBrandPhoto.PhotoImage;
                    cmd.Parameters.Add("@inBrandId", SqlDbType.Int).Value = inBrandPhoto.BrandId;
                    cmd.Parameters.Add("@inCreatedBy", SqlDbType.NVarChar).Value = inCreatedBy;
                    //Open an active connection 
                    conn.Open();
                    //Send the SQL statement to the database
                    try
                    {
                        numOfRecordsAffected = cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.Contains("FK_BrandPhoto_BrandId"))
                        {
                            throw new System.ArgumentException("Unable to insert due to complications in the Brand's ID");
                        }
                        throw ex;
                    }
                    finally
                    {
                        conn.Close();
                    }
                    if (numOfRecordsAffected != 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
        } // end of AddBrandPhoto

        //NEW
        public void UpdateBrandWithoutImage(Brand inBrand, string inUpdatedBy, List<int> inCategoryIdList)
        {
            //Summary
            //[BrandId], [BrandName], [BrandImage], [BrandFileName], [BrandContentLength],
            //[BrandContentType], [CreatedBy], [UpdatedBy], [DeletedBy], [CreatedAt],
            //[UpdatedAt], [DeletedAt], [DeleteFlag]
            //First update the brand table, then delete the Brands from the junction table,
            //then loop through the category list and insert into table, END 
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    string sqlTextUpdate = "UPDATE Brand SET BrandName = @inBrandName,"
                                           + " UpdatedBy = @inUpdatedBy,"
                                           + " UpdatedAt = GETDATE()"
                                           + " WHERE BrandId = @inBrandId;";
                    string sqlTextCleaning = "DELETE FROM Category_Brand WHERE FK_BrandId = @inBrandId";
                    string sqlTextInsert = "INSERT INTO Category_Brand (FK_CategoryId, FK_BrandId)" +
                                           " VALUES (@inCategoryId, @inBrandId);";
                    cmd.CommandText = sqlTextUpdate;
                    cmd.Parameters.Add("@inBrandId", SqlDbType.Int).Value = inBrand.BrandId;
                    cmd.Parameters.Add("@inBrandName", SqlDbType.VarChar, 100).Value = inBrand.BrandName;
                    cmd.Parameters.Add("@inUpdatedBy", SqlDbType.NVarChar, 128).Value = inUpdatedBy;
                    conn.Open();
                    try
                    {
                        //Execute update statement
                        cmd.ExecuteNonQuery();
                        //Change the commandText and clear the Parameters
                        cmd.CommandText = sqlTextCleaning;
                        cmd.Parameters.Clear();
                        //Setting the new Parameters
                        cmd.Parameters.Add("@inBrandId", SqlDbType.Int).Value = inBrand.BrandId;
                        //Execute cleaning code
                        cmd.ExecuteNonQuery();
                        //Change the commandText and clear the Parameters
                        cmd.CommandText = sqlTextInsert;
                        //This part needs a foreach loop to insert
                        foreach (int id in inCategoryIdList)
                        {
                            //Clear and set the new Parameters
                            cmd.Parameters.Clear();
                            cmd.Parameters.Add("@inBrandId", SqlDbType.Int).Value = inBrand.BrandId;
                            cmd.Parameters.Add("@inCategoryId", SqlDbType.Int).Value = id;
                            cmd.ExecuteNonQuery();
                        }
                    }
                    catch (SqlException sqlException)
                    {
                        if (sqlException.Message.Contains("UC_BrandName"))
                        {
                            throw new System.ArgumentException("Duplicate brand names found in database");
                        }
                        else throw;
                    }
                    conn.Close();
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
        } // end of UpdateBrandWithoutImage

        public List<Brand> GetAllBrand(int inDeleteFlag, bool inGetCategories)
        {
            //Gets all brand
            //Input parameters are
            //inDeleteFlag, inGetCategories
            List<Brand> brandList = new List<Brand>();
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    string getAllBrands = "SELECT Brand.BrandId, Brand.BrandName, U1.FullName AS CreatedByName,"
                                          + " U2.FullName AS UpdatedByName, Brand.CreatedAt, Brand.UpdatedAt"
                                          + " FROM Brand"
                                          + " LEFT OUTER JOIN AspNetUsers AS U1 ON Brand.CreatedBy = U1.Id"
                                          + " LEFT OUTER JOIN AspNetUsers AS U2 ON Brand.UpdatedBy = U2.Id"
                                          + " WHERE Brand.DeleteFlag = 0";
                    string getCategories = "SELECT Category.CategoryName, Category.CategoryId"
                                           + " FROM Brand"
                                           + " INNER JOIN Category_Brand ON Brand.BrandId = Category_Brand.FK_BrandId"
                                           +
                                           " INNER JOIN Category ON Category_Brand.FK_CategoryId = Category.CategoryId"
                                           + " WHERE Brand.BrandId = @inBrandId";
                    conn.Open();
                    try
                    {
                        SqlDataAdapter da = new SqlDataAdapter();
                        DataSet ds = new DataSet();
                        da.SelectCommand = cmd;
                        cmd.CommandText = getAllBrands;
                        da.Fill(ds, "BrandData");
                        if (!inGetCategories)
                        {
                            foreach (DataRow row in ds.Tables["BrandData"].Rows)
                            {
                                Brand brand = new Brand();
                                List<String> categoryArrayList = new List<string>();
                                brand.BrandId = Int32.Parse(row["BrandId"].ToString());
                                brand.BrandName = row["BrandName"].ToString();
                                brand.CreatedBy = row["CreatedByName"].ToString();
                                brand.UpdatedBy = row["UpdatedByName"].ToString();
                                brand.CreatedAt = DateTime.Parse(row["CreatedAt"].ToString());
                                brand.UpdatedAt = DateTime.Parse(row["UpdatedAt"].ToString());
                                brandList.Add(brand);
                            } // end of outer foreach
                        }
                        else
                        {
                            ds.Tables.Add("BrandCategoryData");
                            cmd.CommandText = getCategories;
                            foreach (DataRow row in ds.Tables["BrandData"].Rows)
                            {
                                Brand brand = new Brand();
                                List<String> categoryArrayList = new List<string>();
                                brand.BrandId = Int32.Parse(row["BrandId"].ToString());
                                brand.BrandName = row["BrandName"].ToString();
                                brand.CreatedBy = row["CreatedByName"].ToString();
                                brand.UpdatedBy = row["UpdatedByName"].ToString();
                                brand.CreatedAt = DateTime.Parse(row["CreatedAt"].ToString());
                                brand.UpdatedAt = DateTime.Parse(row["UpdatedAt"].ToString());
                                cmd.Parameters.Clear();
                                cmd.Parameters.Add("@inBrandId", SqlDbType.Int).Value = brand.BrandId;
                                ds.Tables["BrandCategoryData"].Clear();
                                da.Fill(ds, "BrandCategoryData");
                                foreach (DataRow dataRow in ds.Tables["BrandCategoryData"].Rows)
                                {
                                    categoryArrayList.Add(dataRow["CategoryName"].ToString());
                                } // end of inner foreach
                                //http://forums.asp.net/t/2017296.aspx?string+builder+with+comma+appending+in+loop+with+datatable
                                brand.CategoryList = string.Join(", ", categoryArrayList.ToArray());
                                brandList.Add(brand);
                            } // end of outer foreach
                        }
                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    conn.Close();
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
            return brandList;
        } // end of GetAllBrand

        public List<object> GetAllBrandNameValue(int inDeleteFlag)
        {
            //Gets all brand
            //Input parameters are
            //inDeleteFlag, inGetCategories
            List<object> brandList = new List<object>();
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    string getAllBrands = "SELECT Brand.BrandId, Brand.BrandName, U1.FullName AS CreatedByName,"
                                          + " U2.FullName AS UpdatedByName, Brand.CreatedAt, Brand.UpdatedAt"
                                          + " FROM Brand"
                                          + " LEFT OUTER JOIN AspNetUsers AS U1 ON Brand.CreatedBy = U1.Id"
                                          + " LEFT OUTER JOIN AspNetUsers AS U2 ON Brand.UpdatedBy = U2.Id"
                                          + " WHERE Brand.DeleteFlag = @inDeleteFlag";
                    conn.Open();
                    try
                    {
                        cmd.Parameters.Add("@inDeleteFlag", SqlDbType.Bit).Value = inDeleteFlag;
                        SqlDataAdapter da = new SqlDataAdapter();
                        DataSet ds = new DataSet();
                        da.SelectCommand = cmd;
                        cmd.CommandText = getAllBrands;
                        da.Fill(ds, "BrandData");
                        foreach (DataRow row in ds.Tables["BrandData"].Rows)
                        {
                            object brand = new
                            {
                                BrandId = Int32.Parse(row["BrandId"].ToString()),
                                BrandName = row["BrandName"].ToString()
                            };
                            brandList.Add(brand);
                        } // end of foreach
                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    conn.Close();
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
            return brandList;
        } // end of GetAllBrandNameValue

        //NEW
        public Brand GetOneBrandById(int inBrandId)
        {
            // Summary:
            Brand brand = new Brand();
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "SELECT * FROM Brand WHERE BrandId = @inBrandId";
                    cmd.Parameters.Add("@inBrandId", SqlDbType.Int).Value = inBrandId;
                    conn.Open();
                    try
                    {
                        SqlDataAdapter da = new SqlDataAdapter();
                        da.SelectCommand = cmd;
                        DataSet ds = new DataSet();
                        da.Fill(ds, "BrandData");
                        DataRow row = ds.Tables["BrandData"].Rows[0];
                        brand.BrandId = Int32.Parse(row["BrandId"].ToString());
                        brand.BrandName = row["BrandName"].ToString();
                        brand.Deleted = (bool)row["DeleteFlag"];
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.Contains("There is no row at position"))
                        {
                            throw new System.ArgumentException("There seems to be no Brand with the specified ID of: " +
                                                               inBrandId);
                        }
                    }
                    finally
                    {
                        conn.Close();
                    }
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
            return brand;
        } // end of GetOneBrandById

        //NEW
        public List<object> GetAllBrandByCategoryIdNameValue(int inDeleteFlag, int inCategoryId)
        {
            List<object> brandList = new List<object>();
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    string sqlText = "SELECT Brand.BrandId, Brand.BrandName, U1.FullName AS CreatedByName,"
                                     + " U2.FullName AS UpdatedByName, Brand.CreatedAt, Brand.UpdatedAt"
                                     + " FROM Brand"
                                     + " LEFT OUTER JOIN AspNetUsers AS U1 ON Brand.CreatedBy = U1.Id"
                                     + " LEFT OUTER JOIN AspNetUsers AS U2 ON Brand.UpdatedBy = U2.Id"
                                     +
                                     " INNER JOIN Category_Brand ON Brand.BrandId = FK_BrandId WHERE FK_CategoryId = @inCategoryId"
                                     + " AND Brand.DeleteFlag = @inDeleteFlag";
                    conn.Open();
                    try
                    {
                        SqlDataAdapter da = new SqlDataAdapter();
                        DataSet ds = new DataSet();
                        da.SelectCommand = cmd;
                        cmd.CommandText = sqlText;
                        cmd.Parameters.Add("@inCategoryId", SqlDbType.Int).Value = inCategoryId;
                        cmd.Parameters.Add("@inDeleteFlag", SqlDbType.Bit).Value = inDeleteFlag;
                        da.Fill(ds, "BrandData");
                        foreach (DataRow dataRow in ds.Tables["BrandData"].Rows)
                        {
                            object brand = new
                            {
                                BrandId = dataRow["BrandId"].ToString(),
                                BrandName = dataRow["BrandName"].ToString()
                            };
                            brandList.Add(brand);
                        }
                    }
                    finally
                    {
                        conn.Close();
                    }
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
            return brandList;
        } // end of GetAllBrandByCategoryIdNameValue

        public List<Brand> GetAllBrandByCategoryId(int inDeleteFlag, int inCategoryId)
        {
            // Summary:
            List<Brand> brandList = new List<Brand>();
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    string sqlText = "SELECT Brand.BrandId, Brand.BrandName, U1.FullName AS CreatedByName,"
                                     + " U2.FullName AS UpdatedByName, Brand.CreatedAt, Brand.UpdatedAt"
                                     + " FROM Brand"
                                     + " LEFT OUTER JOIN AspNetUsers AS U1 ON Brand.CreatedBy = U1.Id"
                                     + " LEFT OUTER JOIN AspNetUsers AS U2 ON Brand.UpdatedBy = U2.Id"
                                     +
                                     " INNER JOIN Category_Brand ON Brand.BrandId = FK_BrandId WHERE FK_CategoryId = @inCategoryId";
                    string sqlText1 = "SELECT Category.CategoryName, Category.CategoryId"
                                      + " FROM Brand"
                                      + " INNER JOIN Category_Brand ON Brand.BrandId = Category_Brand.FK_BrandId"
                                      + " INNER JOIN Category ON Category_Brand.FK_CategoryId = Category.CategoryId"
                                      + " WHERE Brand.BrandId = @inBrandId";
                    conn.Open();
                    try
                    {
                        SqlDataAdapter da = new SqlDataAdapter();
                        DataSet ds = new DataSet();
                        da.SelectCommand = cmd;
                        cmd.CommandText = sqlText;
                        cmd.Parameters.Add("@inCategoryId", SqlDbType.Int).Value = inCategoryId;
                        da.Fill(ds, "BrandData");
                        ds.Tables.Add("BrandCategoryData");
                        cmd.CommandText = sqlText1;
                        foreach (DataRow row in ds.Tables["BrandData"].Rows)
                        {
                            Brand brand = new Brand();
                            List<String> categoryArrayList = new List<string>();
                            brand.BrandId = Int32.Parse(row["BrandId"].ToString());
                            brand.BrandName = row["BrandName"].ToString();
                            brand.CreatedBy = row["CreatedByName"].ToString();
                            brand.UpdatedBy = row["UpdatedByName"].ToString();
                            brand.CreatedAt = DateTime.Parse(row["CreatedAt"].ToString());
                            brand.UpdatedAt = DateTime.Parse(row["UpdatedAt"].ToString());
                            cmd.Parameters.Clear();
                            cmd.Parameters.Add("@inBrandId", SqlDbType.Int).Value = brand.BrandId;
                            ds.Tables["BrandCategoryData"].Clear();
                            da.Fill(ds, "BrandCategoryData");
                            foreach (DataRow dataRow in ds.Tables["BrandCategoryData"].Rows)
                            {
                                categoryArrayList.Add(dataRow["CategoryName"].ToString());
                            } // end of inner foreach
                            //http://forums.asp.net/t/2017296.aspx?string+builder+with+comma+appending+in+loop+with+datatable
                            brand.CategoryList = string.Join(", ", categoryArrayList.ToArray());
                            brandList.Add(brand);
                        } // end of outer foreach
                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    conn.Close();
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
            return brandList;
        } // end of GetAllBrandByCategoryId

        public List<Brand> GetAllBrandByCategoryIdMinimal(int inCategoryId)
        {
            // Summary:
            List<Brand> brandList = new List<Brand>();
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    string sqlText = "SELECT Brand.BrandId, Brand.BrandName FROM Brand"
                                     + " INNER JOIN Category_Brand ON Brand.BrandId = Category_Brand.FK_BrandId"
                                     + " WHERE FK_CategoryId = @inCategoryId";
                    conn.Open();
                    try
                    {
                        SqlDataAdapter da = new SqlDataAdapter();
                        DataSet ds = new DataSet();
                        da.SelectCommand = cmd;
                        cmd.CommandText = sqlText;
                        cmd.Parameters.Add("@inCategoryId", SqlDbType.Int).Value = inCategoryId;
                        da.Fill(ds, "BrandData");
                        foreach (DataRow row in ds.Tables["BrandData"].Rows)
                        {
                            Brand brand = new Brand();
                            brand.BrandId = Int32.Parse(row["BrandId"].ToString());
                            brand.BrandName = row["BrandName"].ToString();
                            brandList.Add(brand);
                        } // end of foreach
                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    conn.Close();
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
            return brandList;
        } // end of GetAllBrandByCategoryIdMinimal

        public List<Brand> GetAllBrandBySearch(string inSearchString)
        {
            // Summary:
            List<Brand> brandList = new List<Brand>();
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    string sqlText = "SELECT Brand.BrandId, Brand.BrandName, U1.FullName AS CreatedByName,"
                                     + " U2.FullName AS UpdatedByName, Brand.CreatedAt, Brand.UpdatedAt"
                                     + " FROM Brand"
                                     + " LEFT OUTER JOIN AspNetUsers AS U1 ON Brand.CreatedBy = U1.Id"
                                     + " LEFT OUTER JOIN AspNetUsers AS U2 ON Brand.UpdatedBy = U2.Id"
                                     + " WHERE Brand.BrandName LIKE @inSearchString";
                    string sqlText1 = "SELECT Category.CategoryName, Category.CategoryId"
                                      + " FROM Brand"
                                      + " INNER JOIN Category_Brand ON Brand.BrandId = Category_Brand.FK_BrandId"
                                      + " INNER JOIN Category ON Category_Brand.FK_CategoryId = Category.CategoryId"
                                      + " WHERE Brand.BrandId = @inBrandId";
                    conn.Open();
                    try
                    {
                        SqlDataAdapter da = new SqlDataAdapter();
                        DataSet ds = new DataSet();
                        da.SelectCommand = cmd;
                        cmd.CommandText = sqlText;
                        cmd.Parameters.Add("@inSearchString", SqlDbType.VarChar).Value = "%" + inSearchString + "%";
                        da.Fill(ds, "BrandData");
                        ds.Tables.Add("BrandCategoryData");
                        cmd.CommandText = sqlText1;
                        foreach (DataRow row in ds.Tables["BrandData"].Rows)
                        {
                            Brand brand = new Brand();
                            List<String> categoryArrayList = new List<string>();
                            brand.BrandId = Int32.Parse(row["BrandId"].ToString());
                            brand.BrandName = row["BrandName"].ToString();
                            brand.CreatedBy = row["CreatedByName"].ToString();
                            brand.UpdatedBy = row["UpdatedByName"].ToString();
                            brand.CreatedAt = DateTime.Parse(row["CreatedAt"].ToString());
                            brand.UpdatedAt = DateTime.Parse(row["UpdatedAt"].ToString());
                            cmd.Parameters.Clear();
                            cmd.Parameters.Add("@inBrandId", SqlDbType.Int).Value = brand.BrandId;
                            ds.Tables["BrandCategoryData"].Clear();
                            da.Fill(ds, "BrandCategoryData");
                            foreach (DataRow dataRow in ds.Tables["BrandCategoryData"].Rows)
                            {
                                categoryArrayList.Add(dataRow["CategoryName"].ToString());
                            } // end of inner foreach
                            //http://forums.asp.net/t/2017296.aspx?string+builder+with+comma+appending+in+loop+with+datatable
                            brand.CategoryList = string.Join(", ", categoryArrayList.ToArray());
                            brandList.Add(brand);
                        } // end of outer foreach
                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    conn.Close();
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
            return brandList;
        } // end of GetAllBrandBySearch

        //NEW
        public List<Category> GetCategoriesOfBrand(int inBrandId)
        {
            List<Category> categoryList = new List<Category>();
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "SELECT Category.CategoryId, Category.CategoryName"
                                      + " FROM Brand"
                                      + " INNER JOIN Category_Brand ON Brand.BrandId = Category_Brand.FK_BrandId"
                                      + " INNER JOIN Category ON Category_Brand.FK_CategoryId = Category.CategoryId"
                                      + " WHERE Brand.BrandId = @inBrandId";
                    cmd.Parameters.Add("@inBrandId", SqlDbType.Int).Value = inBrandId;
                    conn.Open();
                    try
                    {
                        SqlDataAdapter da = new SqlDataAdapter();
                        da.SelectCommand = cmd;
                        DataSet ds = new DataSet();
                        da.Fill(ds, "BrandCategoryData");
                        foreach (DataRow dataRow in ds.Tables["BrandCategoryData"].Rows)
                        {
                            Category category = new Category();
                            category.CategoryId = Int32.Parse(dataRow["CategoryId"].ToString());
                            category.CategoryName = dataRow["CategoryName"].ToString();
                            categoryList.Add(category);
                        }
                    }
                    catch (SqlException ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        conn.Close();
                    }
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
            return categoryList;
        } // end of GetCategoriesOfBrand

        public List<Brand> TestGetAllBrands(bool inDeleteFlag, int inCategoryId, string inSearchField)
        {
            // Summary:
            List<Brand> brandList = new List<Brand>();
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;

                    string innerJoinSqlText = " INNER JOIN Category_Brand ON Brand.BrandId = FK_BrandId WHERE FK_CategoryId = @inCategoryId";
                    string deleteFlagSqlText = " WHERE Brand.DeleteFlag = @inDeleteFlag";
                    string searchFieldSqlText = " AND Brand.BrandName LIKE @inSearchField";
                    string sqlText = "SELECT Brand.BrandId, Brand.BrandName, U1.FullName AS CreatedByName,"
                                     + " U2.FullName AS UpdatedByName, Brand.CreatedAt, Brand.UpdatedAt"
                                     + " FROM Brand"
                                     + " LEFT OUTER JOIN AspNetUsers AS U1 ON Brand.CreatedBy = U1.Id"
                                     + " LEFT OUTER JOIN AspNetUsers AS U2 ON Brand.UpdatedBy = U2.Id";
                    if (inCategoryId != 0)
                    {
                        sqlText += innerJoinSqlText;
                        cmd.Parameters.Add("@inCategoryId", SqlDbType.Int).Value = inCategoryId;
                        sqlText += " AND Brand.DeleteFlag = @inDeleteFlag";
                    }
                    else
                    {
                        sqlText += deleteFlagSqlText;
                    }
                    cmd.Parameters.Add("@inDeleteFlag", SqlDbType.Bit).Value = inDeleteFlag;
                    if (!inSearchField.IsNullOrWhiteSpace())
                    {
                        sqlText += searchFieldSqlText;
                        cmd.Parameters.Add("@inSearchField", SqlDbType.VarChar).Value = "%" + inSearchField + "%";
                    }
                    string categoriesSqlText = "SELECT Category.CategoryName, Category.CategoryId"
                                      + " FROM Brand"
                                      + " INNER JOIN Category_Brand ON Brand.BrandId = Category_Brand.FK_BrandId"
                                      + " INNER JOIN Category ON Category_Brand.FK_CategoryId = Category.CategoryId"
                                      + " WHERE Brand.BrandId = @inBrandId";
                    conn.Open();
                    try
                    {
                        SqlDataAdapter da = new SqlDataAdapter();
                        DataSet ds = new DataSet();
                        da.SelectCommand = cmd;
                        cmd.CommandText = sqlText;
                        da.Fill(ds, "BrandData");
                        ds.Tables.Add("BrandCategoryData");
                        cmd.CommandText = categoriesSqlText;
                        foreach (DataRow row in ds.Tables["BrandData"].Rows)
                        {
                            Brand brand = new Brand();
                            List<String> categoryArrayList = new List<string>();
                            brand.BrandId = Int32.Parse(row["BrandId"].ToString());
                            brand.BrandName = row["BrandName"].ToString();
                            brand.CreatedBy = row["CreatedByName"].ToString();
                            brand.UpdatedBy = row["UpdatedByName"].ToString();
                            brand.CreatedAt = DateTime.Parse(row["CreatedAt"].ToString());
                            brand.UpdatedAt = DateTime.Parse(row["UpdatedAt"].ToString());
                            cmd.Parameters.Clear();
                            cmd.Parameters.Add("@inBrandId", SqlDbType.Int).Value = brand.BrandId;
                            ds.Tables["BrandCategoryData"].Clear();
                            da.Fill(ds, "BrandCategoryData");
                            foreach (DataRow dataRow in ds.Tables["BrandCategoryData"].Rows)
                            {
                                categoryArrayList.Add(dataRow["CategoryName"].ToString());
                            } // end of inner foreach
                            //http://forums.asp.net/t/2017296.aspx?string+builder+with+comma+appending+in+loop+with+datatable
                            brand.CategoryList = string.Join(", ", categoryArrayList.ToArray());
                            brandList.Add(brand);
                        } // end of outer foreach
                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                    finally
                    {
                        conn.Close();
                    }
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
            return brandList;
        } // end of TestGetAllBrands

        public bool UndeleteOneBrand(int inBrandId, string inUpdatedBy)
        {
            // Summary:
            //before marking as deleted
            bool status = false;
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    conn.Open();
                    // Start a local transaction.
                    SqlTransaction transaction = conn.BeginTransaction("transaction");
                    cmd.Connection = conn;
                    cmd.Transaction = transaction;

                    string checkAndDeleteText =
                        "UPDATE Brand SET DeleteFlag = 0, DeletedAt = NULL, DeletedBy = NULL, UpdatedBy = @inUpdatedBy, UpdatedAt = GETDATE()" +
                        " WHERE BrandId = @inBrandId";

                    try
                    {
                        cmd.CommandText = checkAndDeleteText;
                        cmd.Parameters.Add("@inBrandId", SqlDbType.Int).Value = inBrandId;
                        cmd.Parameters.Add("@inUpdatedBy", SqlDbType.NVarChar).Value = inUpdatedBy;

                        int rowsAffected = cmd.ExecuteNonQuery();
                        status = (rowsAffected > 0);
                        //No errors, commit the database
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw;
                    }
                    finally
                    {
                        conn.Close();
                    }
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
            return status;
        } // end of UndeleteOneCategory
    } // end of class BrandManager
} // end of Namespaces