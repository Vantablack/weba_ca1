﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEBAAssignmentProject.BrandManagement.Domain
{
    public class BrandPhoto
    {
        public int BrandPhotoId { get; set; }
        public byte[] PhotoImage { get; set; }
        public int PhotoContentLength { get; set; }
        public string PhotoContentType { get; set; }
        public string PhotoFileName { get; set; }
        public int BrandId { get; set; }
    }
}