﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WEBAAssignmentProject.Startup))]
namespace WEBAAssignmentProject
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
