﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UpdateBrand.aspx.cs" Inherits="WEBAAssignmentProject.Officer.UpdateBrand" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Update Brand</title>
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <link href="../Content/bootstrap-fileinput/css/fileinput.min.css" rel="stylesheet" />
    <link href="../Content/bootstrap-multiselect.css" rel="stylesheet" />
</head>
<body>
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <a href="AdminPanel.aspx" class="btn btn-default navbar-btn"><span class="glyphicon glyphicon-home"></span>
                    Back to AdminPanel</a>
                <a href="BrandList.aspx" class="btn btn-default navbar-btn"><span class="glyphicon glyphicon-chevron-left"></span>
                    Back to BrandList</a>
            </div>
        </nav>
        <div class="col-md-12">
            <div class="page-header">
                <h1>Update Brand</h1>
            </div>
            <div class="col-md-12">
                <div id="messageDiv" class=""></div>
            </div>
            <form class="form-horizontal" id="updateBrandForm">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Brand Name</label>
                    <div class="col-sm-6">
                        <input name="brandNameTxtBox" type="text" id="brandNameTxtBox" class="form-control" placeholder="Name of brand" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Brand Image</label>
                    <div class="col-sm-6">
                        <input type="file" name="imageFileInput" id="imageFileInput" />
                        <p class="bg-warning">NOTE: If no image is selected, the previous image will be retained.</p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Category</label>
                    <div class="col-sm-6">
                        <select id="checkboxMultiSelect" name="checkboxMultiSelect" multiple="multiple"></select>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="col-md-2 col-md-offset-2">
                        <button id="deleteButton" type="button" class="btn btn-default">
                            <span class="glyphicon glyphicon-trash"></span>&nbsp;Delete
                        </button>
                    </div>
                    <div class="col-md-2 col-md-offset-4">
                        <a href="BrandList.aspx" class="btn btn-danger">Cancel</a>
                        <button id="submitButton" class="btn btn-success">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script src="../Scripts/jquery-2.1.4.min.js"></script>
    <script src="../Scripts/bootstrap.min.js"></script>
    <script src="../Scripts/jquery.validate.min.js"></script>
    <script src="../Scripts/fileinput.min.js"></script>
    <script src="../Scripts/bootstrap-multiselect.js"></script>
    <script>
        //VARIABLES
        window.BrandId = getUrlValues()['id'];
        window.$updateBrandForm = $("#updateBrandForm");
        window.$submitButton = $("#submitButton");
        window.$brandNameInput = $("#brandNameTxtBox");
        window.$checkboxDiv = $("#checkboxDiv");
        window.$messageDiv = $("#messageDiv");
        window.$imageInput = $("#imageFileInput");
        window.$deleteButton = $("#deleteButton");
        window.doUpdateImage = false;
        window.chkBoxesArray = new Array();
        //Javascript class
        function WebFormData(inBrandName, inChkBoxes, inBrandId) {
            this.BrandId = inBrandId;
            this.BrandName = inBrandName;
            this.Categories = inChkBoxes;
        }

        $(document).ready(function () {
            if (window.BrandId === undefined) {
                window.location.href = "BrandList.aspx";
            }
            //This function will populate the checkboxes and fill the data for the Brand
            FillCategoryDropDown();
            GetAndFillBrandDetails();

            //Setting up fileinput
            window.$imageInput.fileinput({
                uploadUrl: 'UpdateBrandImageHandler.ashx',
                overwriteInitial: true,
                showUpload: false,
                previewFileType: 'image',
                uploadAsync: false,
                maxFileCount: 1,
                type: 'POST',
                showCaption: true,
                previewFiletype: 'image',
                allowedPreviewTypes: 'image',
                previewFileIcon: '<i class="glyphicon glyphicon-question-sign"></i>',
                uploadExtraData: function () {
                    // callback example
                    var out = {};
                    out['BrandId'] = window.BrandId; //Must send this brand id information too.
                    console.log('This is the out object');
                    console.log(out);
                    return out;
                },
                initialPreview: [
                    '<img class="file-preview-image" src=\"GetBrandImageHandler.ashx?id=' + window.BrandId + '\" >'
                ],
                initialCaption: "Previous image."
            });

            //Events handler
            window.$submitButton.on('click', function (event) {
                $('#updateBrandForm').valid();
            });

            window.$deleteButton.on("click", function () {
                $.ajax({
                    url: 'UpdateBrand.aspx/DeleteOneBrand',
                    type: 'POST',
                    data: JSON.stringify({ inBrandId: window.BrandId }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'JSON',
                    async: false
                }).done(function (data, textStatus, jqXHR) {
                    var responseObject = data.d;
                    var $button = $('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
                    var $clone;
                    if (responseObject.Status === 'success') {
                        $clone = window.$messageDiv.clone().removeAttr("id").removeClass()
                            .addClass("alert alert-success alert-dismissible fade in").empty();
                        var $header = $("<h4><span class='glyphicon glyphicon-thumbs-up'></span>&nbsp;Success</h4>");
                        var $message = $("<p></p>").prepend($header).append(responseObject.Message);
                        $clone.append($button).append($message).alert();
                    } else {
                        $clone = window.$messageDiv.clone().removeAttr("id").removeClass()
                            .addClass("alert alert-danger alert-dismissible fade in").empty();
                        $clone.append($button).alert();
                        var $header = $("<h4><span class='glyphicon glyphicon-thumbs-down'></span>&nbsp;Oh no!</h4>");
                        var $message = $("<p></p>").prepend($header).append(responseObject.Message);
                        $clone.append($message);

                        //If there are data inside the object
                        if (responseObject.Data !== undefined) {
                            var responseData = JSON.parse(responseObject.Data);
                            var $prodMessage = $("<p>Products affected are:</p>");
                            $clone.append($prodMessage);
                            var $prodPara = $("<p></p>");
                            for (index = 0; index < responseData.length; index++) {
                                var $linkButton = $("<a />").attr({ href: "#", "class": "btn btn-default" }).text(responseData[index].ProductName);
                                $prodPara.append("&nbsp;");
                                $prodPara.append($linkButton);
                            }
                            $clone.append($prodPara);
                        } // end of if (responseObject.Data !== undefined)
                    } // end of if (responseObject.Status === 'success')
                    $clone.insertAfter(window.$messageDiv);
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    var $button = $('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>');

                    var $clone = window.$messageDiv.clone().removeAttr("id").removeClass()
                        .addClass("alert alert-danger alert-dismissible fade in").empty();
                    var $header = $("<h4><span class='glyphicon glyphicon-thumbs-down'></span>&nbsp;Oh no!</h4>");
                    var $message = $("<p></p>").prepend($header).append(errorThrown);
                    $clone.append($button).append($message).alert();
                    $clone.insertAfter(window.$messageDiv);
                    console.log("Fail:");
                    console.log(jqXHR);
                    console.log(textStatus);
                    console.log(errorThrown);
                }); // end of  $.ajax(...);
            });

            window.$imageInput.change(function () {
                console.log("Imageinput changed, doUpdateImage set to true");
                window.doUpdateImage = true;
            });

            //Event handler
            $submitButton.on('click', function (event) {
                window.$updateBrandForm.valid();
            });

            //Setting up file validation
            //for uploading, if the image input is not changed, the validation will skip the input and proceed with the insert statement
            window.$updateBrandForm.validate({
                ignore: ":hidden:not(#checkboxMultiSelect)",
                debug: true,
                errorClass: "text-danger bg-danger",
                validClass: "bg-success",
                submitHandler: function (form) {
                    GetBrandDataAndSave();
                },
                highlight: function (element, errorClass, validClass) {
                    $(element.closest("div.form-group")).addClass("has-error");
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element.closest("div.form-group")).removeClass("has-error").addClass("has-success");
                },
                errorPlacement: function (error, element) {
                    var $inputgroup = element.closest('div.input-group');
                    console.log($inputgroup);
                    if ($inputgroup.length === 1) {
                        error.insertAfter($inputgroup);
                    } else {
                        error.insertAfter(element);
                    }
                },
                rules: {
                    brandNameTxtBox: {
                        required: true,
                        minlength: 1
                    },
                    imageFileInput: {
                        ImageOnly: true
                    },
                    checkboxMultiSelect: {
                        required: true
                    }
                },
                messages: {
                    brandNameTxtBox: {
                        required: "Brand name is required.",
                        minlength: jQuery.validator.format("At least {0} characters are needed")
                    }
                }
            }); // end of window.$updateBrandForm.validate(...);

            $.validator.addMethod("ImageOnly", function (value, element) {
                //http://stackoverflow.com/questions/13093971/what-does-this-optionalelement-do-when-adding-a-jquery-validation-method
                return this.optional(element) || (/\.(gif|jpeg|jpg|png|tiff)$/i).test(value);
            });
        }); // end of $(document).ready(...)


        //-------------FUNCTIONS------------------
        //Reference: http://stackoverflow.com/questions/4656843/jquery-get-querystring-from-url
        function getUrlValues() {
            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        } // end of getUrlValues

        function GetAndFillBrandDetails() {
            //AJAX function to fill form with data
            $.ajax({
                url: "UpdateBrand.aspx/GetBrandData",
                type: "POST",
                async: false,
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",
                data: "{inBrandId: " + window.BrandId + "}"
            }).done(function (data, textStatus, jqXHR) {
                var responseObject = data.d;
                if (responseObject.Status === "success") {
                    var responseData = JSON.parse(responseObject.Data);
                    console.log(responseData);
                    window.$brandNameInput.val(responseData.Brand.BrandName);

                    //console.log($inputs);
                    for (index = 0; index < responseData.Categories.length; index++) {
                        ////http://stackoverflow.com/questions/6439859/how-to-select-an-input-by-value-with-jquery-1-4-3-and-higher
                        //var $returnedInput = $inputs.filter(function () { return this.value === String(responseData.Categories[index].CategoryId) });
                        ////http://stackoverflow.com/questions/426258/checking-a-checkbox-with-jquery
                        //$returnedInput.prop("checked", true);

                        $("#checkboxMultiSelect").multiselect("select", responseData.Categories[index].CategoryId);
                        //console.log($("#checkboxMultiSelect option[value='" + responseData.Categories[index].CategoryId + "']"));
                    }
                    $("#checkboxMultiSelect").multiselect("refresh");
                    if (responseData.Brand.Deleted) {
                        var $cloneButton = $("#deleteButton").clone().removeAttr("id").html("&nbsp;Restore");
                        var $span = $("#deleteButton").find("span").removeClass().addClass("glyphicon glyphicon-repeat");
                        $cloneButton.prepend($span);
                        $cloneButton.off("click").on("click", function () {
                            console.log("clone click");
                            if ($("#checkboxMultiSelect").val() !== null) {
                                $.ajax({
                                    url: "UpdateBrand.aspx/RestoreOneBrand",
                                    type: "POST",
                                    async: false,
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "JSON",
                                    statusCode: {
                                        500: function () {
                                            alert("Internal Server Error");
                                        }
                                    },
                                    data: JSON.stringify({ inBrandId: window.BrandId })
                                }).done(function (data, textStatus, jqXHR) {
                                    location.reload(true);
                                }).fail(function (jqXHR, textStatus, errorThrown) {
                                    //console.log(jqXHR);
                                    window.$messageDiv.removeClass().addClass("alert alert-danger").html("Unable to restore Brand: " + errorThrown);
                                }); // end of $.ajax(...)
                            } else {
                                window.$updateBrandForm.valid();
                            }

                            //http://api.jquery.com/ajaxComplete/
                            $(document).ajaxComplete(function (event, xhr, settings) {
                                //Make sure that the ajax to update one brand is completed calling first,
                                //then restore the Brand
                                if (settings.url === "UpdateBrand.aspx/UpdateOneBrand") {
                                    console.log("update complete");
                                    $.ajax({
                                        url: "UpdateBrand.aspx/RestoreOneBrand",
                                        type: "POST",
                                        async: false,
                                        contentType: "application/json; charset=utf-8",
                                        dataType: "JSON",
                                        statusCode: {
                                            500: function () {
                                                alert("Internal Server Error");
                                            }
                                        },
                                        data: JSON.stringify({ inBrandId: window.BrandId })
                                    }).done(function (data, textStatus, jqXHR) {
                                        location.reload(true);
                                    }).fail(function (jqXHR, textStatus, errorThrown) {
                                        //console.log(jqXHR);
                                        window.$messageDiv.removeClass().addClass("alert alert-danger").html("Unable to restore Brand: " + errorThrown);
                                    }); // end of $.ajax(...)
                                }
                            });
                        });
                        $("#deleteButton").replaceWith($cloneButton);
                    }
                } else {
                    window.$messageDiv.empty().removeClass().addClass("alert alert-danger").append(responseObject.Message);
                    window.$submitButton.prop("disabled", true);
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                window.$messageDiv.empty().removeClass().addClass("alert alert-danger").append("Unable to get Brand details. Please refresh page. If problem persists, check console. <br /><ins>" + jqXHR.responseJSON.Message + "</ins>")
                window.$submitButton.prop("disabled", true);
            }); // end of $.ajax(...);
        } // end of GetAndFillBrandDetails

        function GetBrandDataAndSave() {
            //Collect data from the respective form elements
            var newBrandName = window.$brandNameInput.val();
            var categories = $("#checkboxMultiSelect").val();
            var webFormData = new WebFormData(newBrandName, categories, window.BrandId);
            console.log('This is the webFormData text');
            console.log(webFormData);
            $.ajax(
            {
                type: 'POST',
                url: 'UpdateBrand.aspx/UpdateOneBrand',
                data: JSON.stringify({ inWebFormData: JSON.stringify(webFormData) }),
                //If single quote is removed from webFormDataInJason, you are passing the data 
                //as an object. The server side web method need to declare an object parameter.
                //data: "{'WebFormData' : " + studentDataInJson + "}",
                contentType: 'application/json; charset=utf-8',
                dataType: 'JSON',
                async: false,
                success: function (data, textStatus, jqXHR) {
                    var responseObject = data.d;
                    if (responseObject.Status === 'success') {
                        $messageDiv.text(responseObject.Message);
                        $messageDiv.addClass('alert alert-success');
                    } else {
                        $messageDiv.text('Error : ' + responseObject.message);
                        $messageDiv.removeClass("alert alert-success");
                        $messageDiv.addClass('alert alert-danger');
                    }
                },
                error: function (xhr, status, errorThrown) {
                    //I copied these error: complete: and success: from http://learn.jquery.com/ajax/jquery-ajax-methods/
                    //.These are called settings. Ajax() method has a lot of settings.
                    //Learn to use console.log
                    //console.log('Error: ' + errorThrown);
                    //console.log('Status: ' + status);
                    $messageDiv.text('Something happened. Message : ' + errorThrown);
                    $messageDiv.removeClass("alert-success");
                    $messageDiv.addClass('alert alert-danger');
                }
            }).done(function () {
                //If there is any server error, this section will still execute.
                //Therefore, need to have an if statement block to control whether the client logic
                //can upload photo. I used the BrandId variable to check.
                if (window.BrandId != '') {
                    //Reference: http://stackoverflow.com/questions/11448011/jquery-ajax-methods-async-option-deprecated-what-now
                    if (window.doUpdateImage) {
                        window.$imageInput.fileinput('uploadBatch');
                        window.$imageInput.on('fileuploaderror', function (event, data, previewId, index) {
                            //on file upload error, delete the newly created Brand
                            var form = data.form,
                                files = data.files,
                                extra = data.extra,
                                response = data.response,
                                reader = data.reader;
                            console.log('File upload error');
                        });
                    } //end of if (doUpdateImage)
                }
                //Placed this line of code here so that I can notice that this section was executed.
                console.log('ajax().done() section was executed');
            }); //end of $.ajax(...)
        } // end of getBrandDataAndSave

        //Function to fill category
        function FillCategoryDropDown() {
            //AJAX function to populate category checkbox
            $.ajax({
                url: "UpdateBrand.aspx/GetAllCategories",
                type: "POST",
                async: false,
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",
                statusCode: {
                    500: function () {
                        alert("Internal Server Error");
                    }
                }
            }).done(function (data, textStatus, jqXHR) {
                var responseObject = data.d;
                if (responseObject.Status === "success") {
                    var categoryList = JSON.parse(responseObject.Data);
                    categoryList = SortJSON(categoryList, "CategoryName", "ASC");
                    var $checkboxMultiSelect = $("#checkboxMultiSelect");
                    for (index = 0; index < categoryList.length; index++) {
                        var $option = $("<option></option>").val(categoryList[index].CategoryId).text(categoryList[index].CategoryName);
                        $option.appendTo($checkboxMultiSelect);
                    }
                    $checkboxMultiSelect.multiselect();
                } else {
                    window.$messageDiv.empty().removeClass().addClass("alert alert-danger").append(responseObject.Message);
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                window.$messageDiv.empty().append("Unable to get Categories. Please refresh page. If problem persists, check console. " + errorThrown)
                    .removeClass().addClass("alert alert-danger");
                window.$submitButton.prop("disabled", true);
            }); // end of $.ajax(...);
        } // end of FillCategoryDropDown

        function SortJSON(jsonObject, name, order) {
            //http://stackoverflow.com/questions/881510/sorting-json-by-values
            jsonObject = jsonObject.sort(function (a, b) {
                if (order === "ASC") return (a[name] > b[name]) ? 1 : ((a[name] < b[name]) ? -1 : 0);
                else return (b[name] > a[name]) ? 1 : ((b[name] < a[name]) ? -1 : 0);
            });
            return jsonObject;
        } // end of SortJSON
    </script>
</body>
</html>
