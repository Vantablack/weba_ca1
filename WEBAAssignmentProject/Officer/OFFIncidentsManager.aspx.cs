﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using System.Security.Claims;
using WEBAAssignmentProject.IncidentManagement.Domain;

namespace WEBAAssignmentProject.Officer
{
    public partial class OFFIncidentsManager : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            IncidentManager incidentManager = new IncidentManager();
            dataGrid.DataSource = incidentManager.GetIncidentsByUserId(User.Identity.GetUserId());
            dataGrid.DataBind();
        }
    }
}