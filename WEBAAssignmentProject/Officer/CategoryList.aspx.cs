﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices.Internal;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using WEBAAssignmentProject.CategoryManagement.Domain;

namespace WEBAAssignmentProject.Officer
{
    public partial class CategoryList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        } // end of Page_Load

        [WebMethod]
        public static object GetAllVisibleCategory()
        {
            //Gets all the rootCategory with default values
            //Template for responseObject
            //var responseObject = new { Status = "success/fail", Message = "", Data = "{JSON}"};
            CategoryManager categoryManager = new CategoryManager();
            try
            {
                var categoryList = categoryManager.GetAllCategories(0, 3);
                var responseObject = new
                {
                    Status = "success",
                    Message = "Successfully fetched Categories with parameter (0)",
                    Data = JsonConvert.SerializeObject(categoryList, Formatting.None, new JsonSerializerSettings
                    {
                        DateTimeZoneHandling = DateTimeZoneHandling.Local
                    })
                };
                return responseObject;
            }
            catch (Exception ex)
            {
                var responseObject = new
                {
                    Status = "fail",
                    Message = "Error occured: " + ex.Message
                };
                return responseObject;
            }
        } // end of GetAllVisibleCategory

        [WebMethod]
        public static object GetAllVisibleCategoryFiltered(int inVisibility)
        {
            //Gets all categories with the visibility type
            //Template for responseObject
            //var responseObject = new { Status = "success/fail", Message = "", Data = "{JSON}"};
            CategoryManager categoryManager = new CategoryManager();
            try
            {
                var categoryList = categoryManager.GetAllCategoriesFiltered(0, inVisibility);
                var responseObject = new
                {
                    Status = "success",
                    Message = "Successfully fetched Categories with parameter (0,1)",
                    Data = JsonConvert.SerializeObject(categoryList, Formatting.None, new JsonSerializerSettings
                    {
                        DateTimeZoneHandling = DateTimeZoneHandling.Local
                    })
                };
                return responseObject;
            }
            catch (Exception ex)
            {
                var responseObject = new
                {
                    Status = "fail",
                    Message = "Error occured: " + ex.Message
                };
                return responseObject;
            }
        } // end of GetAllVisibleCategoryFiltered

        [WebMethod]
        public static object GetAllCategoriesSearch(int inVisibility, string inSearchField)
        {
            //Gets all the Category with visibility type and matching search value
            //Template for responseObject
            //var responseObject = new { Status = "success/fail", Message = "", Data = "{JSON}"};
            CategoryManager categoryManager = new CategoryManager();
            try
            {
                var categoryList = categoryManager.GetAllCategoriesBySearch(0, inVisibility, inSearchField);
                var responseObject = new
                {
                    Status = "success",
                    Message = "Successfully fetched Categories with parameter (0," + inVisibility + inSearchField + ")",
                    Data = JsonConvert.SerializeObject(categoryList, Formatting.None, new JsonSerializerSettings
                    {
                        DateTimeZoneHandling = DateTimeZoneHandling.Local
                    })
                };
                return responseObject;
            }
            catch (Exception ex)
            {
                var responseObject = new
                {
                    Status = "fail",
                    Message = "Error occured: " + ex.Message
                };
                return responseObject;
            }
        } // end of GetAllCategoriesSearch

        [WebMethod]
        public static object TestGetAllCategories(int inVisibility, string inSearchField, bool inSpecial, bool inDeleteFlag)
        {
            //Gets all the rootCategory with values passed in
            //Template for responseObject
            //var responseObject = new { Status = "success/fail", Message = "", Data = "{JSON}"};
            CategoryManager categoryManager = new CategoryManager();
            try
            {
                List<Category> categoryList = categoryManager.TestGetAllCategories(inVisibility, inSearchField,
                    inSpecial, inDeleteFlag);
                var responseObject = new
                {
                    Status = "success",
                    Message = "Successfully fetched Categories",
                    Data = JsonConvert.SerializeObject(categoryList, Formatting.None, new JsonSerializerSettings
                    {
                        DateTimeZoneHandling = DateTimeZoneHandling.Local
                    })
                };
                return responseObject;
            }
            catch (Exception ex)
            {
                var responseObject = new
                {
                    Status = "fail",
                    Message = "Error occured: " + ex.Message
                };
                return responseObject;
            }
        } // end of TestGetAllCategories
    } // end of Class CategoryList
} // end of Namespace