﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using WEBAAssignmentProject.BrandManagement.Domain;
using Microsoft.AspNet.Identity;

namespace WEBAAssignmentProject.Officer
{
    /// <summary>
    /// Summary description for UpdateBrandImageHandler
    /// </summary>
    public class UpdateBrandImageHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                if (!HttpContext.Current.Request.Files.AllKeys.Any())
                //if the current request does not contain any files, then return an error
                {
                    return;
                }
                BrandManager brandManager = new BrandManager();
                BrandPhoto brandPhoto = new BrandPhoto();
                int numOfFiles = HttpContext.Current.Request.Files.Count;
                // Get the uploaded image from the Files collection
                for (int index = 0; index < numOfFiles; index++)
                {
                    HttpPostedFile httpPostedFile = (HttpPostedFile)HttpContext.Current.Request.Files[index];

                    if (httpPostedFile != null)
                    {
                        //Reference: http://stackoverflow.com/questions/359894/how-to-create-byte-array-from-httppostedfile
                        //Converting posted file into a byte array

                        using (var binaryReader = new BinaryReader(httpPostedFile.InputStream))
                        {
                            brandPhoto = new BrandPhoto();
                            //This BrandId is posted using the fileinput uploadExtraData
                            brandPhoto.BrandId = Int32.Parse(HttpContext.Current.Request.Form["BrandId"].ToString());
                            brandPhoto.PhotoFileName = httpPostedFile.FileName;
                            brandPhoto.PhotoContentLength = httpPostedFile.ContentLength;
                            brandPhoto.PhotoContentType = httpPostedFile.ContentType;
                            //Call the ReadBytes method of the binaryReader (which has the file information)
                            //to begin writing all the file data into a byte array with the correct size (I used the content length info)
                            brandPhoto.PhotoImage = binaryReader.ReadBytes(brandPhoto.PhotoContentLength);
                        }
                        try
                        {
                            string officerId = HttpContext.Current.User.Identity.GetUserId();
                            brandManager.UpdateBrandPhoto(brandPhoto, officerId);
                        }
                        catch (Exception ex)
                        {
                            var failResponse = new
                            {
                                //return the error as {error : 'errormessage'}
                                //The plugin will automatically validate and display ajax exception errors.
                                error = "Unable to update photo. Error message is: " + ex.Message + ". Please try to refresh the page and reupload."
                            };
                            context.Response.ContentType = "application/json";
                            context.Response.Write(JsonConvert.SerializeObject(failResponse));
                            return;
                        }
                    } // end of if (httpPostedFile != null)
                } //end of foreach block to save each product photo
                var successResponse = new
                {
                    status = "success",
                    message = "Created " + numOfFiles + " photos."
                };
                context.Response.ContentType = "application/json";
                context.Response.Write(JsonConvert.SerializeObject(successResponse));
            }
            catch (Exception ex)
            {
                context.Response.Write(new KeyValuePair<bool, string>(false,
                    "An error occurred while uploading the file. Error Message: " + ex.Message));
            }
        } // end of ProcessRequest

        public bool IsReusable
        {
            get
            {
                return false;
            }
        } // end of IsReusable
    } // end of Class UpdateBrandImageHandler
} // end of Namespace