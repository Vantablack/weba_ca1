﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using WEBAAssignmentProject.CategoryManagement.Domain;

namespace WEBAAssignmentProject.Officer
{
    public partial class AddCategory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String userId = User.Identity.GetUserId();
            String userName = User.Identity.GetUserName();

            //userNameDiv.InnerHtml = userName;
            //userIdDiv.InnerHtml = userId;

            ////Reference: http://stackoverflow.com/questions/21688928/asp-net-identity-get-all-roles-of-logged-in-user
            //var userIdentity = (ClaimsIdentity) User.Identity;
            //var claims = userIdentity.Claims;
            //var roleClaimType = userIdentity.RoleClaimType;
            //var roles = claims.Where(c => c.Type == roleClaimType).ToList();
            //for (int index = 0; index < roles.Count; index++)
            //{
            //    userRoleDiv.InnerHtml += "Role is :  " +
            //                                        roles[index].Value.ToString() + "<br />";
            //} //for loop
        } //end of Page_Load()

        [WebMethod]
        public static object AddOneRootCategory(string inWebFormData)
        {
            //Try to add one Root/Primary category,
            //if success, return the id of the newly created category
            //if fail, return the error message
            //Template for responseObject
            //var responseObject = new { Status = "success/fail", Message = "", Data = "{JSON}"};
            try
            {
                var webFormObject = JsonConvert.DeserializeObject<dynamic>(inWebFormData);
                Category category = new Category();
                string officerId = HttpContext.Current.User.Identity.GetUserId();
                category.CategoryName = webFormObject.CategoryName.Value;
                category.Visibility = Int32.Parse(webFormObject.Visibility.Value);
                category.Special = bool.Parse(webFormObject.Special.Value.ToString());
                // will only parse the datetime if the visibility is 2 which is with the start date and end date
                if (category.Visibility == 2)
                {
                    category.StartDate = DateTime.ParseExact(webFormObject.StartDate.Value, "dd/MM/yyyy",
                        CultureInfo.InvariantCulture);
                    category.EndDate = DateTime.ParseExact(webFormObject.EndDate.Value, "dd/MM/yyyy",
                        CultureInfo.InvariantCulture);
                    if (category.StartDate.Date > category.EndDate.Date)
                    {
                        throw new Exception("Start date is larger than End date. Please check your dates.");
                    }
                } // end of if (...)
                CategoryManager categoryManager = new CategoryManager();
                int returnedId = categoryManager.AddOneRootCategory(category, officerId);
                var responseObject = new
                {
                    Status = "success",
                    Message = "Successfully created Category with the ID: " + returnedId
                };
                return responseObject;
            }
            catch (Exception ex)
            {
                var responseObject = new
                {
                    Status = "fail",
                    Message = "An error occured: " + ex.Message
                };
                return responseObject;
            }
        } // end of AddOneRootCategory

    } // end of Class AddCategory
}  // end of Namespace