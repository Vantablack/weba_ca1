﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEBAAssignmentProject.Officer
{
    public partial class ExperimentHowToGetUserIdWhoLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            userIdDisplayBox.InnerHtml = "The user id is " + User.Identity.GetUserId();
            userLoginNameDisplayBox.InnerHtml = "The user login name is " +
                                            User.Identity.GetUserName();

            //Reference: http://stackoverflow.com/questions/21688928/asp-net-identity-get-all-roles-of-logged-in-user
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == roleClaimType).ToList();
            for (int index = 0; index < roles.Count; index++)
            {
                userRoleNameDisplayBox.InnerHtml += "Role is :  " +
                                                                 roles[index].Value.ToString() + "<br />";
            }//for loop
        }
    }
}