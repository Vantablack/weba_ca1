﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExperimentHowToGetUserIdWhoLogin.aspx.cs" Inherits="WEBAAssignmentProject.Officer.ExperimentHowToGetUserIdWhoLogin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Experiment How to Grab User Id of Login User</title>
    <style>
        .dataDisplay{padding:3px;border: 2px solid black;width:50%;
            font-family:Arial;font-size:14pt;
            margin-bottom:10px;}
    </style>
</head>
<body>
    <i>The purpose of this web form is to find out how to obtain the user id of the Logon User so that you can use it when coding the create and update record logic</i>
    <form id="formData" runat="server">
       <div id="userIdDisplayBox" runat="server"  class="dataDisplay"></div>
       <div id="userLoginNameDisplayBox" runat="server"  class="dataDisplay"></div>
       <div id="userRoleNameDisplayBox" runat="server" class="dataDisplay"></div>
    </form>
</body>
</html>
