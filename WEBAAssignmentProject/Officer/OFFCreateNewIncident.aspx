﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OFFCreateNewIncident.aspx.cs" Inherits="WEBAAssignmentProject.Officer.OFFCreateNewIncident" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Add Incident</title>
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
    <h1>Add Incident</h1>
    
    <div class="container">
        <div class="row">
            <div class="col-md-offset-2 col-md-8">
                <div class="panel panel-info">
                    <div class="panel-heading"><h3>Add New Incident</h3></div>
                    <div class="panel-body">
                        <div class="alert alert-info" id="messageBox" runat="server">Enter incident details.</div>
                        <form class="form-horizontal" role="form" runat="server">
                            <div class="form-group">
                                <label for="titleInput" class="col-md-2 control-label">Title</label>
                                <div class="col-md-10">
                                    <asp:TextBox ID="titleInput" runat="server" placeholder="Title" text="Incident A" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="descriptionInput" class="col-md-2 control-label">Description</label>
                                <div class="col-md-10">
                                    <asp:TextBox  CssClass="form-control" runat="server" TextMode="MultiLine"
                                        id="descriptionInput" placeholder="Description" Text="Incident A Description" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-2 col-md-10">
                                    <asp:Button runat="server" id="saveButton" class="btn btn-default" Text="Save" 
                                        OnClick="saveButton_Click" />
                                </div>
                            </div>
                        </form>

                    </div>
                </div>

            </div>
        </div>
    </div>

</body>
</html>
