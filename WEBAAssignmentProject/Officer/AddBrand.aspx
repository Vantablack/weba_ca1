﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddBrand.aspx.cs" Inherits="WEBAAssignmentProject.Officer.AddBrand" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Add Brand</title>
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <link href="../Content/bootstrap-fileinput/css/fileinput.min.css" rel="stylesheet" />
    <link href="../Content/bootstrap-multiselect.css" rel="stylesheet" />
</head>
<body>
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <a href="AdminPanel.aspx" class="btn btn-default navbar-btn">
                    <span class="glyphicon glyphicon-home"></span>
                    Back to AdminPanel
                </a>
            </div>
        </nav>
        <div class="col-md-12">
            <div class="page-header">
                <h1>Add Brand</h1>
            </div>
            <div class="col-md-12">
                <div id="messageDiv" role="alert"></div>
                <div id="fi-messageDiv"></div>
            </div>
            <form class="form-horizontal" id="addBrandForm">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Brand Name</label>
                    <div class="col-sm-6">
                        <input id="brandNameTxtBox" name="brandNameTxtBox" class="form-control" placeholder="Name of brand" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Brand Image</label>
                    <div class="col-sm-6">
                        <input id="imageFileInput" name="imageFileInput" type="file" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Category</label>
                    <div class="col-sm-6">
                        <select id="checkboxMultiSelect" name="checkboxMultiSelect" multiple="multiple"></select>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-2 col-md-offset-8">
                        <a href="AdminPanel.aspx" class="btn btn-danger">Cancel</a>
                        <button id="submitButton" class="btn btn-success">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script src="../Scripts/jquery-2.1.4.min.js"></script>
    <script src="../Scripts/bootstrap.min.js"></script>
    <script src="../Scripts/jquery.validate.min.js"></script>
    <script src="../Scripts/fileinput.min.js"></script>
    <script src="../Scripts/bootstrap-multiselect.js"></script>
    <script>
        //VARIABLES
        window.newBrandId;
        window.$messageDiv = $('#messageDiv');
        window.$imageInput = $('#imageFileInput');
        window.$addBrandForm = $('#addBrandForm');
        window.$submitButton = $('#submitButton');
        window.$checkboxDiv = $('#checkboxDiv');
        window.chkBoxesArray = new Array();

        //Javascript class
        function WebFormData(inBrandName, inChkBoxes) {
            this.BrandName = inBrandName;
            this.Categories = inChkBoxes;
        }

        //On document ready, attach the form validation
        $(document).ready(function () {
            FillCategoryDropDown();

            //Event handler
            $submitButton.on('click', function (event) {
                $('#addBrandForm').validate();
            });

            var footerTemplate = '<div class="file-thumbnail-footer">\n' +
            '    <div class="file-caption-name" style="width:{width}">{caption}</div>\n' +
            '    {actions}\n' +
            '</div>';

            var actionTemplate = '<div class="file-actions">\n' +
                '       <div class="file-footer-buttons">\n {delete} </div>\n' +
                '    <div class="file-upload-indicator" tabindex="-1" title="{indicatorTitle}">{indicator}</div>\n' +
                '    <div class="clearfix"></div>\n' +
                '</div>';
            window.$imageInput.fileinput({
                uploadUrl: 'AddBrandImageHandler.ashx',
                type: 'POST',
                maxFileCount: 1,
                previewFileType: 'image',
                allowedFileTypes: ['image'],
                showUpload: false,
                uploadAsync: false,
                showCaption: true,
                msgInvalidFileType: 'Invalid type for file "{name}". Only "{types}" files are supported.',
                msgErrorClass: 'alert alert-danger',
                elErrorContainer: '#fi-messageDiv',
                //previewFileIcon: '<i class="glyphicon glyphicon-king"></i>',
                layoutTemplates: { footer: footerTemplate, actions: actionTemplate },
                //previewSettings: { image: { width: "350px", height: "auto" } },
                uploadExtraData: function () {
                    var out = {};
                    out['BrandId'] = window.newBrandId; //Must send this brand id information too.
                    console.log('This is the out object');
                    console.log(out);
                    return out;
                }
            });

            window.$addBrandForm.validate({
                //http://stackoverflow.com/questions/10225928/validating-multiselect-with-jquery-validation-plugin
                ignore: ":hidden:not(#checkboxMultiSelect)",
                debug: true,
                errorClass: "text-danger bg-danger",
                validClass: "bg-success",
                submitHandler: function (form) {
                    CollectBrandDataAndSave();
                },
                highlight: function (element, errorClass, validClass) {
                    $(element.closest("div.form-group")).addClass("has-error");
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element.closest("div.form-group")).removeClass("has-error");
                },
                errorPlacement: function (error, element) {
                    var $inputgroup = element.closest('div.input-group');
                    console.log($inputgroup);
                    if ($inputgroup.length === 1) {
                        error.insertAfter($inputgroup);
                    } else {
                        error.insertAfter(element);
                    }
                },
                rules: {
                    brandNameTxtBox: {
                        required: true,
                        minlength: 1
                    },
                    imageFileInput: {
                        required: true,
                        ImageOnly: true
                    },
                    checkboxMultiSelect: {
                        required: true
                    }
                },
                messages: {
                    brandNameTxtBox: {
                        required: "Brand name is required.",
                        minlength: jQuery.validator.format("At least {0} characters are needed")
                    },
                    imageFileInput: {
                        required: "Brand image is required."
                    }
                }
            }); // end of window.$addBrandForm.validate(...);

            $.validator.addMethod("ImageOnly", function (value, element) {
                if (!(/\.(gif|jpeg|jpg|png|tiff)$/i).test(value)) {
                    return false;
                } else {
                    return true;
                }
            }, "Please upload a proper image type.");


        });

        //Function to collect brandData and save it
        function CollectBrandDataAndSave() {
            //Collect data from the respective form elements
            var brandName = $('#brandNameTxtBox').val();
            var categories = $("#checkboxMultiSelect").val();
            //Initialize a Brand class object, brandData
            var webFormData = new WebFormData(brandName, categories);
            $.ajax(
            {
                type: 'POST',
                url: 'AddBrand.aspx/AddOneBrand',
                data: JSON.stringify({ inWebFormData: JSON.stringify(webFormData) }),
                //If single quote is removed from webFormDataInJason, you are passing the data 
                //as an object. The server side web method need to declare an object parameter.
                //data: "{'WebFormData' : " + studentDataInJson + "}",
                contentType: 'application/json; charset=utf-8',
                dataType: 'JSON',
                async: false,
                statusCode: {
                    500: function () {
                        alert("Internal Server Error");
                    }
                },
                error: function (xhr, status, errorThrown) {
                    //I copied these error: complete: and success: from http://learn.jquery.com/ajax/jquery-ajax-methods/
                    //.These are called settings. Ajax() method has a lot of settings.
                    //Learn to use console.log
                    console.log(xhr);
                    $messageDiv.text('Something happened. Message : ' + xhr.responseJSON.Message);
                    $messageDiv.removeClass("alert-success");
                    $messageDiv.addClass('alert alert-danger');
                }
            }).done(function (data, textStatus, jqXHR) {
                console.log("AJAX Done called");
                var responseObject = data.d;
                if (responseObject.Status === "fail") {
                    console.log("response fail " + responseObject.Message);
                    window.$messageDiv.empty().removeClass().addClass("alert alert-danger").append(responseObject.Message);
                } else {
                    console.log("response success");
                    var responseData = JSON.parse(responseObject.Data);
                    console.log(responseObject);
                    console.log(responseData);
                    window.newBrandId = responseData.newBrandId;
                    console.log("brandid is " + window.newBrandId);
                    window.$messageDiv.empty().removeClass().addClass("alert alert-success").append(responseObject.Message);
                    if (window.newBrandId !== "") {
                        window.$imageInput.fileinput('upload');
                        window.$imageInput.on('filebatchuploaderror', function (event, data, previewId, index) {
                            //on file upload error, delete the newly created Brand
                            console.log("Fileupload error");
                            console.log(data);
                            $.ajax({
                                type: 'POST',
                                url: 'AddBrand.aspx/DestroyBrand',
                                data: JSON.stringify({ inBrandId: window.newBrandId }),
                                contentType: 'application/json; charset=utf-8',
                                dataType: 'json',
                                async: false
                            }).done(function () {
                                console.log("Deleted new Brand");
                            });
                        });
                    } // end of if (window.newBrandId);
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                window.$messageDiv.empty().removeClass().addClass("alert alert-danger").append("Execution error: " + errorThrown);
            }); //end of $.ajax(...)
        } // end of CollectBrandDataAndSave

        //Function to fill category
        function FillCategoryDropDown() {
            //AJAX function to populate category checkbox
            $.ajax({
                url: "AddBrand.aspx/GetAllCategories",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",
                async: false,
                statusCode: {
                    500: function () {
                        alert("Internal Server Error");
                    }
                }
            }).done(function (data, textStatus, jqXHR) {
                var responseObject = data.d;
                if (responseObject.Status === "success") {
                    var categoryList = JSON.parse(responseObject.Data);
                    categoryList = SortJSON(categoryList, "CategoryName", "ASC");
                    var $checkboxMultiSelect = $("#checkboxMultiSelect");
                    for (index = 0; index < categoryList.length; index++) {
                        var $option = $("<option></option>").val(categoryList[index].CategoryId).text(categoryList[index].CategoryName);
                        $option.appendTo($checkboxMultiSelect);
                    }
                    $checkboxMultiSelect.multiselect();
                } else {
                    window.$messageDiv.empty().removeClass().addClass("alert alert-danger").append(responseObject.Message);
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                window.$messageDiv.empty().append("Unable to get Categories. Please refresh page. If problem persists, check console. " + errorThrown)
                    .removeClass().addClass("alert alert-danger");
                window.$submitButton.prop("disabled", true);
            }); // end of $.ajax(...);
        } // end of fillCategoryCheckbox

        function SortJSON(jsonObject, name, order) {
            //http://stackoverflow.com/questions/881510/sorting-json-by-values
            jsonObject = jsonObject.sort(function (a, b) {
                if (order === "ASC") return (a[name] > b[name]) ? 1 : ((a[name] < b[name]) ? -1 : 0);
                else return (b[name] > a[name]) ? 1 : ((b[name] < a[name]) ? -1 : 0);
            });
            return jsonObject;
        } // end of SortJSON
    </script>
</body>
</html>
