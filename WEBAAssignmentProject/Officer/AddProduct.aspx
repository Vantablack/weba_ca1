﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddProduct.aspx.cs" Inherits="WEBAAssignmentProject.Officer.AddProduct" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Add Product</title>
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <link href="../Content/bootstrap-fileinput/css/fileinput.min.css" rel="stylesheet" />
    <link href="../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <style>
        .radioButton label {
            display: block;
        }

        .radioButton input {
            display: inline-block;
            text-align: center;
        }

        .select-radius {
            border-radius: 3px;
        }
    </style>
</head>
<body>
    <!-- Modal -->
    <div class="modal fade" id="AddAttributeModal" tabindex="-1" role="dialog" aria-labelledby="AddAttributeModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="AddAttributeModalLabel">Add Attribute</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <form id="addAttributeForm" class="form-horizontal" role="form">
                            <label class="col-sm-3 col-xs-12 control-label">Attribute Name</label>
                            <div class="col-sm-7 col-xs-10">
                                <input name="addAttributeInput" class="form-control" placeholder="Name of attribute" type="text" />
                            </div>
                            <div class="col-sm-1 col-xs-1">
                                <button id="submitAttrButton" class="btn btn-success">Add</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Done</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <a href="AdminPanel.aspx" class="btn btn-default navbar-btn">
                    <span class="glyphicon glyphicon-home"></span>
                    Back to AdminPanel
                </a>
            </div>
        </nav>
        <div class="col-md-12">
            <div class="page-header">
                <h1>Add Product</h1>
            </div>
            <div class="col-md-12">
                <div id="messageDiv" role="alert"></div>
            </div>
            <form class="form-horizontal" id="addProductForm">
                <div class="form-group">
                    <div class="col-sm-10 col-md-offset-1">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Attributes of Product</h3>
                            </div>
                            <div class="panel-body">
                                <div id="addonDiv"></div>
                            </div>
                            <div class="panel-footer">
                                <!-- Button trigger modal -->
                                Attribute that you want to add not inside? Add it here&nbsp;<span class="glyphicon glyphicon-hand-right"></span>&nbsp;
                            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#AddAttributeModal">Add attribute</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Product Name</label>
                    <div class="col-sm-9">
                        <input id="productName" name="productName" class="form-control" placeholder="Name of product" type="text" />
                    </div>
                </div>
                <%-- Start of panel form-group --%>
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">Brand & Category</div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <label class="col-sm-4 control-label" for="brandDD">Brand</label>
                                    <div class="col-sm-8">
                                        <select id="brandDD" name="brandDD"></select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label class="col-sm-4 control-label" for="categoryDD">Category</label>
                                    <div class="col-sm-8">
                                        <select id="categoryDD" name="categoryDD" multiple=""></select>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <p>
                                    <span class="glyphicon glyphicon-question-sign pull-right" aria-hidden="true" data-toggle="popover"
                                        data-placement="auto" data-trigger="hover click" data-content="A product must have a brand & under a category of that brand"></span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <%-- Break between Panels --%>
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">Special Categories</div>
                            <div class="panel-body">
                                <label class="col-sm-4 control-label" for="specialCategoryDD">Category</label>
                                <div class="col-sm-8">
                                    <select id="specialCategoryDD" name="specialCategoryDD" multiple=""></select>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <p>
                                    <span class="glyphicon glyphicon-question-sign pull-right" aria-hidden="true" data-toggle="popover"
                                        data-placement="auto" data-trigger="hover click" data-content="Product may be under special categories like 'Christmas Sales'"></span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <%-- End of panel form-group --%>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Product Image</label>
                    <div class="col-sm-9">
                        <input id="imageFileInput" name="imageFileInput" type="file" multiple="" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Pricings</label>
                    <div class="col-sm-9">
                        <div id="pricingsRow" class="row">
                            <div class="col-sm-4" data-weightdiv="">
                                <label>Weight</label>
                            </div>
                            <div class="col-sm-3">
                                <label>Price</label>
                            </div>
                            <div class="col-sm-5">
                                <label>Quantity</label>
                            </div>
                        </div>
                        <div class="form-group" data-pricing-index="0">
                            <div class="col-sm-4" data-weightdiv="">
                                <div class="input-group">
                                    <input name="pricing[0].weight" class="form-control" placeholder="Weight" />
                                    <span class="input-group-addon">
                                        <select name="weightDropDown" class="select-radius">
                                            <option value="KG">KG</option>
                                            <option value="LBS">LBS</option>
                                        </select>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-3" data-pricediv="">
                                <input name="pricing[0].price" class="form-control" placeholder="Price" type="text" />
                            </div>
                            <div class="col-sm-5" data-quantitydiv="">
                                <div class="row">
                                    <div class="col-sm-9">
                                        <input name="pricing[0].quantity" class="form-control" placeholder="Quantity" type="text" />
                                    </div>
                                    <div class="col-sm-3">
                                        <button class="btn btn-default addButton" type="button">
                                            <span class="glyphicon-plus"></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%-- Template for rendering --%>
                        <div class="form-group hidden" id="pricingTemplate">
                            <div class="col-sm-4" data-weightdiv="">
                                <div class="input-group">
                                    <input name="weight" class="form-control" placeholder="Weight" />
                                    <span class="input-group-addon">
                                        <select name="weightDropDown" class="select-radius">
                                            <option value="KG">KG</option>
                                            <option value="LBS">LBS</option>
                                        </select>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <input name="price" class="form-control" placeholder="Price" type="text" />
                            </div>
                            <div class="col-sm-5">
                                <div class="row">
                                    <div class="col-sm-9">
                                        <input name="quantity" class="form-control" placeholder="Quantity" type="text" />
                                    </div>
                                    <div class="col-sm-3">
                                        <button class="btn btn-default removeButton" type="button">
                                            <span class="glyphicon-minus"></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%-- End of template --%>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-9">
                        <textarea id="Description-textarea"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-10">
                        <a href="AdminPanel.aspx" class="btn btn-danger">Cancel</a>
                        <button id="submitButton" class="btn btn-success">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script src="../Scripts/jquery-2.1.4.min.js"></script>
    <script src="../Scripts/jquery.validate.min.js"></script>
    <script src="../Scripts/bootstrap.min.js"></script>
    <script src="../Scripts/fileinput.min.js"></script>
    <script src="../Scripts/tinymce/tinymce.min.js"></script>
    <script src="../Scripts/bootstrap-multiselect.js"></script>
    <script>
        //VARIABLES
        window.$messageDiv = $("#messageDiv");
        window.$brandDD = $("#brandDD");
        window.$imageInput = $("#imageFileInput");
        window.$addProductForm = $("#addProductForm");
        window.$submitButton = $("#submitButton");
        window.priceIndex = 0;
        window.showWeight = false;
        window.newProductId = 0;

        var changeEvent = function () {
            if ($(this).data("attribute") === "Weight") {
                if ($(this).is(":checked")) {
                    window.showWeight = true;
                    $("#addonDiv").find("[data-attribute='Weight-Unit']").prop("checked", true);
                    var $weightDiv = $("div[data-weightdiv]");
                    $weightDiv.removeClass("hidden").find("input").prop("disabled", false).end().find("select").prop("disabled", false);
                } else {
                    window.showWeight = false;
                    $("#addonDiv").find("[data-attribute='Weight- Unit']").prop("checked", false);
                    var $weightDiv = $("div[data-weightdiv]");
                    $weightDiv.addClass("hidden").find("input").prop("disabled", true).end().find("select").prop("disabled", true);
                }
                console.log("Show Weight:" + window.showWeight);
            } else {
                if ($(this).is(":checked") && !$(this).is(":disabled")) {
                    //if not checked and not disabled, then create the form-group
                    //insert append the form
                    var template = $.validator.format(textAreaTemplate);
                    //console.log($(this).closest("label").text());
                    var templateFormatted = template($(this).closest("label").text(), $(this).data("attribute") + "-textarea");
                    var $formgroup = $(templateFormatted);
                    $formgroup.insertAfter($("#pricingTemplate").parents(".form-group"));
                    InitialiseTinyMCE($(this).data("attribute") + "-textarea");
                } else {
                    var editor_id = $(this).data("attribute") + "-textarea";
                    //if the checkbox is unchecked, delete the form-group from the form
                    //http://stackoverflow.com/questions/4651676/how-do-i-remove-tinymce-and-then-re-add-it
                    if (editor_id !== "Description-textarea") {
                        tinymce.EditorManager.execCommand('mceRemoveEditor', true, editor_id);
                        $("#" + $(this).data("attribute") + "-textarea").closest(".form-group").remove();
                    }
                }
            }

            //var textArea;
            //if ($(this).is(":checked")) {
            //    //http://stackoverflow.com/questions/5309926/how-to-get-the-data-id-attribute
            //    textArea = $(this).data("textarea");
            //    $('#' + textArea).closest(".form-group").removeClass("hidden");
            //} else {
            //    textArea = $(this).data("textarea");
            //    $('#' + textArea).closest(".form-group").addClass("hidden");
            //}
        } // end of changeEvent var

        var footerTemplate = '<div class="file-thumbnail-footer">\n' +
            '<div class="radioButton">' +
            '<input type="radio" name="primary[]" value="{fileindex}" checked="" >' +
            '<label>Primary Photo</label>' +
            '</div>' +
            '    <div class="file-caption-name" style="width:{width}">' +
            '{caption}' +
            '</div>\n' +
            '     {actions}\n' +
            '</div>';

        var actionsTemplate = '<div class="file-actions">\n' +
            '    <div class="file-footer-buttons">\n' +
            '        {delete}' +
            '    </div>\n' +
            '    <div class="file-upload-indicator" tabindex="-1" title="{indicatorTitle}">{indicator}</div>\n' +
            '    <div class="clearfix"></div>\n' +
            '</div>';

        var textAreaTemplate = "<div class=\"form-group\">\n" +
            "\t<label class=\"col-sm-2 control-label\">{0}</label>\n" +
            "\t<div class=\"col-sm-9\">\n" +
            "\t\t<textarea id=\"{1}\"></textarea>\n" +
            "\t</div>\n" +
            "</div>";

        function Product(inProductName, inProductBrand, inProductCategories, inSpecialCategories, inProductAddons, inProductPricings) {
            this.ProductName = inProductName;
            this.ProductBrand = inProductBrand;
            this.ProductCategories = inProductCategories;
            this.SpecialCategories = inSpecialCategories;
            this.ProductAddons = inProductAddons;
            this.ProductPricings = inProductPricings;
        }

        function Attribute(inAttributeId, inAttributeContent) {
            this.AttributeId = inAttributeId;
            this.AttributeContent = inAttributeContent;
        }

        $(document).ready(function () {
            $('[data-toggle="popover"]').popover();

            $("#addProductForm").validate({
                debug: true,
                errorClass: "text-danger bg-danger",
                validClass: "bg-success",
                submitHandler: function (form) {
                    //IMPLEMENT THIS PART
                    CollectDataAndSave();
                },
                highlight: function (element, errorClass, validClass) {
                    if ($(element).is("input[name*='pricing']")) {
                        var $inputgroup = element.closest("div.input-group");
                        if ($inputgroup) {
                            $($inputgroup).addClass("has-error");
                        } else {
                            //console.log("Adding class to input");
                            //console.log(element);
                            $(element).closest("div").addClass("has-error");
                        }
                    } else {
                        $($(element).closest("div.form-group")).addClass("has-error");
                    }
                },
                unhighlight: function (element, errorClass, validClass) {
                    //console.log("Unhighlight called");
                    //console.log(element);
                    if ($(element).is("input[name*='pricing']")) {
                        var $inputgroup = element.closest("div.input-group");
                        if ($inputgroup) {
                            $($inputgroup).removeClass("has-error").addClass("has-success");
                        } else {
                            //console.log("Adding class to input");
                            //console.log(element);
                            $(element).closest("div").removeClass("has-error").addClass("has-success");
                        }
                    } else {
                        $($(element).closest("div.form-group")).removeClass("has-error").addClass("has-success");
                    }
                },
                errorPlacement: function (error, element) {
                    var $inputgroup = $(element).closest("div.input-group");
                    if ($(element).is("input[name*='pricing']")) {
                        if ($inputgroup.length) {
                            error.insertAfter($inputgroup);
                        } else {
                            error.insertAfter(element);
                        }
                    } else {
                        if ($inputgroup.length) {
                            error.insertAfter($inputgroup);
                        } else {
                            error.insertAfter(element);
                        }
                    }
                },
                rules: {
                    imageFileInput: {
                        required: true,
                        ImageOnly: true
                    },
                    productName: 'required',
                    'pricing[0].weight': 'required',
                    'pricing[0].price': { required: true },
                    'pricing[0].quantity': 'required'
                },
                messages: {
                    imageFileInput: {
                        required: "Brand image is required."
                    }
                }
                //showErrors: function (errorMap, errorList) {
                //    console.log(this);
                //    console.log(this.numberOfInvalids());
                //    this.defaultShowErrors();
                //}
            }); // end of window.$addProductForm.validate(...);

            $("#addAttributeForm").validate({
                debug: true,
                errorClass: "text-danger bg-danger",
                validClass: "bg-success",
                submitHandler: function (form) {
                    CollectAndAddAttribute();
                },
                highlight: function (element, errorClass, validClass) {
                    $("#addAttributeForm").addClass("has-error").removeClass("has-success");
                },
                unhighlight: function (element, errorClass, validClass) {
                    $("#addAttributeForm").removeClass("has-error").addClass("has-success");
                },
                errorPlacement: function (error, element) {
                    error.insertAfter(element);
                },
                rules: {
                    addAttributeInput: "required"
                }
            }); // end of window.$addProductForm.validate(...);

            $.validator.addMethod("ImageOnly", function (value, element) {
                if (!(/\.(gif|jpeg|jpg|png|tiff)$/i).test(value)) {
                    return false;
                } else {
                    return true;
                }
            }, "Please upload a proper image type.");

            //On add button click handler and the rules
            $("#addProductForm").on("click", ".addButton", function () {
                window.priceIndex++;
                var $template = $("#pricingTemplate");
                //Clone the template and 
                var $clone = $template
                    .clone()
                    .removeClass("hidden")
                    .removeAttr("id")
                    .attr("data-pricing-index", window.priceIndex)
                    .insertBefore($template);
                //Update name attributes
                $clone.find("input[name='weight']").attr("name", "pricing[" + window.priceIndex + "].weight")
                    .rules("add", "required");
                $clone.find("input[name='price']").attr("name", "pricing[" + window.priceIndex + "].price")
                    .rules("add", "required");
                $clone.find("input[name='quantity']").attr("name", "pricing[" + window.priceIndex + "].quantity")
                    .rules("add", "required");
            }); // end of $("#addProductForm").on("click", ".addButton")

            //On remove button click handler and remove rules
            $("#addProductForm").on("click", ".removeButton", function () {
                var $formgroup = $(this).closest(".form-group");
                $formgroup.find("input[name*=pricing]").each(function () {
                    $(this).rules("remove");
                });
                $formgroup.remove();
            }); // end of $("#addProductForm").on("click", ".removeButton")

            $("#AddAttributeModal").on("shown.bs.modal", function () {
                $("#addAttributeForm input").focus();
            });

            $("#submitAttrButton").on("click", function () {
                console.log("#submitAttrButton click");
                $("#addAttributeForm").valid();
            });

            GetAllAttributes();
            BindBrand();
            BindSpecialCategory();
            SetupImageInput();
            InitialiseTinyMCE("Description-textarea");
        }); // end of $(document).ready(...);


        //------FUNCTIONS------
        function InitialiseTinyMCE(idOfTextArea) {
            //For configuration, check here
            //http://codepen.io/tinymce/pen/YydQrY/
            tinyMCE.init({
                selector: 'textarea#' + idOfTextArea,
                setup: function (editor) {
                    editor.on('click', function (e) {
                        //console.log('Editor was clicked: ' + e);
                    });
                },
                plugins: ["table textcolor preview charmap fullscreen code"],
                toolbar: 'insertfile undo redo | styleselect forecolor backcolor | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent'
            });
        }; // end of InitialiseTinyMCE()

        function BindCategory(brandId) {
            $.ajax({
                url: "AddProduct.aspx/GetAllCategoriesOfBrand",
                type: "POST",
                async: false,
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",
                data: JSON.stringify({ inBrandId: brandId })
            }).done(function (data, textStatus, jqXHR) {
                var responseObject = data.d;
                if (responseObject.Status === "success") {
                    var categoryList = JSON.parse(responseObject.Data);
                    var $categoryDD = $("#categoryDD");
                    if (categoryList.length === 0) {
                        $categoryDD.empty();
                        $categoryDD.multiselect("rebuild");
                        window.$messageDiv.empty().append("Weird, a Brand must have a category").removeClass().addClass("alert alert-warning");
                        window.$submitButton.prop("disabled", true);
                    } else {
                        window.$messageDiv.addClass("hidden");
                        $categoryDD.empty();
                        categoryList = SortJSON(categoryList, "CategoryName", "ASC");
                        for (index = 0; index < categoryList.length; index++) {
                            var $option = $("<option></option>").val(categoryList[index].CategoryId).text(categoryList[index].CategoryName);
                            $option.appendTo($categoryDD);
                        }
                        $categoryDD.multiselect("rebuild");
                        window.$submitButton.prop("disabled", false);
                    }
                } else {
                    window.$messageDiv.empty().removeClass().addClass("alert alert-danger").append(responseObject.Message);
                    window.$submitButton.prop("disabled", true);
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR);
                window.$messageDiv.removeClass().addClass("alert alert-danger").html(" Unable to retrieve Categories: " + errorThrown + "<br/>Refresh page. If error persists, contact administrator.");
            }); // end of $.ajax(...)
        } // end of BindCategory

        function BindBrand() {
            $.ajax({
                url: "AddProduct.aspx/GetAllBrand",
                type: "POST",
                async: false,
                contentType: "application/json; charset=utf-8",
                dataType: "JSON"
            }).done(function (data, textStatus, jqXHR) {
                //console.log("AddProduct.aspx/GetAllBrand .done() called");
                //console.log(data);
                var responseObject = data.d;
                //check if response is success/fail
                if (responseObject.Status === 'fail') {
                    window.$messageDiv.empty();
                    window.$messageDiv.removeClass().addClass("alert alert-danger").html(responseObject.Message);
                    window.$submitButton.prop("disabled", true);
                } else {
                    window.$messageDiv.addClass("hidden");
                    var brandList = $.parseJSON(responseObject.Data);
                    if (brandList.length === 0) {
                        window.$messageDiv.empty().append("No brands with this category in database").removeClass().addClass("alert alert-warning");
                        window.$submitButton.prop("disabled", true);
                    } else {
                        window.$brandDD.empty();
                        brandList = SortJSON(brandList, "BrandName", "ASC");
                        for (index = 0; index < brandList.length; index++) {
                            var $option = $("<option></option>").val(brandList[index].BrandId).text(brandList[index].BrandName);
                            $option.appendTo(window.$brandDD);
                        }
                        window.$submitButton.prop("disabled", false);
                        window.$brandDD.multiselect({
                            onChange: function (option, checked, selected) {
                                console.log(option);
                                console.log(checked);
                                console.log(selected);
                                BindCategory($(option).val());
                            }
                        });
                        BindCategory(window.$brandDD.val());
                    }
                } // end of if... else...
            }).fail(function (jqXHR, textStatus, errorThrown) {
                window.$messageDiv.empty();
                window.$messageDiv.removeClass().addClass("alert alert-danger").html("Something happened : " + errorThrown);
            }); // end of $.ajax(...);
        } // end of BindDropDownList()

        function BindSpecialCategory() {
            $.ajax({
                url: "AddProduct.aspx/GetAllSpecialCategories",
                type: "POST",
                async: false,
                contentType: "application/json; charset=utf-8",
                dataType: "JSON"
            }).done(function (data, textStatus, jqXHR) {
                var responseObject = data.d;
                if (responseObject.Status === "success") {
                    var categoryList = JSON.parse(responseObject.Data);
                    if (categoryList.length === 0) {
                        $("#specialCategoryDD").closest("div.panel-body").empty().append(function () {
                            var $paragraph = $("<p></p>").html("No special categories found. Perhaps you'll want to add one ");
                            var $u = $("<u></u>").append($("<a></a>").attr({ href: "AddCategory.aspx" }).text("here"));
                            $paragraph.append($u);
                            return $paragraph;
                        });
                    } else {
                        window.$messageDiv.addClass("hidden");
                        var $specialCategoryDD = $("#specialCategoryDD");
                        categoryList = SortJSON(categoryList, "CategoryName", "ASC");
                        for (index = 0; index < categoryList.length; index++) {
                            var $option = $("<option></option>").val(categoryList[index].CategoryId).text(categoryList[index].CategoryName);
                            $option.appendTo($specialCategoryDD);
                        }
                        $specialCategoryDD.multiselect();
                        window.$submitButton.prop("disabled", false);
                    }
                } else {
                    window.$messageDiv.empty().removeClass().addClass("alert alert-danger").append(responseObject.Message);
                    window.$submitButton.prop("disabled", true);
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR);
                window.$messageDiv.removeClass().addClass("alert alert-danger").html(" Unable to retrieve Categories: " + errorThrown + "<br/>Refresh page. If error persists, contact administrator.");
            }); // end of $.ajax(...)
        } // end of BindSpecialCategory

        function SetupImageInput() {
            window.$imageInput.fileinput({
                uploadUrl: 'AddProductImageHandler.ashx',
                overwriteInitial: true,
                showUpload: false,
                previewFileType: 'image',
                uploadAsync: false,
                maxFileCount: 5,
                type: 'POST',
                showCaption: true,
                previewFiletype: 'image',
                previewFileIcon: '<i class="glyphicon glyphicon-king"></i>',
                layoutTemplates: { footer: footerTemplate, actions: actionsTemplate },
                uploadExtraData: function () {
                    var extraData = {};
                    var $previewFrame = $('div.file-preview-frame[data-fileindex]');
                    //Add the newProductID into the extraData
                    extraData["newProductID"] = window.newProductId;
                    //find the image preview and get if that is the primary photo,
                    //and store it in { PhotoFileName : IsPrimary }
                    $previewFrame.each(function () {
                        extraData[$(this).find(".file-caption-name").html()] = $(this).find("input[name*=primary]").is(":checked");
                    });
                    console.log("This is the extra data sent to the server: ");
                    console.log(extraData);
                    return extraData;
                }
            });
            window.$imageInput.on('filebatchuploadsuccess', function (event, data, previewId, index) {
                console.log('File batch upload success');
                console.log(data);
                var $clone = window.$messageDiv.clone().removeAttr("id").removeClass()
                    .addClass("alert alert-success alert-dismissible fade in").empty();
                var $button = $('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
                $clone.append($button).append("Successfully uploaded product images.").alert();
                $clone.insertAfter(window.$messageDiv);
            });
            window.$imageInput.on('filebatchuploaderror', function (event, data) {
                console.log('File batch upload error');
                console.log(data);
                var $clone = window.$messageDiv.clone().removeAttr("id").removeClass()
                    .addClass("alert alert-danger alert-dismissible fade in").empty();
                var $button = $('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
                $clone.append($button).append("<h4>Oh snap! Failed to upload product images.</h4><p>Either the server stopped responding or execution error.</p>").alert();
                if (data.response.error !== undefined)
                    $clone.append("<p>" + data.response.error + "</p>");
                $clone.insertAfter(window.$messageDiv);
            });
        } // end of SetupImageInput

        function GetAllAttributes() {
            $.ajax({
                url: "AddProduct.aspx/GetAllAttributes",
                type: "POST",
                async: false,
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",
                statusCode: {
                    500: function () {
                        alert("Internal Server Error");
                    }
                }
            }).done(function (data, textStatus, jqXHR) {
                var responseObject = data.d;
                if (responseObject.Status === "success") {
                    var $addonDiv = $("#addonDiv");
                    $addonDiv.empty();
                    var attributeList = JSON.parse(responseObject.Data);
                    for (index = 0; index < attributeList.length; index++) {
                        var $label = $("<label></label>").append(attributeList[index].AttributeName).addClass("checkbox-inline");
                        var $input;

                        switch (attributeList[index].AttributeName) {
                            case "Description":
                                $input = $("<input />").attr({
                                    'type': 'checkbox',
                                    'value': attributeList[index].AttributeId,
                                    'data-attribute': attributeList[index].AttributeName.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-')
                                }).prop({ 'checked': true, 'disabled': true });
                                break;
                            case "Quantity":
                                $input = $("<input />").attr({
                                    'type': 'checkbox',
                                    'value': attributeList[index].AttributeId,
                                    'data-attribute': attributeList[index].AttributeName.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-')
                                }).prop({ 'checked': true, 'disabled': true });
                                break;
                            case "Weight Unit":
                                $input = $("<input />").attr({
                                    'type': 'checkbox',
                                    'value': attributeList[index].AttributeId,
                                    'data-attribute': attributeList[index].AttributeName.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-')
                                }).prop({ 'disabled': true });
                                break;
                            default:
                                $input = $("<input />").attr({
                                    'type': 'checkbox',
                                    'value': attributeList[index].AttributeId,
                                    'data-attribute': attributeList[index].AttributeName.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-')
                                });
                                break;
                        }
                        //attach change handler, call the change handler
                        $input.change(changeEvent).change();
                        $label.prepend($input);
                        $addonDiv.append($label);
                    }
                    window.$submitButton.prop("disabled", false);
                } else {
                    window.$messageDiv.empty().append(responseObject.Message).removeClass().addClass("alert alert-danger");
                    window.$submitButton.prop("disabled", true);
                }
            })
                .fail(function (jqXHR, textStatus, errorThrown) {
                    window.$messageDiv.empty().append("Error occurred: " + errorThrown).removeClass().addClass("alert alert-warning");
                    window.$submitButton.prop("disabled", true);
                });
        };

        function CollectAndAddAttribute() {
            var $modal = $("#AddAttributeModal");
            var attributeName = $modal.find("input").val();
            console.log("AttributeName:" + attributeName);
            $.ajax({
                url: "AddProduct.aspx/AddOneAttribute",
                type: "POST",
                async: false,
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",
                data: JSON.stringify({ inAttributeName: attributeName }),
                statusCode: {
                    500: function () {
                        alert("Internal Server Error");
                    }
                }
            })
                .done(function (data, textStatus, jqXHR) {
                    if (data.d.Status === "success") {
                        GetAllAttributes();
                        $modal.find("input").val("");
                        $modal.modal('hide');
                    } else {
                        alert("Fail");
                    }
                })
                .fail(function (jqXHR, textStatus, errorThrown) {
                    alert("AJAX FAIL");
                });
        } // end of CollectAndAddAttribute

        function SortJSON(jsonObject, name, order) {
            //http://stackoverflow.com/questions/881510/sorting-json-by-values
            jsonObject = jsonObject.sort(function (a, b) {
                if (order === "ASC") return (a[name] > b[name]) ? 1 : ((a[name] < b[name]) ? -1 : 0);
                else return (b[name] > a[name]) ? 1 : ((b[name] < a[name]) ? -1 : 0);
            });
            return jsonObject;
        } // end of SortJSON

        function CollectDataAndSave() {
            var textAreaContentsJson = {
                Addons: []
            }
            var productPricingsJson = {
                Pricings: []
            }

            //Foreach checked attribute, get the content and store in an array
            //this will definitely collect Description and ignore quantity and weight
            $("#addonDiv :checked").each(function () {
                var attribute = $(this).data("attribute");
                console.log(attribute);
                var attributeObject;
                var contents;
                if (attribute === "Description") {
                    contents = tinyMCE.get(attribute + "-textarea").getContent();
                    console.log(contents);
                    attributeObject = new Attribute($(this).val(), contents);
                    textAreaContentsJson.Addons.push(attributeObject);
                } else if (attribute !== "Quantity" && attribute !== "Weight-Unit" && attribute !== "Weight") {
                    contents = tinyMCE.get(attribute + "-textarea").getContent();
                    console.log(contents);
                    attributeObject = new Attribute($(this).val(), contents);
                    textAreaContentsJson.Addons.push(attributeObject);
                }
            });
            console.log(textAreaContentsJson);

            //Getting the product name 
            var productName = $("#productName").val();
            //console.log(productName);

            //Getting the product brand
            var productBrand = $("#brandDD").val();

            //Getting the product categories, this will be an array
            var productCategories = $("#categoryDD").val();

            var productSpecialCategories = $("#specialCategoryDD").val();

            //Getting all the pricings
            //console.log($(".form-group [data-pricing-index]"));
            if (window.showWeight) {
                //if showWeight is checked, then collect the weight data
                $(".form-group [data-pricing-index]").each(function () {
                    var weight = $(this).find("input[name*=weight]").val();
                    var weightType = $(this).find("select[name=weightDropDown]").val();
                    var price = $(this).find("input[name*=price]").val();
                    var quantity = $(this).find("input[name*=quantity]").val();
                    var pricingObject = { "Weight": weight, "WeightType": weightType, "Price": price, "Quantity": quantity };
                    productPricingsJson.Pricings.push(pricingObject);
                });
            } else {
                $(".form-group [data-pricing-index]").each(function () {
                    var price = $(this).find("input[name*=price]").val();
                    var quantity = $(this).find("input[name*=quantity]").val();
                    var pricingObject = { "Price": price, "Quantity": quantity };
                    productPricingsJson.Pricings.push(pricingObject);
                });
            }
            console.log(productPricingsJson);
            //console.log(JSON.stringify(productPricingsJson));

            var productObject = new Product(productName, productBrand, productCategories, productSpecialCategories,
                textAreaContentsJson.Addons, productPricingsJson.Pricings);
            console.log(productObject);
            console.log(JSON.stringify(productObject));

            //AJAX call to add the product
            $.ajax({
                url: "AddProduct.aspx/AddOneProduct",
                type: "POST",
                async: false,
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",
                data: JSON.stringify({ inWebFormData: JSON.stringify(productObject) }),
                statusCode: {
                    500: function () {
                        alert("Internal Server Error");
                    }
                }
            }).done(function (data, textStatus, jqXHR) {
                var responseObject = data.d;
                if (responseObject.Status === "success") {
                    var dataObject = JSON.parse(responseObject.Data);
                    window.newProductId = dataObject.newProductID;
                    console.log(window.newProductId);
                    window.$messageDiv.empty().removeClass().addClass("alert alert-success").append(responseObject.Message);
                    //Call the file upload, this will trigger the upload extra data for the file input
                    window.$imageInput.fileinput("upload");
                } else {
                    window.$messageDiv.empty().removeClass().addClass("alert alert-danger").append(responseObject.Message);
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                window.$messageDiv.empty().removeClass().addClass("alert alert-danger").append(errorThrown);
            });
        } // end of CollectDataAndSave()
    </script>
</body>
</html>
