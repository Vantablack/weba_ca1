﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CategoryList.aspx.cs" Inherits="WEBAAssignmentProject.Officer.CategoryList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>List of Categories</title>
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <link href="../Content/bootstrap-switch/bootstrap3/bootstrap-switch.min.css" rel="stylesheet" />
</head>
<body>
    <div class="container-fluid">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <a href="AdminPanel.aspx" class="btn btn-default navbar-btn">
                    <span class="glyphicon glyphicon-home"></span>
                    Back to AdminPanel
                </a>
            </div>
        </nav>
        <div class="page-header">
            <h3>List of Categories</h3>
        </div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-sm-6">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <div class="input-group">
                                    <span class="input-group-addon">Visibility</span>
                                    <select id="visibilityDropDown" class="form-control">
                                        <option value="3" selected="">All</option>
                                        <option value="1">Visible (ignore start and end date)</option>
                                        <option value="2">Visible (with start and end date)</option>
                                        <option value="0">Hidden</option>
                                    </select>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="input-group">
                                    <input id="searchInput" type="text" class="form-control" placeholder="Search for..." />
                                    <span class="input-group-btn">
                                        <button id="searchButton" class="btn btn-primary" type="button">Search</button>
                                    </span>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-6">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <input id="showDeletedCategories" type="checkbox" name="showDeletedCategories" data-size="small"
                                    data-on-color="success" data-off-color="danger" data-on-text="YES" data-off-text="NO" />&nbsp;Show deleted categories
                            </li>
                            <li class="list-group-item">
                                <input id="isSpecialCategory" type="checkbox" name="isSpecialCategory" data-size="small"
                                    data-on-color="success" data-off-color="danger" data-on-text="YES" data-off-text="NO" />&nbsp;Special Category?
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div id="messageDiv" role="alert"></div>
        </div>
        <table class="table table-hover" id="categoryListTable">
            <thead>
                <tr id="defaultTr">
                    <th scope="col">ID</th>
                    <th scope="col">Category Name</th>
                    <th scope="col">No. of Sub-Categories</th>
                    <th scope="col">Created At</th>
                    <th scope="col">Created By</th>
                    <th scope="col">Updated At</th>
                    <th scope="col">UpdatedBy</th>
                    <th scope="col">Visibility</th>
                    <th scope="col">Manage Brand</th>
                    <th scope="col">Update</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
    <script src="../Scripts/jquery-2.1.4.min.js"></script>
    <script src="../Scripts/bootstrap.min.js"></script>
    <script src="../Scripts/moment.min.js"></script>
    <script src="../Scripts/bootstrap-switch.min.js"></script>

    <script>
        window.$categoryListTable = $("#categoryListTable");
        window.$visibilityDropDown = $("#visibilityDropDown");
        window.$searchInput = $("#searchInput");
        window.$searchButton = $("#searchButton");
        window.$messageDiv = $("#messageDiv");
        window.$showDeletedCategories = $("#showDeletedCategories");
        window.$isSpecialCategory = $("#isSpecialCategory");
        window.$trClone = $("#defaultTr").clone().removeAttr("id");

        $(document).ready(function () {
            window.$showDeletedCategories.bootstrapSwitch();
            window.$isSpecialCategory.bootstrapSwitch();
            TestGetAllCategories();
        }); // end of $(document).ready(...);

        window.$visibilityDropDown.change(function () {
            //Clear searchField in case it looks confusing
            TestGetAllCategories();
        }); // end of window.$categoryDropDown.change(...)

        window.$showDeletedCategories.on("switchChange.bootstrapSwitch", function () {
            if (window.$showDeletedCategories.is(":checked")) {
                console.log($("#defaultTr"));
                $("#defaultTr > th:nth-last-child(2)").remove();
            } else {
                $("#defaultTr").replaceWith($trClone.clone().attr("id", "defaultTr"));
            }
            TestGetAllCategories();
        });

        window.$isSpecialCategory.on("switchChange.bootstrapSwitch", function () {
            TestGetAllCategories();
        });

        window.$searchButton.on("click", function () {
            TestGetAllCategories();
        }); // end of window.$searchButton.on("click");

        //FUNCTIONS
        //Function to populate table
        function PopulateTable(inCategoryList) {
            //console.log("In populate table: ");
            //console.log(inBrandList);
            var $tableBodyElement = window.$categoryListTable.find("tbody");
            $tableBodyElement.empty();
            for (this.index = 0; index < inCategoryList.length; this.index++) {
                var date;
                var $rowElement = $("<tr></tr>");
                var $cellElement = $("<td></td>").text(inCategoryList[index].CategoryId);
                $rowElement.append($cellElement);
                $cellElement = $("<td></td>").text(inCategoryList[index].CategoryName);
                $rowElement.append($cellElement);
                $cellElement = $("<td></td>").text(inCategoryList[index].SubCategoryCount);
                $rowElement.append($cellElement);
                //http://stackoverflow.com/questions/206384/format-a-microsoft-json-date/2316066#2316066
                date = new Date(inCategoryList[index].CreatedAt);
                $cellElement = $("<td></td>").text(moment(date).format("DD MMM YYYY"));
                $rowElement.append($cellElement);
                $cellElement = $("<td></td>").text(inCategoryList[index].CreatedBy);
                $rowElement.append($cellElement);
                date = new Date(inCategoryList[index].UpdatedAt);
                $cellElement = $("<td></td>").text(moment(date).format("DD MMM YYYY"));
                $rowElement.append($cellElement);
                $cellElement = $("<td></td>").text(inCategoryList[index].UpdatedBy);
                $rowElement.append($cellElement);
                var $span = $("<span></span>");
                switch (inCategoryList[index].Visibility) {
                    case 0:
                        $span.text("Hidden").addClass("label label-warning");
                        break;
                    case 1:
                        $span.text("Always Visible").addClass("label label-success");
                        break;
                    case 2:
                        $span.text("Visible (with start date and end date)").addClass("label label-primary");
                        break;
                    default:
                        $span.text("ERROR").addClass("label label-danger");
                        break;
                }
                $cellElement = $("<td></td>").append($span);
                $rowElement.append($cellElement);
                if (!window.$showDeletedCategories.is(":checked")) {
                    var $brandButton = $("<a />").attr({ href: 'BrandList.aspx?categoryid=' + inCategoryList[index].CategoryId }).addClass('btn btn-default').text('Brands');
                    $cellElement = $("<td></td>").append($brandButton);
                    $rowElement.append($cellElement);
                }
                var $updateButton = $("<a />").attr({ href: 'UpdateCategory.aspx?id=' + inCategoryList[index].CategoryId }).addClass('btn btn-default').text('Update');
                $cellElement = $("<td></td>").append($updateButton);
                $rowElement.append($cellElement);
                $rowElement.appendTo($tableBodyElement);
            } // end of for {}
        }; // end of PopulateTable

        function TestGetAllCategories() {
            var visibility = window.$visibilityDropDown.val();
            var showDeleted = window.$showDeletedCategories.is(":checked");
            var isSpecial = window.$isSpecialCategory.is(":checked");
            var searchField = window.$searchInput.val();
            console.log(JSON.stringify({
                inVisibility: visibility,
                inSearchField: searchField,
                inSpecial: isSpecial,
                inDeleteFlag: showDeleted
            }));
            $.ajax({
                url: "CategoryList.aspx/TestGetAllCategories",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",
                async: false,
                statusCode: {
                    500: function () {
                        alert("Internal Server Error");
                    }
                },
                data: JSON.stringify({
                    inVisibility: visibility,
                    inSearchField: searchField,
                    inSpecial: isSpecial,
                    inDeleteFlag: showDeleted
                })
            }).done(function (data, textStatus, jqXHR) {
                var responseObject = data.d;
                if (responseObject.Status === "success") {
                    if (responseObject.Data !== undefined) {
                        var categoryList = JSON.parse(responseObject.Data);
                        if (categoryList.length === 0) {
                            window.$messageDiv.empty().append("No categories found").removeClass().addClass("alert alert-warning");
                            window.$categoryListTable.find("tbody").empty();
                        } else {
                            window.$messageDiv.addClass("hidden");
                            PopulateTable(categoryList);
                        }
                    }
                } else {
                    window.$messageDiv.empty().removeClass().addClass("alert alert-danger").append(responseObject.Message);
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR);
                window.$messageDiv.removeClass().addClass("alert alert-danger").html(" Unable to retrieve Categories: " + errorThrown + "<br/>Refresh page. If error persists, contact administrator.");
            }); // end of $.ajax(...)
        }
    </script>
</body>
</html>
