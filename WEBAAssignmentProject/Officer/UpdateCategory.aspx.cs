﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using WEBAAssignmentProject.BrandManagement.Domain;
using WEBAAssignmentProject.CategoryManagement.Domain;
using WEBAAssignmentProject.ProductManagement.Domain;

namespace WEBAAssignmentProject.Officer
{
    public partial class UpdateCategory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        } // end of Page_Load

        [WebMethod]
        public static object DeleteOneCategory(int inCategoryId)
        {
            //Try to delete category if the number of brands linked to the Category is 0
            //else return an object with the message telling the user that deletion cannot occur
            //Template for responseObject
            //var responseObject = new { Status = "", Message = "", Data = "{JSON}"};
            string officerId = HttpContext.Current.User.Identity.GetUserId();
            CategoryManager categoryManager = new CategoryManager();
            try
            {
                if (categoryManager.DeleteOneCategory(inCategoryId, officerId))
                {
                    var responseObject = new
                    {
                        Status = "success",
                        Message = "Successfully deleted category with ID: " + inCategoryId
                    };
                    return responseObject;
                }
                else
                {
                    BrandManager brandManager = new BrandManager();
                    ProductManager productManager = new ProductManager();
                    List<Brand> brandIdList = brandManager.GetAllBrandByCategoryIdMinimal(inCategoryId);
                    List<object> productIdList = productManager.GetAllProductByCategoryIdNameValue(inCategoryId, 0);

                    object dataObject = new
                    {
                        BrandList = brandIdList,
                        ProductList = productIdList
                    };

                    var responseObject = new
                    {
                        Status = "fail",
                        Message = "Unable to delete category with ID: " + inCategoryId,
                        Data = JsonConvert.SerializeObject(dataObject)
                    };
                    return responseObject;
                } // end of else {...}
            } // end of try {...}
            catch (Exception ex)
            {
                var responseObject = new
                {
                    Status = "fail",
                    Message = "Execution error " + ex.Message
                };
                return responseObject;
            } // end of catch() {...}
        } // end of DeleteCategory

        [WebMethod]
        public static object RestoreOneCategory(int inCategoryId)
        {
            //Try to delete category if the number of brands linked to the Category is 0
            //else return an object with the message telling the user that deletion cannot occur
            //Template for responseObject
            //var responseObject = new { Status = "", Message = "", Data = "{JSON}"};
            string officerId = HttpContext.Current.User.Identity.GetUserId();
            CategoryManager categoryManager = new CategoryManager();
            try
            {
                if (categoryManager.UndeleteOneCategory(inCategoryId, officerId))
                {
                    var responseObject = new
                    {
                        Status = "success",
                        Message = "Successfully restored category with ID: " + inCategoryId
                    };
                    return responseObject;
                }
                else
                {
                    var responseObject = new
                    {
                        Status = "fail",
                        Message = "Unable to restore category with ID: " + inCategoryId
                    };
                    return responseObject;
                } // end of else {...}
            } // end of try {...}
            catch (Exception ex)
            {
                var responseObject = new
                {
                    Status = "fail",
                    Message = "Execution error: " + ex.Message
                };
                return responseObject;
            } // end of catch() {...}
        } // end of RestoreOneCategory

        [WebMethod]
        public static object UpdateOneCategory(string inWebFormData)
        {
            //Try to update category with the ID
            //if success, return success message
            //if fail, return the error message
            //Template for responseObject
            //var responseObject = new { Status = "success/fail", Message = "", Data = "{JSON}"};
            try
            {
                var webFormObject = JsonConvert.DeserializeObject<dynamic>(inWebFormData);
                Category category = new Category();
                string officerId = HttpContext.Current.User.Identity.GetUserId();
                category.CategoryId = webFormObject.CategoryId;
                category.CategoryName = webFormObject.CategoryName.Value;
                category.Visibility = Int32.Parse(webFormObject.Visibility.Value);
                category.Special = bool.Parse(webFormObject.Special.Value.ToString());

                // will only parse the datetime if the visibility is 2 which is with the start date and end date
                if (category.Visibility == 2)
                {
                    category.StartDate = DateTime.ParseExact(webFormObject.StartDate.Value, "dd/MM/yyyy",
                        CultureInfo.InvariantCulture);
                    category.EndDate = DateTime.ParseExact(webFormObject.EndDate.Value, "dd/MM/yyyy",
                        CultureInfo.InvariantCulture);
                    if (category.StartDate.Date > category.EndDate.Date)
                    {
                        throw new Exception("Start date is larger than End date. Please check your dates.");
                    }
                } // end of if (...)
                CategoryManager categoryManager = new CategoryManager();
                categoryManager.UpdateCategory(category, officerId);
                var responseObject = new
                {
                    Status = "success",
                    Message = "Successfully updated Category with the ID: " + category.CategoryId
                };
                return responseObject;
            }
            catch (Exception ex)
            {
                var responseObject = new
                {
                    Status = "fail",
                    Message = "An error occured: " + ex.Message
                };
                return responseObject;
            }
        } // end of UpdateOneCategory

        [WebMethod]
        public static object GetDetailsOfCategory(int inCategoryId)
        {
            //Get the details of the object and return it as a JSON string
            //if fail, return the error message
            //Template for responseObject
            //var responseObject = new { Status = "success/fail", Message = "", Data = "{JSON}"};
            try
            {
                CategoryManager categoryManager = new CategoryManager();
                Category categoryObject = categoryManager.GetOneCategoryById(inCategoryId);
                var responseObject = new
                {
                    Status = "success",
                    Message = "Successfully fetched details for Category with ID of " + inCategoryId,
                    Data = JsonConvert.SerializeObject(categoryObject, Formatting.None, new JsonSerializerSettings
                    {
                        DateTimeZoneHandling = DateTimeZoneHandling.Local
                    })
                };
                return responseObject;
            }
            catch (Exception ex)
            {
                var responseObject = new
                {
                    Status = "fail",
                    Message = "Error occured: " + ex.Message
                };
                return responseObject;
            }
        } // end of GetDetailsOfCategory
    } //end of Class UpdateCategory
} // end of Namespace