﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using WEBAAssignmentProject.BrandManagement.Domain;
using WEBAAssignmentProject.CategoryManagement.Domain;

namespace WEBAAssignmentProject.Officer
{
    public partial class AddBrand : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {
            }
        } // end of Page_Load

        [WebMethod]
        public static object AddOneBrand(string inWebFormData)
        {
            //Template for responseObject
            //var responseObject = new { Status = "success/fail", Message = "", Data = "{JSON}"};
            BrandManager brandManager = new BrandManager();
            Brand brand = new Brand();
            List<int> categoryIdList = new List<int>();

            //Deserialize the JSON object
            var webFormObject = JsonConvert.DeserializeObject<dynamic>(inWebFormData);
            int newBrandId = 0;
            string officerId = HttpContext.Current.User.Identity.GetUserId();

            brand.BrandName = webFormObject.BrandName.Value;
            foreach (int child in webFormObject.Categories.Children())
            //http://stackoverflow.com/questions/16045569/how-to-access-elements-of-a-jarray
            {
                categoryIdList.Add(child);
            }
            try
            {
                newBrandId = brandManager.AddOneBrand(brand, officerId, categoryIdList);
                if (newBrandId > 0)
                {
                    var responseObject = new
                    {
                        Status = "success",
                        Message = "Successfully created a new Brand record with ID of :" + newBrandId,
                        Data = JsonConvert.SerializeObject(new { newBrandId = newBrandId })
                    };
                    return responseObject;
                }
                else
                {
                    var responseObject = new
                    {
                        Status = "fail",
                        Message = "Unable to insert a new Brand record."
                    };
                    return responseObject;
                }
            }
            catch (Exception ex)
            {
                var responseObject = new
                {
                    Status = "fail",
                    Message = "Error occurred: Unable to save brand record. " + ex.Message
                };
                return responseObject;
            }
        } // end of AddBrand

        [WebMethod]
        public static object GetAllCategories()
        {
            //Mainly used for dropdownlist
            //Template for responseObject
            //var responseObject = new { Status = "success/fail", Message = "", Data = "{JSON}"};
            CategoryManager categoryManager = new CategoryManager();
            try
            {
                List<object> categoryList = categoryManager.GetAllCategoriesNameValue(0, 0);
                var responseObject = new
                {
                    Status = "success",
                    Message = "Successfully fetched Categories",
                    Data = JsonConvert.SerializeObject(categoryList)
                };
                return responseObject;
            }
            catch (Exception ex)
            {
                var responseObject = new
                {
                    Status = "fail",
                    Message = "Error occured: " + ex.Message
                };
                return responseObject;
            }
        } // end of GetAllCategories

        [WebMethod]
        public static object DestroyBrand(int inBrandId)
        {
            //tries to delete, if fails then return the error 
            //Template for responseObject
            //var responseObject = new { Status = "success/fail", Message = "", Data = "{JSON}"};
            try
            {
                string officerId = HttpContext.Current.User.Identity.GetUserId();
                BrandManager brandManager = new BrandManager();
                if (brandManager.DestroyOneBrand(inBrandId))
                {
                    var responseObject = new
                    {
                        Status = "success",
                        Message = "Successfully deleted Brand"
                    };
                    return responseObject;
                }
                else
                {
                    var responseObject = new
                    {
                        Status = "fail",
                        Message = "Unable to delete Brand"
                    };
                    return responseObject;
                }
            }
            catch (Exception ex)
            {
                var responseObject = new
                {
                    Status = "fail",
                    Message = "Error occurred: " + ex.Message
                };
                return responseObject;
            }
        } // end of DeleteBrand
    } // end of Class AddBrand
} // end of Namespace