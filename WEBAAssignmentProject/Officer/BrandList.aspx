﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BrandList.aspx.cs" Inherits="WEBAAssignmentProject.Officer.BrandList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>List of Brands</title>
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <link href="../Content/bootstrap-switch/bootstrap3/bootstrap-switch.min.css" rel="stylesheet" />
</head>
<body>
    <div class="container-fluid">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <a href="AdminPanel.aspx" class="btn btn-default navbar-btn"><span class="glyphicon glyphicon-home"></span>
                    Back to AdminPanel</a>
            </div>
        </nav>
        <div class="page-header">
            <h3>List of Brands</h3>
        </div>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <div class="input-group">
                                <span class="input-group-addon">Filter</span>
                                <select id="categoryDropDown" class="form-control"></select>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="input-group">
                                <input id="searchInput" type="text" class="form-control" placeholder="Search for..." />
                                <span class="input-group-btn">
                                    <button id="searchButton" class="btn btn-primary" type="button">Search</button>
                                </span>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <input id="showDeletedBrands" type="checkbox" name="showDeletedBrands" data-size="small"
                                data-on-color="success" data-off-color="danger" data-on-text="YES" data-off-text="NO" />&nbsp;Show deleted brands
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-12">
                <div id="messageDiv" role="alert"></div>
            </div>
            <table class="table table-hover table-striped" id="brandListTable">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Image</th>
                        <th scope="col">Brand Name</th>
                        <th scope="col">Category</th>
                        <th scope="col">Created At</th>
                        <th scope="col">Created By</th>
                        <th scope="col">Updated At</th>
                        <th scope="col">Updated By</th>
                        <th scope="col">Update</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
    <script src="../Scripts/jquery-2.1.4.min.js"></script>
    <script src="../Scripts/bootstrap.min.js"></script>
    <script src="../Scripts/moment.min.js"></script>
    <script src="../Scripts/bootstrap-switch.min.js"></script>

    <script>
        window.categoryid = getUrlValues()["categoryid"];
        window.$tableBodyElement = $("#brandListTable tbody");
        window.$categoryDropDown = $("#categoryDropDown");
        window.$messageDiv = $("#messageDiv");
        window.$searchField = $("#searchInput");
        window.$showDeletedBrands = $("#showDeletedBrands");

        $(document).ready(function () {
            window.$showDeletedBrands.bootstrapSwitch().on("switchChange.bootstrapSwitch", function () {
                TestGetAllBrands();
            });
            //Handler for dropdownlist change
            window.$categoryDropDown.change(function () {
                console.log("dropdownlist change");
                TestGetAllBrands();
            }); // end of window.$categoryDropDown.change(...)

            window.$searchField.keyup(function () {
                TestGetAllBrands();
            });
            //Handler for search button click
            $("#searchButton").on("click", function () {
                TestGetAllBrands();
            }); // end of $("#searchButton").on(...)

            //AJAX call to get Categories to populate dropdown
            $.ajax({
                url: "BrandList.aspx/GetAllCategories",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",
                async: false,
                statusCode: {
                    500: function () {
                        alert("Internal Server Error");
                    }
                }
            }).done(function (data, textStatus, jqXHR) {
                var responseObject = data.d;
                if (responseObject.Status === "success") {
                    var categoryList = JSON.parse(responseObject.Data);
                    categoryList = SortJSON(categoryList, "CategoryName", "ASC");
                    var $default = $("<option></option>").val("0").html("All Categories");
                    window.$categoryDropDown.append($default);

                    for (index = 0; index < categoryList.length; index++) {
                        var $option = $("<option></option>").val(categoryList[index].CategoryId).html(categoryList[index].CategoryName);
                        $option.appendTo(window.$categoryDropDown);
                    } // end of for {}

                    if (window.categoryid === undefined) {
                        window.$categoryDropDown.val(0).change();
                    } else {
                        window.$categoryDropDown.val(0).change();
                        window.$categoryDropDown.find("option").each(function () {
                            if ($(this).val() === window.categoryid) {
                                window.$categoryDropDown.val(window.categoryid).change();
                            }
                        });
                    }
                } else {
                    window.$messageDiv.empty().removeClass().addClass("alert alert-danger").append(responseObject.Message);
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                window.$messageDiv.empty().removeClass().addClass("alert alert-danger").append("Execution error: " + errorThrown);
            }); // end of $.ajax(...)

        }); // end of $(document).ready(...);

        //FUNCTIONS
        //Function to populate table
        function PopulateTable(inBrandList) {
            //console.log("In populate table: ");
            //console.log(inBrandList);
            window.$tableBodyElement.empty();
            for (index = 0; index < inBrandList.length; index++) {
                var $rowElement = $("<tr></tr>");
                var $cellElement = $("<td></td>").text(inBrandList[index].BrandId);
                var date;
                $rowElement.append($cellElement);
                $cellElement = $("<td></td>");
                var $imageTag = $("<img />").attr({ src: 'GetBrandImageHandler.ashx?id=' + inBrandList[index].BrandId, width: '100', alt: inBrandList[index].BrandName });
                var $anchorForImage = $("<a />").attr({ href: 'GetBrandImageHandler.ashx?id=' + inBrandList[index].BrandId }).addClass("thumbnail");
                $imageTag.appendTo($anchorForImage);
                $anchorForImage.appendTo($cellElement);
                $rowElement.append($cellElement);
                $cellElement = $("<td></td>").text(inBrandList[index].BrandName);
                $rowElement.append($cellElement);
                if (!inBrandList[index].CategoryList) {
                    $cellElement = $("<td></td>").html("<mark>No categories</mark>");
                } else {
                    $cellElement = $("<td></td>").text(inBrandList[index].CategoryList);
                }
                $rowElement.append($cellElement);
                //http://stackoverflow.com/questions/206384/format-a-microsoft-json-date/2316066#2316066
                date = new Date(inBrandList[index].CreatedAt);
                $cellElement = $("<td></td>").text(moment(date).format("DD MMM YYYY"));
                $rowElement.append($cellElement);
                $cellElement = $("<td></td>").text(inBrandList[index].CreatedBy);
                $rowElement.append($cellElement);
                date = new Date(inBrandList[index].UpdatedAt);
                $cellElement = $("<td></td>").text(moment(date).format("DD MMM YYYY"));
                $rowElement.append($cellElement);
                $cellElement = $("<td></td>").text(inBrandList[index].UpdatedBy);
                $rowElement.append($cellElement);
                var $updateButton = $("<a />").attr({ href: 'UpdateBrand.aspx?id=' + inBrandList[index].BrandId }).addClass('btn btn-default').text('Update');
                $cellElement = $("<td></td>").append($updateButton);
                $rowElement.append($cellElement);
                $rowElement.appendTo(window.$tableBodyElement);
            } // end of for {}
        }; // end of PopulateTable

        //Reference: http://stackoverflow.com/questions/4656843/jquery-get-querystring-from-url
        function getUrlValues() {
            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        } // end of getUrlValues

        function SortJSON(jsonObject, name, order) {
            //http://stackoverflow.com/questions/881510/sorting-json-by-values
            jsonObject = jsonObject.sort(function (a, b) {
                if (order === "ASC") return (a[name] > b[name]) ? 1 : ((a[name] < b[name]) ? -1 : 0);
                else return (b[name] > a[name]) ? 1 : ((b[name] < a[name]) ? -1 : 0);
            });
            return jsonObject;
        } // end of SortJSON

        function TestGetAllBrands() {
            var confirmedCatId = 0;
            var isDelete = window.$showDeletedBrands.is(":checked");
            var searchField = window.$searchField.val();

            confirmedCatId = window.$categoryDropDown.val();

            console.log(JSON.stringify({
                inDeleted: isDelete,
                inCategoryId: confirmedCatId,
                inSearchField: searchField
            }));

            $.ajax({
                url: "BrandList.aspx/TestGetAllBrands",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",
                async: false,
                statusCode: {
                    500: function () {
                        alert("Internal Server Error");
                    }
                },
                data: JSON.stringify({
                    inDeleted: isDelete,
                    inCategoryId: confirmedCatId,
                    inSearchField: searchField
                })
            }).done(function (data, textStatus, jqXHR) {
                var responseObject = data.d;
                if (responseObject.Status === "success") {
                    if (responseObject.Data !== undefined) {
                        var brandList = JSON.parse(responseObject.Data);
                        console.log(brandList);
                        if (brandList.length === 0) {
                            window.$messageDiv.empty().removeClass().addClass("alert alert-warning").append("No categories found");
                            $tableBodyElement.empty();
                        } else {
                            window.$messageDiv.addClass("hidden");
                            PopulateTable(brandList);
                        }
                    }
                } else {
                    window.$messageDiv.empty().removeClass().addClass("alert alert-danger").append(responseObject.Message);
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR);
                window.$messageDiv.removeClass().addClass("alert alert-danger").html(" Unable to retrieve Categories: " + errorThrown + "<br/>Refresh page. If error persists, contact administrator.");
            }); // end of $.ajax(...)
        }
    </script>
</body>
</html>
