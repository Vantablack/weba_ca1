﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminPanel.aspx.cs" Inherits="WEBAAssignmentProject.Officer.AdminPanel" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Administration Panel</title>
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
    <div class="container">
        <h3 class="page-header">Administration Panel</h3>
        <div class="col-sm-6">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Category Management</h3>
                </div>
                <div class="panel-body">
                    <a href="CategoryList.aspx" class="btn btn-default btn-block">Category List</a>
                    <a href="AddCategory.aspx" class="btn btn-default btn-block">Add Category</a>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Brand Management</h3>
                </div>
                <div class="panel-body">
                    <a href="BrandList.aspx" class="btn btn-default btn-block">Brand List</a>
                    <a href="AddBrand.aspx" class="btn btn-default btn-block">Add Brand</a>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Product Management</h3>
                </div>
                <div class="panel-body">
                    <a href="AddProduct.aspx" class="btn btn-default btn-block">Add Product</a>
                </div>
            </div>
        </div>
    </div>
    <script src="../Scripts/jquery-2.1.4.min.js"></script>
    <script src="../Scripts/bootstrap.min.js"></script>
</body>
</html>
