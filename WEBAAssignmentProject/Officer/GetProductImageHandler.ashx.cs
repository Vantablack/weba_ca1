﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;

namespace WEBAAssignmentProject.Officer
{
    /// <summary>
    /// Summary description for GetProductImageHandler
    /// </summary>
    public class GetProductImageHandler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            string inProductPhotoId = "";
            int imageHeight = 0;
            if (string.IsNullOrEmpty(context.Request.QueryString["id"]))
            {

            }
            else
            {
                inProductPhotoId = context.Request.QueryString["id"];
                if (!string.IsNullOrEmpty(context.Request.QueryString["height"]))
                {
                    imageHeight = Int32.Parse(context.Request.QueryString["height"]);
                }
                else
                {
                    imageHeight = 150;
                }
                byte[] productImage; //Prepare the image buffer variable (for usage by Response.BinaryWrite())
                string contentType = "";
                string fileName = "";

                //I stored the connection string inside the Web.Config.
                //Whenever I need to use it, I will just use the following code to grab it for
                //my connection object.
                string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        conn.Open();
                        cmd.Connection = conn;
                        string sql = "SELECT PhotoImage, PhotoContentType, PhotoFileName" +
                                      " FROM ProductPhoto WHERE ProductPhotoId = @inProductPhotoId";
                        cmd.CommandText = sql;
                        cmd.Parameters.Add("@inProductPhotoId", SqlDbType.Int).Value = Convert.ToInt32(inProductPhotoId);
                        cmd.Prepare();
                        SqlDataReader dr = cmd.ExecuteReader();
                        dr.Read();
                        productImage = (byte[])dr["PhotoImage"];
                        fileName = dr["PhotoFileName"].ToString();
                        contentType = dr["PhotoContentType"].ToString();
                        dr.Close();
                        conn.Close();
                        context.Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", fileName));
                        context.Response.ContentType = contentType;
                        //calling program may not want original size, but other sizes
                        //. For example orginal image size may be 120px height. But calling program
                        //wants image scalled to 50px height.
                        if (imageHeight != 0)
                        {
                            Image tempImage;
                            tempImage = ByteArrayToImagebyMemoryStream(productImage);
                            productImage = ImageToByteArraybyImageConverter(ScaleImage(tempImage, imageHeight));
                        }
                        context.Response.BinaryWrite(productImage);
                    } // end of using (SqlCommand)
                } // end of using (SqlConnection)
            } // end of else
        } // end of ProcessRequest

        public static System.Drawing.Image ScaleImage(System.Drawing.Image image, int maxHeight)
        {
            //Obtained this ScaleImage method code from:
            //http://stackoverflow.com/questions/13988269/how-can-i-generate-a-thumbnail-from-an-image-in-server-folder
            var ratio = (double)maxHeight / image.Height;
            var newWidth = (int)(image.Width * ratio);
            var newHeight = (int)(image.Height * ratio);
            var newImage = new Bitmap(newWidth, newHeight);
            using (var g = Graphics.FromImage(newImage))
            {
                g.DrawImage(image, 0, 0, newWidth, newHeight);
            }
            return newImage;
        } // end of ScaleImage

        private static byte[] ImageToByteArraybyImageConverter(System.Drawing.Image image)
        {
            //Obtained this ByteArrayToImageByMemoryStream method from:
            //http://www.morgantechspace.com/2013/11/Convert-Image-to-Byte-Array-and-Byte-Array-to-Image-in-csharp.html
            ImageConverter imageConverter = new ImageConverter();
            byte[] imageByte = (byte[])imageConverter.ConvertTo(image, typeof(byte[]));
            return imageByte;
        } // end of ImageToByteArraybyImageConverter

        public static Image ByteArrayToImagebyMemoryStream(byte[] imageByte)
        {
            //Obtained this ByteArrayToImageByMemoryStream method from:
            //http://www.morgantechspace.com/2013/11/Convert-Image-to-Byte-Array-and-Byte-Array-to-Image-in-csharp.html
            MemoryStream ms = new MemoryStream(imageByte);
            Image image = Image.FromStream(ms);
            return image;
        } // end of ByteArrayToImagebyMemoryStream

        public bool IsReusable
        {
            get { return false; }
        } // end of IsReusable
    } // end of Class GetProductImageHandler
} // end of Namespace