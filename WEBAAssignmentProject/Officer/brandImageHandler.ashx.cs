﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WEBAAssignmentProject.Officer
{
    /// <summary>
    /// Summary description for brandImageHandler
    /// </summary>
    public class brandImageHandler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            string brandId = "";
            if (context.Request.QueryString["id"] == "")
            {
                brandId = "2";
                //TODO: Error catching here
            }
            else
            {
                brandId = context.Request.QueryString["id"];
            }

            byte[] brandImage; //Prepare the image buffer variable (for usage by Response.BinaryWrite())
            string contentType = "";
            string fileName = "";

            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    string sqlText = "SELECT BrandImage, BrandContentType, BrandFileName" +
                                     " FROM Brand where BrandId = @inBrandId";
                    cmd.CommandText = sqlText;
                    cmd.Parameters.Add("@inBrandId", SqlDbType.Int).Value = Int32.Parse(brandId);
                    conn.Open();
                    cmd.Prepare();
                    SqlDataReader dr = cmd.ExecuteReader();
                    dr.Read();
                    brandImage = (byte[])dr["BrandImage"];
                    fileName = dr["BrandFileName"].ToString();
                    contentType = dr["BrandContentType"].ToString();
                    dr.Close();
                    conn.Close();
                    //to prevent the image from immediately downloading, comment out the line below
                    //context.Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", fileName));
                    context.Response.ContentType = contentType;
                    context.Response.BinaryWrite(brandImage);
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
        }  // end of ProcessRequest

        public bool IsReusable
        {
            get { return false; }
        }
    } // end of Class brandImageHandler
} // end of Namespace