﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using WEBAAssignmentProject.BrandManagement.Domain;
using WEBAAssignmentProject.ProductManagement.Domain;

namespace WEBAAssignmentProject.Officer
{
    /// <summary>
    /// Summary description for AddProductImageHandler
    /// </summary>
    public class AddProductImageHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                if (!HttpContext.Current.Request.Files.AllKeys.Any())
                //if the current request does not contain any files, then return an error
                {
                    return;
                }
                ProductManager productManager = new ProductManager();
                List<ProductPhoto> productPhotoList = new List<ProductPhoto>();
                int numOfFiles = HttpContext.Current.Request.Files.Count;
                string officerId = HttpContext.Current.User.Identity.GetUserId();
                // Get the uploaded image from the Files collection
                for (int index = 0; index < numOfFiles; index++)
                {
                    HttpPostedFile postedFile = HttpContext.Current.Request.Files[index];
                    //Reference: http://stackoverflow.com/questions/359894/how-to-create-byte-array-from-httppostedfile
                    //Converting posted file into a byte array
                    int newProductID = Int32.Parse(HttpContext.Current.Request.Form["newProductID"]);
                    using (var binaryReader = new BinaryReader(postedFile.InputStream))
                    {
                        ProductPhoto productPhoto = new ProductPhoto();
                        productPhoto.ProductId = newProductID;
                        productPhoto.PhotoFileName = postedFile.FileName;
                        productPhoto.IsPrimary = Convert.ToBoolean(HttpContext.Current.Request.Form[productPhoto.PhotoFileName]);
                        productPhoto.PhotoContentType = postedFile.ContentType;
                        productPhoto.PhotoContentLength = postedFile.ContentLength;
                        //Reads specified number of bytes from the input parameter
                        productPhoto.PhotoImage = binaryReader.ReadBytes(productPhoto.PhotoContentLength);
                        productPhotoList.Add(productPhoto);
                    }
                }
                if (productManager.AddProductPhoto(productPhotoList, officerId))
                {
                    var responseObject = new
                    {
                        Status = "success",
                        Message = "Created " + numOfFiles + " photos."
                    };
                    context.Response.ContentType = "application/json";
                    context.Response.Write(JsonConvert.SerializeObject(responseObject));
                }
                else
                {
                    var responseObject = new
                    {
                        Status = "fail",
                        Message = "Unable to add " + numOfFiles + " photos.",
                        error = "Unable to add " + numOfFiles + " photos."
                    };
                    context.Response.ContentType = "application/json";
                    context.Response.Write(JsonConvert.SerializeObject(responseObject));
                }
            }
            catch (Exception ex)
            {
                var responseObject = new
                {
                    Status = "fail",
                    Message = "An error occurred while uploading the file. Error Message: " + ex.Message,
                    //error is required for BS-Fileinput
                    //http://plugins.krajee.com/file-input#async-send
                    error = "An error occurred while uploading the file. Error Message: " + ex.Message
                };
                context.Response.ContentType = "application/json";
                context.Response.Write(JsonConvert.SerializeObject(responseObject));
            }
        } // end of ProcessRequest

        public bool IsReusable
        {
            get
            {
                return false;
            }
        } // end of IsReusable
    } // end of Class AddProductImageHandler
} // end of Namespace