﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using WEBAAssignmentProject.BrandManagement.Domain;
using WEBAAssignmentProject.CategoryManagement.Domain;
using WEBAAssignmentProject.ProductManagement.Domain;

namespace WEBAAssignmentProject.Officer
{
    public partial class UpdateBrand : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        } // end of Page_Load

        [WebMethod]
        public static object GetBrandData(int inBrandId)
        {
            //Get the brand data and return
            //Template for responseObject
            //var responseObject = new { Status = "success/fail", Message = "", Data = "{JSON}"};
            try
            {
                BrandManager brandManager = new BrandManager();
                var brandObject = new
                {
                    //Brand consists of { BrandId , BrandName }
                    Brand = brandManager.GetOneBrandById(inBrandId),
                    //Categories consists of {CategoryName, CategoryId }
                    Categories = brandManager.GetCategoriesOfBrand(inBrandId)
                };
                var responseObject = new
                {
                    Status = "success",
                    Message = "Successfully collected brand data",
                    Data = JsonConvert.SerializeObject(brandObject)
                };
                return responseObject;
            }
            catch (Exception ex)
            {
                var responseObject = new
                {
                    Status = "success",
                    Message = "Failed to collect brand data: " + ex.Message
                };
                return responseObject;
            }
        } // end of GetBrandData

        [WebMethod]
        public static object GetAllCategories()
        {
            //Mainly used for dropdownlist
            //Template for responseObject
            //var responseObject = new { Status = "success/fail", Message = "", Data = "{JSON}"};
            CategoryManager categoryManager = new CategoryManager();
            try
            {
                List<object> categoryList = categoryManager.GetAllCategoriesNameValue(0, 0);
                var responseObject = new
                {
                    Status = "success",
                    Message = "Successfully fetched Categories",
                    Data = JsonConvert.SerializeObject(categoryList)
                };
                return responseObject;
            }
            catch (Exception ex)
            {
                var responseObject = new
                {
                    Status = "fail",
                    Message = "Error occured: " + ex.Message
                };
                return responseObject;
            }
        } // end of GetAllCategories

        [WebMethod]
        public static object UpdateOneBrand(string inWebFormData)
        {
            BrandManager brandManager = new BrandManager();
            Brand brand = new Brand();
            List<int> categoryIdList = new List<int>();
            //Deserialize the JSON object
            var webFormObject = JsonConvert.DeserializeObject<dynamic>(inWebFormData);
            string officerId = HttpContext.Current.User.Identity.GetUserId();

            brand.BrandName = webFormObject.BrandName.Value;
            brand.BrandId = Int32.Parse(webFormObject.BrandId.Value);
            foreach (int child in webFormObject.Categories.Children())
            //http://stackoverflow.com/questions/16045569/how-to-access-elements-of-a-jarray
            {
                categoryIdList.Add(child);
            }
            try
            {
                brandManager.UpdateBrandWithoutImage(brand, officerId, categoryIdList);
                var responseObject = new
                {
                    Status = "success",
                    Message = "Successfully updated Brand record."
                };
                return responseObject;
            }
            catch (Exception ex)
            {
                var responseObject = new
                {
                    Status = "fail",
                    Message = "Unable to save brand record. " + ex.Message
                };
                return responseObject;
            }
        } // end of UpdateBrand

        [WebMethod]
        public static object DeleteOneBrand(int inBrandId)
        {
            //Try to delete brand if the number of products linked to it is 0
            //Template for responseObject
            //var responseObject = new { Status = "", Message = "", Data = "{JSON}"};
            string officerId = HttpContext.Current.User.Identity.GetUserId();
            BrandManager brandManager = new BrandManager();
            try
            {
                if (brandManager.DeleteOneBrand(inBrandId, officerId))
                {
                    var responseObject = new
                    {
                        Status = "success",
                        Message = "Successfully deleted category with ID: " + inBrandId
                    };
                    return responseObject;
                }
                else
                {
                    ProductManager productManager = new ProductManager();
                    List<object> productIdList = productManager.GetAllProductByBrandIdNameValue(inBrandId);

                    var responseObject = new
                    {
                        Status = "fail",
                        Message = "Unable to delete brand with ID: " + inBrandId,
                        Data = JsonConvert.SerializeObject(productIdList)
                    };
                    return responseObject;
                } // end of else {...}
            } // end of try {...}
            catch (Exception ex)
            {
                var responseObject = new
                {
                    Status = "fail",
                    Message = "Execution error: " + ex.Message
                };
                return responseObject;
            } // end of catch() {...}
        } // end of DeleteBrand

        [WebMethod]
        public static object RestoreOneBrand(int inBrandId)
        {
            //Try to delete category if the number of brands linked to the Category is 0
            //else return an object with the message telling the user that deletion cannot occur
            //Template for responseObject
            //var responseObject = new { Status = "", Message = "", Data = "{JSON}"};
            string officerId = HttpContext.Current.User.Identity.GetUserId();
            BrandManager brandManager = new BrandManager(); 
            try
            {
                if (brandManager.UndeleteOneBrand(inBrandId, officerId))
                {
                    var responseObject = new
                    {
                        Status = "success",
                        Message = "Successfully restored brand with ID: " + inBrandId
                    };
                    return responseObject;
                }
                else
                {
                    var responseObject = new
                    {
                        Status = "fail",
                        Message = "Unable to restore brand with ID: " + inBrandId
                    };
                    return responseObject;
                } // end of else {...}
            } // end of try {...}
            catch (Exception ex)
            {
                var responseObject = new
                {
                    Status = "fail",
                    Message = "Execution error: " + ex.Message
                };
                return responseObject;
            } // end of catch() {...}
        } // end of RestoreOneCategory
    } // end of Class UpdateBrand
} // end of Namespace