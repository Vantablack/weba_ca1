﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using Newtonsoft.Json;
using WEBAAssignmentProject.BrandManagement.Domain;
using WEBAAssignmentProject.CategoryManagement.Domain;
using WEBAAssignmentProject.ProductManagement.Domain;

namespace WEBAAssignmentProject.Officer
{
    public partial class BrandList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        } // end of Page_Load

        [WebMethod]
        public static object GetAllBrand()
        {
            BrandManager brandManager = new BrandManager();
            List<Brand> brandList = brandManager.GetAllBrand(0, true);
            return brandList;
        } // end of GetAllBrand

        [WebMethod]
        public static object GetAllBrandByCategoryId(string inCategoryId)
        {
            BrandManager brandManager = new BrandManager();
            List<Brand> brandList = brandManager.GetAllBrandByCategoryId(0, Int32.Parse(inCategoryId));
            return brandList;
        } // end of GetAllBrandByCategoryId

        [WebMethod]
        public static object GetAllBrandBySearchString(string inSearchString)
        {
            BrandManager brandManager = new BrandManager();
            List<Brand> brandList = brandManager.GetAllBrandBySearch(inSearchString);
            return brandList;
        } // end of GetAllBrandBySearchString

        [WebMethod]
        public static object GetAllCategories()
        {
            //Mainly used for dropdownlist
            //Template for responseObject
            //var responseObject = new { Status = "success/fail", Message = "", Data = "{JSON}"};
            CategoryManager categoryManager = new CategoryManager();
            try
            {
                List<object> categoryList = categoryManager.GetAllCategoriesNameValue(0, 3);
                var responseObject = new
                {
                    Status = "success",
                    Message = "Successfully fetched Categories",
                    Data = JsonConvert.SerializeObject(categoryList)
                };
                return responseObject;
            }
            catch (Exception ex)
            {
                var responseObject = new
                {
                    Status = "fail",
                    Message = "Error occured: " + ex.Message
                };
                return responseObject;
            }
        } // end of GetAllCategories

        [WebMethod]
        public static object TestGetAllBrands(bool inDeleted, int inCategoryId, string inSearchField)
        {
            //Gets all the rootCategory with values passed in
            //Template for responseObject
            //var responseObject = new { Status = "success/fail", Message = "", Data = "{JSON}"};
            BrandManager brandManager = new BrandManager();
            try
            {
                List<Brand> brandList = brandManager.TestGetAllBrands(inDeleted, inCategoryId, inSearchField);
                var responseObject = new
                {
                    Status = "success",
                    Message = "Successfully fetched Categories",
                    Data = JsonConvert.SerializeObject(brandList, Formatting.None, new JsonSerializerSettings
                    {
                        DateTimeZoneHandling = DateTimeZoneHandling.Local
                    })
                };
                return responseObject;
            }
            catch (Exception ex)
            {
                var responseObject = new
                {
                    Status = "fail",
                    Message = "Error occured: " + ex.Message
                };
                return responseObject;
            }
        }
    } // end of Class BrandList
} // end of Namespace