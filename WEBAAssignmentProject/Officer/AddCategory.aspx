﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddCategory.aspx.cs" Inherits="WEBAAssignmentProject.Officer.AddCategory" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Add Category</title>
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <%--<link href="../Content/bootstrap-datepicker.min.css" rel="stylesheet" />--%>
    <link href="../Content/bootstrap-datepicker3.min.css" rel="stylesheet" />
</head>
<body>
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <a href="AdminPanel.aspx" class="btn btn-default navbar-btn"><span class="glyphicon glyphicon-home"></span>
                    Back to AdminPanel</a>
            </div>
        </nav>
        <div class="col-md-12">
            <div class="page-header">
                <h1>Add Category</h1>
            </div>
            <div class="col-md-12">
                <div id="messageDiv" runat="server"></div>
            </div>
            <form class="form-horizontal" id="addCategoryForm" runat="server">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Category Name</label>
                    <div class="col-sm-6">
                        <input id="categoryNameTxtBox" name="categoryNameTxtBox" class="form-control" placeholder="Name of category" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">
                        Visibility
                    </label>
                    <div id="visibilityRadioDiv" class="col-sm-6">
                        <div class="radio-inline">
                            <label>
                                <input type="radio" name="visibility[]" value="1" />Visible (ignore start and end date)
                            </label>
                        </div>
                        <div class="radio-inline">
                            <label>
                                <input type="radio" name="visibility[]" value="2" checked="" />Visible (with start and end date)</label>
                        </div>
                        <div class="radio-inline">
                            <label>
                                <input type="radio" name="visibility[]" value="0" />Hidden</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">
                        Display Start Date
                    </label>
                    <div class="col-sm-6">
                        <div class="input-group picker-container">
                            <input id="startDateTxtBox" name="startDateTxtBox" class="form-control" placeholder="Display start date" />
                            <span class="input-group-addon" id="basic-addon2"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">
                        Display End Date
                    </label>
                    <div class="col-sm-6">
                        <div class="input-group picker-container">
                            <input id="endDateTxtBox" name="endDateTxtBox" class="form-control" placeholder="Display end date" />
                            <span class="input-group-addon" id="basic-addon2"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Special Category</label>
                    <div class="col-sm-6">
                        <div id="checkboxDiv">
                            <label class="checkbox-inline">
                                <input type="checkbox" name="specialCB" id="specialCB" />Special</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-2 col-md-offset-8">
                        <a href="AdminPanel.aspx" class="btn btn-danger">Cancel</a>
                        <button id="submitButton" class="btn btn-success">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script src="../Scripts/jquery-2.1.4.min.js"></script>
    <script src="../Scripts/bootstrap.min.js"></script>
    <script src="../Scripts/jquery.validate.min.js"></script>
    <script src="../Scripts/bootstrap-datepicker.min.js"></script>
    <script src="../Scripts/moment.min.js"></script>
    <script>
        window.$addCategoryForm = $("#addCategoryForm");
        window.$visibilityRadioDiv = $("#visibilityRadioDiv");
        window.$messageDiv = $("#messageDiv");
        window.$submitButton = $("#submitButton");
        window.$startDateTxtBox = $("#startDateTxtBox");
        window.$endDateTxtBox = $("#endDateTxtBox");

        function WebFormData(inCategoryName, inVisibility, inStartDate, inEndDate, inSpecial) {
            this.CategoryName = inCategoryName;
            this.Visibility = inVisibility;
            this.StartDate = inStartDate;
            this.EndDate = inEndDate;
            this.Special = inSpecial;
        }

        //Documentation for the form is from http://jqueryvalidation.org/validate/
        //Additional: http://www.c-sharpcorner.com/UploadFile/ee01e6/jquery-validation-with-Asp-Net-web-form/

        $(document).ready(function () {
            //Add form validation
            window.$addCategoryForm.validate({
                ignore: ':disabled',
                debug: true,
                errorClass: "text-danger bg-danger",
                validClass: "bg-success",
                submitHandler: function (form) {
                    // do other things for a valid form
                    CollectDataAndSave();
                },
                highlight: function (element, errorClass, validClass) {
                    //console.log(element + ", " + errorClass + ", " + validClass);
                    //$(element).addClass(errorClass).removeClass(validClass);
                    //$(element.form).find("label[for=" + element.id + "]")
                    //    .addClass(errorClass);
                    //console.log(element);
                    $(element.closest("div.form-group")).addClass("has-error");
                },
                unhighlight: function (element, errorClass, validClass) {
                    //console.log(element + ", " + errorClass + ", " + validClass);
                    //$(element).removeClass(errorClass).addClass(validClass);
                    //$(element.form).find("label[for=" + element.id + "]")
                    //    .removeClass(errorClass);
                    $(element.closest("div.form-group")).removeClass("has-error").addClass("has-success");
                },
                errorPlacement: function (error, element) {
                    var $formgroup = element.closest('div.input-group');
                    if ($formgroup.length === 1) {
                        error.insertAfter($formgroup);
                    } else {
                        error.insertAfter(element);
                    }
                },
                rules: {
                    categoryNameTxtBox: {
                        required: true,
                        minlength: 1
                    },
                    endDateTxtBox: {
                        required: true,
                        customDate: true
                    },
                    startDateTxtBox: {
                        required: true,
                        customDate: true
                    }
                },
                messages: {
                    categoryNameTxtBox: {
                        required: "Category name is required.",
                        minlength: jQuery.validator.format("At least {0} characters are needed")
                    },
                    startDateTxtBox: {
                        required: "Start date of the category is required."
                    },
                    endDateTxtBox: {
                        required: "End date of the category is required."
                    }
                }
            }); // end of form.validate(...);

            $.validator.addMethod("customDate", function (value, element) {
                console.log("customDate function called");
                console.log(moment(value, "DD/MM/YYYY").isValid());
                //using Moment.js is valid and date format
                //http://momentjs.com/docs/
                if (moment(value, "DD/MM/YYYY").isValid()) {
                    return true;
                } else {
                    return false;
                }
            }, "Please enter a proper date format 'DD/MM/YYYY'"); // end of $.validator.addMethod("customDate");

            //http://www.bootply.com/74352
            //Load datepicker
            window.$startDateTxtBox.datepicker({
                format: "dd/mm/yyyy",
                orientation: "top"
            }).on("changeDate", function (e) {
                window.$endDateTxtBox.datepicker("setStartDate", moment($(this).val(), "DD/MM/YYYY").toDate());
            });

            window.$endDateTxtBox.datepicker({
                format: "dd/mm/yyyy",
                orientation: "top"
            }).on("changeDate", function (e) {
                window.$startDateTxtBox.datepicker("setEndDate", moment($(this).val(), "DD/MM/YYYY").toDate());
            });

            window.$submitButton.on("click", function () {
                window.$addCategoryForm.validate();
            });

            window.$visibilityRadioDiv.find("input[type=radio]").change(function () {
                if ($(this).val() === "0" || $(this).val() === "1") {
                    window.$startDateTxtBox.prop("disabled", true);
                    window.$endDateTxtBox.prop("disabled", true);
                } else {
                    window.$startDateTxtBox.prop("disabled", false);
                    window.$endDateTxtBox.prop("disabled", false);
                }
            });
        }); // end of $(document).ready(...);


        //FUNCTIONS
        function CollectDataAndSave() {
            var visibility = window.$visibilityRadioDiv.find("input[type=radio]:checked").val();
            var categoryName = $("#categoryNameTxtBox").val();
            var startDate = $("#startDateTxtBox").val();
            var endDate = $("#endDateTxtBox").val();
            var special = $("#specialCB").is(":checked");
            console.log(visibility + ", " + categoryName + ", " + startDate + ", " + endDate + ", " + special);

            var webFormData = new WebFormData(categoryName, visibility, startDate, endDate, special);
            console.log(webFormData);
            $.ajax({
                url: "AddCategory.aspx/AddOneRootCategory",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",
                async: false,
                data: JSON.stringify({ inWebFormData: JSON.stringify(webFormData) })
            }).done(function (data, textStatus, jqXHR) {
                var responseObject = data.d;
                if (responseObject.Status === "success") {
                    window.$messageDiv.removeClass().addClass("alert alert-success").html(responseObject.Message);
                } else {
                    window.$messageDiv.removeClass().addClass("alert alert-danger").html(responseObject.Message);
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                window.$messageDiv.removeClass().addClass("alert alert-danger").html(errorThrown);
                console.log(jqXHR);
            }); // end of  $.ajax({...});
        } // end of CollectDataAndSave

    </script>
</body>
</html>
