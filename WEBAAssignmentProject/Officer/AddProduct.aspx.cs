﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using WEBAAssignmentProject.BrandManagement.Domain;
using WEBAAssignmentProject.CategoryManagement.Domain;
using WEBAAssignmentProject.ProductManagement.Domain;
using Attribute = WEBAAssignmentProject.ProductManagement.Domain.Attribute;

namespace WEBAAssignmentProject.Officer
{
    public partial class AddProduct : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        } // end of Page_Load

        [WebMethod]
        public static object GetAllBrand()
        {
            //Fetch Brand data for binding of the dropdownlist
            //Template for responseObject
            //var responseObject = new { Status = "success/fail", Message = "", Data = "{JSON}"};
            BrandManager brandManager = new BrandManager();
            try
            {
                //Get all not deleted brands
                List<object> brandList = brandManager.GetAllBrandNameValue(0);
                var responseObject = new
                {
                    Status = "success",
                    Message = "Successfully fetched all Brands",
                    Data = JsonConvert.SerializeObject(brandList)
                };
                return responseObject;
            }
            catch (Exception ex)
            {
                var responseObject = new
                {
                    Status = "fail",
                    Message = "Failed to fetch Brands: " + ex.Message
                };
                return responseObject;
            }
        } // end of GetAllBrand

        [WebMethod]
        public static object GetAllCategoriesOfBrand(int inBrandId)
        {
            //Mainly used for dropdownlist
            //Template for responseObject
            //var responseObject = new { Status = "success/fail", Message = "", Data = "{JSON}"};
            BrandManager brandManager = new BrandManager();
            try
            {
                List<Category> categoryList = brandManager.GetCategoriesOfBrand(inBrandId);
                var responseObject = new
                {
                    Status = "success",
                    Message = "Successfully fetched Categories",
                    Data = JsonConvert.SerializeObject(categoryList)
                };
                return responseObject;
            }
            catch (Exception ex)
            {
                var responseObject = new
                {
                    Status = "fail",
                    Message = "Error occured: " + ex.Message
                };
                return responseObject;
            }
        } // end of GetAllCategories

        [WebMethod]
        public static object GetAllSpecialCategories()
        {
            //Used to populate the dropdownlist for the special categories
            //Template for responseObject
            //var responseObject = new { Status = "success/fail", Message = "", Data = "{JSON}"};
            CategoryManager categoryManager = new CategoryManager();
            try
            {
                List<Category> categoryList = categoryManager.GetAllCategories(0, 1);
                var responseObject = new
                {
                    Status = "success",
                    Message = "Successfully fetched Categories",
                    Data = JsonConvert.SerializeObject(categoryList)
                };
                return responseObject;
            }
            catch (Exception ex)
            {
                var responseObject = new
                {
                    Status = "fail",
                    Message = "Error occured: " + ex.Message
                };
                return responseObject;
            }
        } // end of GetAllSpecialCategories

        [WebMethod]
        public static object AddOneProduct(string inWebFormData)
        {
            //Deserialize inWebFormData
            // {
            //   ProductName: "", ProductBrand: "", ProductCategories : "[...]"
            //   SpecialCategories : [...], ProductAddons: [{...}], 
            //   ProductPricings: [{...}] 
            // }
            //Parse the webFormObject into a Product class
            Product product = new Product();
            string officerId = HttpContext.Current.User.Identity.GetUserId();
            product.Brand = new Brand();
            product.Pricing = new List<Pricing>();
            product.Attributes = new List<Attribute>();
            product.Categories = new List<Category>();

            var webFormObject = JsonConvert.DeserializeObject<dynamic>(inWebFormData);
            product.ProductName = webFormObject.ProductName.Value;
            product.Brand.BrandId = Int32.Parse(webFormObject.ProductBrand.Value);

            //Getting the attributes of the product
            //http://www.newtonsoft.com/json/help/html/T_Newtonsoft_Json_Linq_JArray.htm
            var productAddons = webFormObject.ProductAddons;
            foreach (var item in productAddons.Children())
            {
                Attribute attribute = new Attribute();
                attribute.AttributeId = Int32.Parse(item.AttributeId.Value);
                attribute.AtrributeContent = item.AttributeContent.Value;
                product.Attributes.Add(attribute);
            }

            //Getting the pricings of the product
            var productPricings = webFormObject.ProductPricings;
            foreach (var pricing in productPricings.Children())
            {
                Pricing price = new Pricing();
                if (pricing.Weight != null)
                {
                    price.Weight = Int32.Parse(pricing.Weight.Value);
                    price.WeightType = pricing.WeightType.Value;
                }
                price.Price = Int32.Parse(pricing.Price.Value);
                price.Quantity = Int32.Parse(pricing.Quantity.Value);
                product.Pricing.Add(price);
            }

            //Getting the categories of the product
            var productCategories = webFormObject.ProductCategories;
            foreach (var item in productCategories)
            {
                Category category = new Category();
                category.CategoryId = Int32.Parse(item.Value);
                product.Categories.Add(category);
            }

            //If there are special categories, add it into the product
            if (webFormObject.SpecialCategories != null)
            {
                product.SpecialCategories = new List<Category>();
                foreach (var specialCategory in webFormObject.SpecialCategories)
                {
                    Category category = new Category();
                    category.CategoryId = Int32.Parse(specialCategory.Value);
                    product.SpecialCategories.Add(category);
                }
            }

            try
            {
                ProductManager productManager = new ProductManager();
                int returnedProductId = productManager.AddProduct(product, officerId);
                var data = new { newProductID = returnedProductId };
                var responseObject = new
                {
                    Status = "success",
                    Message = "Successfully added a new Product",
                    Data = JsonConvert.SerializeObject(data)
                };
                return responseObject;
            }
            catch (Exception ex)
            {
                var responseObject = new
                {
                    Status = "fail",
                    Message = "Error occured: " + ex.Message
                };
                return responseObject;
            }
        } // end of AddOneProduct

        [WebMethod]
        public static object GetAllAttributes()
        {
            try
            {
                ProductManager productManager = new ProductManager();
                string attributeJson = JsonConvert.SerializeObject(productManager.GetAllAttributes());
                var responseObject = new
                {
                    Status = "success",
                    Message = "Successfully retrieved all Attributes",
                    Data = attributeJson
                };
                return responseObject;
            }
            catch (Exception ex)
            {
                var responseObject = new
                {
                    Status = "fail",
                    Message = "Error occurred: " + ex.Message
                };
                return responseObject;
            }
        } // end of GetAllAttributes

        [WebMethod]
        public static object AddOneAttribute(string inAttributeName)
        {
            try
            {
                ProductManager productManager = new ProductManager();
                Attribute attribute = new Attribute();
                attribute.AttributeName = inAttributeName;
                productManager.AddAttribute(attribute);
                var responseObject = new
                {
                    Status = "success",
                    Message = "Successfully added attribute"
                };
                return responseObject;
            }
            catch (Exception ex)
            {
                var responseObject = new
                {
                    Status = "fail",
                    Message = "Error occurred: " + ex.Message
                };
                return responseObject;
            }
        } // end of AddOneAttribute
    } // end of class AddProduct
} // end of Namespace