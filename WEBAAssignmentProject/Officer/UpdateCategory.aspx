﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UpdateCategory.aspx.cs" Inherits="WEBAAssignmentProject.Officer.UpdateCategory" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Update Category</title>
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <link href="../Content/bootstrap-datepicker.min.css" rel="stylesheet" />
    <link href="../Content/bootstrap-datepicker3.min.css" rel="stylesheet" />
</head>
<body>
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <a href="AdminPanel.aspx" class="btn btn-default navbar-btn"><span class="glyphicon glyphicon glyphicon-home"></span>
                    Back to AdminPanel</a>
                <a href="CategoryList.aspx" class="btn btn-default navbar-btn"><span class="glyphicon glyphicon-chevron-left"></span>
                    Back to CategoryList</a>
            </div>
        </nav>
        <div class="col-md-12">
            <div class="page-header">
                <h1>Update Category</h1>
            </div>
            <div class="col-md-12">
                <div id="messageDiv"></div>
            </div>
            <form class="form-horizontal" id="updateCategoryForm">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Category Name</label>
                    <div class="col-sm-6">
                        <input id="categoryNameTxtBox" name="categoryNameTxtBox" class="form-control" placeholder="Name of category" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">
                        Visibility
                    </label>
                    <div id="visibilityRadioDiv" class="col-sm-6">
                        <div class="radio-inline">
                            <label>
                                <input type="radio" name="visibility[]" value="1" />Visible (ignore start and end date)
                            </label>
                        </div>
                        <div class="radio-inline">
                            <label>
                                <input type="radio" name="visibility[]" value="2" checked="" />Visible (with start and end date)
                            </label>
                        </div>
                        <div class="radio-inline">
                            <label>
                                <input type="radio" name="visibility[]" value="0" />Hidden
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">
                        Display Start Date
                    </label>
                    <div class="col-sm-6">
                        <div class="input-group picker-container">
                            <input id="startDateTxtBox" name="startDateTxtBox" class="form-control" placeholder="Display start date" />
                            <span class="input-group-addon" id="basic-addon2"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">
                        Display End Date
                    </label>
                    <div class="col-sm-6">
                        <div class="input-group picker-container">
                            <input id="endDateTxtBox" name="endDateTxtBox" class="form-control" placeholder="Display end date" />
                            <span class="input-group-addon" id="basic-addon2"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Special Category</label>
                    <div class="col-sm-6">
                        <div id="checkboxDiv">
                            <label class="checkbox-inline">
                                <input type="checkbox" name="specialCB" id="specialCB" />Special</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-2 col-md-offset-2">
                        <button id="deleteButton" type="button" class="btn btn-default">
                            <span class="glyphicon glyphicon-trash"></span>&nbsp;Delete
                        </button>
                    </div>
                    <div class="col-md-2 col-md-offset-4">
                        <a href="CategoryList.aspx" class="btn btn-danger">Cancel</a>
                        <button id="submitButton" class="btn btn-success">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script src="../Scripts/jquery-2.1.4.min.js"></script>
    <script src="../Scripts/bootstrap.min.js"></script>
    <script src="../Scripts/jquery.validate.min.js"></script>
    <script src="../Scripts/bootstrap-datepicker.min.js"></script>
    <script src="../Scripts/moment.min.js"></script>

    <script>
        window.CategoryId = getUrlValues()["id"];
        window.$messageDiv = $("#messageDiv");
        window.$updateCategoryForm = $("#updateCategoryForm");
        window.$visibilityRadioDiv = $("#visibilityRadioDiv");
        window.$submitButton = $("#submitButton");
        window.$startDateTxtBox = $("#startDateTxtBox");
        window.$endDateTxtBox = $("#endDateTxtBox");
        window.$categoryNameTxtBox = $("#categoryNameTxtBox");

        function WebFormData(inCategoryName, inVisibility, inStartDate, inEndDate, inSpecial) {
            this.CategoryId = window.CategoryId;
            this.CategoryName = inCategoryName;
            this.Visibility = inVisibility;
            this.StartDate = inStartDate;
            this.EndDate = inEndDate;
            this.Special = inSpecial;
        }

        $(document).ready(function () {
            if (window.CategoryId === undefined) {
                window.location.href = "CategoryList.aspx";
            }

            GetAndFillCategoryDetails();

            window.$visibilityRadioDiv.find("input[type=radio]").change(function () {
                if ($(this).val() === "0" || $(this).val() === "1") {
                    window.$startDateTxtBox.prop("disabled", true);
                    window.$endDateTxtBox.prop("disabled", true);
                } else {
                    window.$startDateTxtBox.prop("disabled", false);
                    window.$endDateTxtBox.prop("disabled", false);
                }
            });

            //Load datepicker
            window.$startDateTxtBox.datepicker({
                format: "dd/mm/yyyy",
                orientation: "top"
            }).on("changeDate", function (e) {
                window.$endDateTxtBox.datepicker("setStartDate", moment($(this).val(), "DD/MM/YYYY").toDate());
            });
            window.$endDateTxtBox.datepicker({
                format: "dd/mm/yyyy",
                orientation: "top"
            }).on("changeDate", function (e) {
                window.$startDateTxtBox.datepicker("setEndDate", moment($(this).val(), "DD/MM/YYYY").toDate());
            });

            window.$submitButton.on("click", function () {
                window.$updateCategoryForm.validate();
            });

            //Event handler for delete button
            $("#deleteButton").on("click", function () {
                $.ajax({
                    url: 'UpdateCategory.aspx/DeleteOneCategory',
                    type: 'POST',
                    data: JSON.stringify({ inCategoryId: window.CategoryId }),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'JSON',
                    async: false
                }).done(function (data, textStatus, jqXHR) {
                    var responseObject = data.d;
                    var $button = $('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
                    var $clone;
                    if (responseObject.Status === 'success') {
                        $clone = window.$messageDiv.clone().removeAttr("id").removeClass()
                            .addClass("alert alert-success alert-dismissible fade in").empty();
                        var $header = $("<h4><span class='glyphicon glyphicon-thumbs-up'></span>&nbsp;Success</h4>");
                        var $message = $("<p></p>").prepend($header).append(responseObject.Message);
                        $clone.append($button).append($message).alert();
                        GetAndFillCategoryDetails();
                    } else {
                        $clone = window.$messageDiv.clone().removeAttr("id").removeClass()
                            .addClass("alert alert-danger alert-dismissible fade in").empty();
                        $clone.append($button).alert();
                        var $header = $("<h4><span class='glyphicon glyphicon-thumbs-down'></span>&nbsp;Oh no!</h4>");
                        var $message = $("<p></p>").prepend($header).append(responseObject.Message);
                        $clone.append($message);

                        //If there are data inside the object
                        if (responseObject.Data !== undefined) {
                            var responseData = JSON.parse(responseObject.Data);
                            //if BrandList is not null
                            if (responseData.BrandList !== undefined) {
                                var $brandMessage = $("<p>Brands affected are:</p>");
                                $clone.append($brandMessage);

                                var $brandPara = $("<p></p>");
                                for (index = 0; index < responseData.BrandList.length; index++) {
                                    var $linkButton = $("<a />").attr({ href: "UpdateBrand.aspx?id=" + responseData.BrandList[index].BrandId, "class": "btn btn-default" }).text(responseData.BrandList[index].BrandName);
                                    $brandPara.append("&nbsp;");
                                    $brandPara.append($linkButton);
                                }
                                $clone.append($brandPara);
                            }
                            if (responseData.ProductList !== undefined) {
                                var $prodMessage = $("<p>Products affected are:</p>");
                                $clone.append($prodMessage);

                                var $prodPara = $("<p></p>");
                                for (index = 0; index < responseData.ProductList.length; index++) {
                                    var $linkButton = $("<a />").attr({ href: "#", "class": "btn btn-default" }).text(responseData.ProductList[index].ProductName);
                                    $prodPara.append("&nbsp;");
                                    $prodPara.append($linkButton);
                                }
                                $clone.append($prodPara);
                            }
                        } // end of if (responseObject.Data !== undefined)
                    } // end of if (responseObject.Status === 'success')
                    $clone.insertAfter(window.$messageDiv);
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    var $button = $('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>');

                    var $clone = window.$messageDiv.clone().removeAttr("id").removeClass()
                        .addClass("alert alert-success alert-dismissible fade in").empty();
                    var $header = $("<h4><span class='glyphicon glyphicon-thumbs-up'></span>&nbsp;Success</h4>");
                    var $message = $("<p></p>").prepend($header).append(errorThrown);
                    $clone.append($button).append($message).alert();

                    $clone.insertAfter(window.$messageDiv);
                    console.log("Fail:");
                    console.log(jqXHR);
                    console.log(textStatus);
                    console.log(errorThrown);
                }); // end of  $.ajax(...);
            }); // end of $("#deleteButton").on("click");

            window.$updateCategoryForm.validate({
                ignore: ":disabled",
                debug: true,
                errorClass: "text-danger bg-danger",
                validClass: "bg-success",
                submitHandler: function (form) {
                    CollectDataAndSave();
                },
                highlight: function (element, errorClass, validClass) {
                    $(element.closest("div.form-group")).addClass("has-error");
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element.closest("div.form-group")).removeClass("has-error").addClass("has-success");
                },
                errorPlacement: function (error, element) {
                    var $placement = element.closest('div.input-group');
                    if ($placement.length === 1) {
                        error.insertAfter($placement);
                    } else {
                        error.insertAfter(element);
                    }
                },
                rules: {
                    categoryNameTxtBox: {
                        required: true,
                        minlength: 1
                    },
                    endDateTxtBox: {
                        required: true,
                        customDate: true
                    },
                    startDateTxtBox: {
                        required: true,
                        customDate: true
                    }
                },
                messages: {
                    categoryNameTxtBox: {
                        required: "Category name is required.",
                        minlength: jQuery.validator.format("At least {0} characters are needed")
                    },
                    startDateTxtBox: {
                        required: "Start date of the category is required."
                    },
                    endDateTxtBox: {
                        required: "End date of the category is required."
                    }
                }
            }); // end of window.$updateCategoryForm.validate(...);

            $.validator.addMethod("customDate", function (value, element) {
                console.log("customDate function called");
                console.log(moment(value, "DD/MM/YYYY").isValid());
                //using Moment.js is valid and date format
                //http://momentjs.com/docs/
                if (moment(value, "DD/MM/YYYY").isValid()) {
                    return true;
                } else {
                    return false;
                }
            }, "Please enter a proper date format 'DD/MM/YYYY'"); // end of $.validator.addMethod("customDate");

        }); // end of $(document).ready(...);

        //FUNCTIONS
        //Reference: http://stackoverflow.com/questions/4656843/jquery-get-querystring-from-url
        function getUrlValues() {
            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        } // end of getUrlValues

        function GetAndFillCategoryDetails() {
            $.ajax({
                url: "UpdateCategory.aspx/GetDetailsOfCategory",
                type: "POST",
                async: false,
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",
                statusCode: {
                    500: function () {
                        alert("Internal Server Error");
                    }
                },
                data: JSON.stringify({ inCategoryId: window.CategoryId })
            }).done(function (data, textStatus, jqXHR) {
                var responseObject = data.d;
                if (responseObject.Status === "success") {
                    var categoryObject = JSON.parse(responseObject.Data);
                    window.$categoryNameTxtBox.val(categoryObject.CategoryName);
                    //http://stackoverflow.com/questions/16892382/jquery-checkbox-on-change-event-doesnt-fire-if-checked-using-jquery
                    window.$visibilityRadioDiv.find("input[value=" + categoryObject.Visibility + "]:radio").prop("checked", true).change();
                    if (categoryObject.Visibility === 2) {
                        window.$startDateTxtBox.datepicker("update", new Date(categoryObject.StartDate)).datepicker("setEndDate", new Date(categoryObject.EndDate));
                        window.$endDateTxtBox.datepicker("update", new Date(categoryObject.EndDate)).datepicker("setStartDate", new Date(categoryObject.StartDate));
                    }
                    if (categoryObject.Deleted) {
                        var $cloneButton = $("#deleteButton").clone().removeAttr("id").html("&nbsp;Restore");
                        var $span = $("#deleteButton").find("span").removeClass().addClass("glyphicon glyphicon-repeat");
                        $cloneButton.prepend($span);
                        $cloneButton.off("click").on("click", function () {
                            $.ajax({
                                url: "UpdateCategory.aspx/RestoreOneCategory",
                                type: "POST",
                                async: false,
                                contentType: "application/json; charset=utf-8",
                                dataType: "JSON",
                                statusCode: {
                                    500: function () {
                                        alert("Internal Server Error");
                                    }
                                },
                                data: JSON.stringify({ inCategoryId: window.CategoryId })
                            }).done(function (data, textStatus, jqXHR) {
                                location.reload(true);
                            }).fail(function (jqXHR, textStatus, errorThrown) {
                                //console.log(jqXHR);
                                window.$messageDiv.removeClass().addClass("alert alert-danger").html("Unable to restore Category: " + errorThrown);
                            }); // end of $.ajax(...)
                        });
                        $("#deleteButton").replaceWith($cloneButton);
                    }
                    $("#specialCB").prop("checked", categoryObject.Special);
                } else {
                    window.$messageDiv.empty().removeClass().addClass("alert alert-danger").append(responseObject.Message);
                    window.$submitButton.prop("disabled", true);
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                //console.log(jqXHR);
                window.$messageDiv.removeClass().addClass("alert alert-danger").html(" Unable to retrieve category details: " + errorThrown + "<br/>Refresh page. If error persists, contact administrator.");
            }); // end of $.ajax(...)
        }; // end of GetAndFillCategoryDetails

        function CollectDataAndSave() {
            var visibility = window.$visibilityRadioDiv.find("input[type=radio]:checked").val();
            var categoryName = $("#categoryNameTxtBox").val();
            var startDate = $("#startDateTxtBox").val();
            var endDate = $("#endDateTxtBox").val();
            var special = $("#specialCB").is(":checked");

            var webFormData = new WebFormData(categoryName, visibility, startDate, endDate, special);
            console.log(webFormData);

            $.ajax({
                url: "UpdateCategory.aspx/UpdateOneCategory",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "JSON",
                async: false,
                data: JSON.stringify({ inWebFormData: JSON.stringify(webFormData) }),
                statusCode: {
                    500: function () {
                        alert("Internal Server Error");
                    }
                }
            }).done(function (data, textStatus, jqXHR) {
                var responseObject = data.d;
                var $clone;
                var $button = $('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
                if (responseObject.Status === "success") {
                    $clone = window.$messageDiv.clone().removeAttr("id").removeClass()
                            .addClass("alert alert-success alert-dismissible fade in").empty();
                    var $header = $("<h4><span class='glyphicon glyphicon-thumbs-up'></span>&nbsp;Success</h4>");
                    var $message = $("<p></p>").prepend($header).append(responseObject.Message);
                    $clone.append($button).append($message).alert();
                } else {
                    $clone = window.$messageDiv.clone().removeAttr("id").removeClass()
                            .addClass("alert alert-danger alert-dismissible fade in").empty();
                    var $header = $("<h4><span class='glyphicon glyphicon-thumbs-down'></span>&nbsp;Oh no!</h4>");
                    var $message = $("<p></p>").prepend($header).append(responseObject.Message);
                    $clone.append($button).append($message).alert();
                }
                $clone.insertAfter(window.$messageDiv);
            }).fail(function (jqXHR, textStatus, errorThrown) {
                window.$messageDiv.removeClass().addClass("alert alert-danger").html(errorThrown);
                console.log(jqXHR);
            }); // end of  $.ajax({...});
        }; // end of CollectDataAndSave
    </script>
</body>
</html>
