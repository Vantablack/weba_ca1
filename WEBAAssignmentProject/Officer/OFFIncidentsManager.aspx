﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OFFIncidentsManager.aspx.cs" Inherits="WEBAAssignmentProject.Officer.OFFIncidentsManager" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>View Incidents</title>
     <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <style>
        body{margin-top:65px;}
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="container">
   <div class="row">
   <div class="col-md-offset-1 col-md-10">
    <div class="panel panel-info">
                    <div class="panel-heading"><h3>My Incident Records
                         <a href="OFFCreateNewIncident.aspx" class="btn btn-default pull-right" >Create Incident Record</a>
                                               </h3>
                       
                    </div>
                    
                    <div class="panel-body">
                        
    <asp:GridView CssClass="table table-striped" ID="dataGrid" runat="server" AutoGenerateColumns="false" >
        <Columns>
            <asp:BoundField DataField="IncidentId" HeaderText="Id" />
            <asp:BoundField DataField="Title" HeaderText="Title" />
            <asp:BoundField DataField="Description" HeaderText="Description" />
            <asp:BoundField DataField="CreatedAt" HeaderText="Created At" />
            <asp:BoundField DataField="CreatedBy" HeaderText="Created By" />
        </Columns>
    </asp:GridView>
                        </div><!-- end of panel-body css class -->
              </div><!-- end of panel css class -->
   </div>
    </div>
   </div>
    </form>
</body>
</html>
