﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using System.Security.Claims;
using WEBAAssignmentProject.IncidentManagement.Domain;

namespace WEBAAssignmentProject.Officer
{
    public partial class OFFCreateNewIncident : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void saveButton_Click(object sender, EventArgs e)
        {
            string collectedTitle = titleInput.Text;
            string collectedDescription = descriptionInput.Text;
            //Obtain the current user login unique id.
            string currentUserId = User.Identity.GetUserId();
            int newIncidentRecordId = 0;
            IncidentManager incidentManager = new IncidentManager();
            newIncidentRecordId = incidentManager.AddNewIncident(collectedTitle, collectedDescription, currentUserId);
            if (newIncidentRecordId > 0)
            {
                messageBox.InnerHtml = "Added new record. The new record id is " + newIncidentRecordId.ToString();
                messageBox.InnerHtml += "<br /><i>This is just a sample test message to check the new record id value. We should not display the value of the new record id to the user.</i>";
            }
            else
            {
                messageBox.InnerHtml = "Something went wrong inside the code.";
            }
        }
    }
}