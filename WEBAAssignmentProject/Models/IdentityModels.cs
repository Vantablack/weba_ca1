﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;

namespace WEBAAssignmentProject.Models
{

    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        //http://www.codeproject.com/Tips/991663/Displaying-User-Full-Name-instead-of-User-Email-in
        //http://blog.falafel.com/customize-mvc-5-application-users-using-asp-net-identity-2-0/
        //Referenced the above link to find out how to customize the ApplicationUser class.
        //Date: 10 Nov 2015 12 PM By:Tan Hu Shien

        public const string DisplayNameClaimType = "FullName";

        [Required]
        [MaxLength(128)]
        public string FullName { get; set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            //We add the display name so that the _LoginPartial can pick it up;
            userIdentity.AddClaim(new Claim(DisplayNameClaimType, FullName));
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}