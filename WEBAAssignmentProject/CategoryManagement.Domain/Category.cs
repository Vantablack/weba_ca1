﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEBAAssignmentProject.CategoryManagement.Domain
{
    public class Category
    {
        public int CategoryId { get; set; }
        public int Visibility { get; set; }
        public string CategoryName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public int SubCategoryCount { get; set; }
        public bool Special { get; set; }
        public bool Deleted { get; set; }
    } // end of Class Category
} // end of Namespace