﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Microsoft.Ajax.Utilities;

namespace WEBAAssignmentProject.CategoryManagement.Domain
{
    public class CategoryManager
    {
        //NEW
        public List<Category> GetAllCategoriesFiltered(int inDeleteFlag, int inVisibility)
        {
            //Selects all the root categories from the Category table
            //Method caller has to input the DeleteFlag and the Visibility

            List<Category> categoryList = new List<Category>();
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    string sqlText = "SELECT C.CategoryId, C.CategoryName, U1.FullName AS CreatedByName,"
                                     + " U2.FullName AS UpdatedByName, C.CreatedAt, C.UpdatedAt,"
                                     + " C.DisplayStart, C.DisplayEnd, C.Visibility,"
                                     + " SubQuery.SubCategoryCount FROM Category AS C"
                                     + " LEFT OUTER JOIN AspNetUsers AS U1 ON C.CreatedBy = U1.Id"
                                     + " LEFT OUTER JOIN AspNetUsers AS U2 ON C.UpdatedBy = U2.Id"
                                     + " INNER JOIN"
                                     + " (SELECT C.CategoryId, COUNT(B.ParentCategoryId) SubCategoryCount"
                                     + " FROM Category C LEFT JOIN Category B ON C.CategoryId = B.ParentCategoryId"
                                     + " GROUP BY C.CategoryId) AS SubQuery ON C.CategoryId = SubQuery.CategoryId"
                                     + " WHERE C.DeleteFlag = @inDeleteFlag AND C.Visibility = @inVisibility";
                    cmd.CommandText = sqlText;
                    cmd.Parameters.Add("@inDeleteFlag", SqlDbType.Bit).Value = inDeleteFlag;
                    cmd.Parameters.Add("@inVisibility", SqlDbType.Int).Value = inVisibility;
                    SqlDataAdapter da = new SqlDataAdapter();
                    DataSet ds = new DataSet();
                    da.SelectCommand = cmd;
                    da.Fill(ds, "CategoryData");
                    foreach (DataRow row in ds.Tables["CategoryData"].Rows)
                    {
                        Category category = new Category();
                        category.CategoryId = Int32.Parse(row["CategoryId"].ToString());
                        category.CategoryName = row["CategoryName"].ToString();
                        category.CreatedBy = row["CreatedByName"].ToString();
                        category.UpdatedBy = row["UpdatedByName"].ToString();
                        category.CreatedAt = DateTime.Parse(row["CreatedAt"].ToString());
                        category.UpdatedAt = DateTime.Parse(row["UpdatedAt"].ToString());
                        category.Visibility = Int32.Parse(row["Visibility"].ToString());
                        category.SubCategoryCount = Int32.Parse(row["SubCategoryCount"].ToString());
                        if (category.Visibility == 2)
                        {
                            //Parse the start date and end date only when the Visibility is
                            //With the start date and end date
                            category.StartDate = DateTime.Parse(row["DisplayStart"].ToString());
                            category.EndDate = DateTime.Parse(row["DisplayEnd"].ToString());
                        }
                        categoryList.Add(category);
                    } // end of foreach
                    conn.Close();
                    return categoryList;
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
        } // end if GetAllCategoriesFiltered

        //NEW
        public List<Category> GetAllCategories(int inDeleteFlag, int inSpecial)
        {
            //Selects all the root categories from the Category table
            //Method caller has to input the DeleteFlag

            List<Category> categoryList = new List<Category>();
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    string sqlText = "";
                    if (inSpecial != 3)
                    {
                        sqlText = "SELECT C.CategoryId, C.CategoryName, U1.FullName AS CreatedByName,"
                                  + " U2.FullName AS UpdatedByName, C.CreatedAt, C.UpdatedAt,"
                                  + " C.DisplayStart, C.DisplayEnd, C.Visibility,"
                                  + " SubQuery.SubCategoryCount FROM Category AS C"
                                  + " LEFT OUTER JOIN AspNetUsers AS U1 ON C.CreatedBy = U1.Id"
                                  + " LEFT OUTER JOIN AspNetUsers AS U2 ON C.UpdatedBy = U2.Id"
                                  + " INNER JOIN"
                                  + " (SELECT C.CategoryId, COUNT(B.ParentCategoryId) SubCategoryCount"
                                  + " FROM Category C LEFT JOIN Category B ON C.CategoryId = B.ParentCategoryId"
                                  + " GROUP BY C.CategoryId) AS SubQuery ON C.CategoryId = SubQuery.CategoryId"
                                  + " WHERE C.DeleteFlag = @inDeleteFlag AND C.IsSpecial = @inSpecial";
                        cmd.Parameters.Add("@inSpecial", SqlDbType.Bit).Value = inSpecial;
                    }
                    else
                    {
                        sqlText = "SELECT C.CategoryId, C.CategoryName, U1.FullName AS CreatedByName,"
                                  + " U2.FullName AS UpdatedByName, C.CreatedAt, C.UpdatedAt,"
                                  + " C.DisplayStart, C.DisplayEnd, C.Visibility,"
                                  + " SubQuery.SubCategoryCount FROM Category AS C"
                                  + " LEFT OUTER JOIN AspNetUsers AS U1 ON C.CreatedBy = U1.Id"
                                  + " LEFT OUTER JOIN AspNetUsers AS U2 ON C.UpdatedBy = U2.Id"
                                  + " INNER JOIN"
                                  + " (SELECT C.CategoryId, COUNT(B.ParentCategoryId) SubCategoryCount"
                                  + " FROM Category C LEFT JOIN Category B ON C.CategoryId = B.ParentCategoryId"
                                  + " GROUP BY C.CategoryId) AS SubQuery ON C.CategoryId = SubQuery.CategoryId"
                                  + " WHERE C.DeleteFlag = @inDeleteFlag";
                    }

                    cmd.CommandText = sqlText;
                    cmd.Parameters.Add("@inDeleteFlag", SqlDbType.Bit).Value = inDeleteFlag;

                    SqlDataAdapter da = new SqlDataAdapter();
                    DataSet ds = new DataSet();
                    da.SelectCommand = cmd;
                    da.Fill(ds, "CategoryData");
                    foreach (DataRow row in ds.Tables["CategoryData"].Rows)
                    {
                        Category category = new Category();
                        category.CategoryId = Int32.Parse(row["CategoryId"].ToString());
                        category.CategoryName = row["CategoryName"].ToString();
                        category.CreatedBy = row["CreatedByName"].ToString();
                        category.UpdatedBy = row["UpdatedByName"].ToString();
                        category.CreatedAt = DateTime.Parse(row["CreatedAt"].ToString());
                        category.UpdatedAt = DateTime.Parse(row["UpdatedAt"].ToString());
                        category.Visibility = Int32.Parse(row["Visibility"].ToString());
                        category.SubCategoryCount = Int32.Parse(row["SubCategoryCount"].ToString());
                        if (category.Visibility == 2)
                        {
                            //Parse the start date and end date only when the Visibility is
                            //With the start date and end date
                            category.StartDate = DateTime.Parse(row["DisplayStart"].ToString());
                            category.EndDate = DateTime.Parse(row["DisplayEnd"].ToString());
                        }
                        categoryList.Add(category);
                    } // end of foreach
                    conn.Close();
                    return categoryList;
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
        } // end if GetAllCategories

        //NEW
        public List<Category> GetAllCategoriesBySearch(int inDeleteFlag, int inVisibility, string inSearchField)
        {
            //Selects all the root categories from the Category table
            //Method caller has to input the DeleteFlag and the Visibility and the Search field

            List<Category> categoryList = new List<Category>();
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    string sqlText = "SELECT C.CategoryId, C.CategoryName, U1.FullName AS CreatedByName,"
                                     + " U2.FullName AS UpdatedByName, C.CreatedAt, C.UpdatedAt,"
                                     + " C.DisplayStart, C.DisplayEnd, C.Visibility,"
                                     + " SubQuery.SubCategoryCount FROM Category AS C"
                                     + " LEFT OUTER JOIN AspNetUsers AS U1 ON C.CreatedBy = U1.Id"
                                     + " LEFT OUTER JOIN AspNetUsers AS U2 ON C.UpdatedBy = U2.Id"
                                     + " INNER JOIN"
                                     + " (SELECT C.CategoryId, COUNT(B.ParentCategoryId) SubCategoryCount"
                                     + " FROM Category C LEFT JOIN Category B ON C.CategoryId = B.ParentCategoryId"
                                     + " GROUP BY C.CategoryId) AS SubQuery ON C.CategoryId = SubQuery.CategoryId"
                                     + " WHERE C.DeleteFlag = @inDeleteFlag AND C.Visibility = @inVisibility"
                                     + " AND C.CategoryName LIKE @inSearchField";
                    if (inVisibility == 3)
                    {
                        sqlText = "SELECT C.CategoryId, C.CategoryName, U1.FullName AS CreatedByName,"
                                  + " U2.FullName AS UpdatedByName, C.CreatedAt, C.UpdatedAt,"
                                  + " C.DisplayStart, C.DisplayEnd, C.Visibility,"
                                  + " SubQuery.SubCategoryCount FROM Category AS C"
                                  + " LEFT OUTER JOIN AspNetUsers AS U1 ON C.CreatedBy = U1.Id"
                                  + " LEFT OUTER JOIN AspNetUsers AS U2 ON C.UpdatedBy = U2.Id"
                                  + " INNER JOIN"
                                  + " (SELECT C.CategoryId, COUNT(B.ParentCategoryId) SubCategoryCount"
                                  + " FROM Category C LEFT JOIN Category B ON C.CategoryId = B.ParentCategoryId"
                                  + " GROUP BY C.CategoryId) AS SubQuery ON C.CategoryId = SubQuery.CategoryId"
                                  + " WHERE C.DeleteFlag = @inDeleteFlag AND C.CategoryName LIKE @inSearchField";
                    }
                    cmd.CommandText = sqlText;
                    cmd.Parameters.Add("@inDeleteFlag", SqlDbType.Bit).Value = inDeleteFlag;
                    cmd.Parameters.Add("@inVisibility", SqlDbType.Int).Value = inVisibility;
                    cmd.Parameters.Add("@inSearchField", SqlDbType.VarChar).Value = "%" + inSearchField + "%";
                    SqlDataAdapter da = new SqlDataAdapter();
                    DataSet ds = new DataSet();
                    da.SelectCommand = cmd;
                    da.Fill(ds, "CategoryData");
                    foreach (DataRow row in ds.Tables["CategoryData"].Rows)
                    {
                        Category category = new Category();
                        category.CategoryId = Int32.Parse(row["CategoryId"].ToString());
                        category.CategoryName = row["CategoryName"].ToString();
                        category.CreatedBy = row["CreatedByName"].ToString();
                        category.UpdatedBy = row["UpdatedByName"].ToString();
                        category.CreatedAt = DateTime.Parse(row["CreatedAt"].ToString());
                        category.UpdatedAt = DateTime.Parse(row["UpdatedAt"].ToString());
                        category.Visibility = Int32.Parse(row["Visibility"].ToString());
                        category.SubCategoryCount = Int32.Parse(row["SubCategoryCount"].ToString());
                        if (category.Visibility == 2)
                        {
                            //Parse the start date and end date only when the Visibility is
                            //With the start date and end date
                            category.StartDate = DateTime.Parse(row["DisplayStart"].ToString());
                            category.EndDate = DateTime.Parse(row["DisplayEnd"].ToString());
                        }
                        categoryList.Add(category);
                    } // end of foreach
                    conn.Close();
                    return categoryList;
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
        } // end of GetAllCategoriesBySearch

        //NEW
        public List<object> GetAllCategoriesNameValue(int inDeleteFlag, int inSpecial)
        {
            // Summary:
            // Get all the categories that are not deleted
            // Returns the Id and Name
            List<object> categoryList = new List<object>();
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    string sqlCommand = "";
                    if (inSpecial != 3)
                    {
                        sqlCommand = "SELECT * FROM Category WHERE Category.DeleteFlag = @inDeleteFlag AND Category.IsSpecial = @inSpecial"
                                     + " ORDER BY CategoryName DESC;";
                        cmd.Parameters.Add("@inSpecial", SqlDbType.Bit).Value = inSpecial;
                    }
                    else
                    {
                        sqlCommand = "SELECT * FROM Category WHERE Category.DeleteFlag = @inDeleteFlag"
                                     + " ORDER BY CategoryName DESC;";
                    }
                    cmd.CommandText = sqlCommand;
                    cmd.Parameters.Add("@inDeleteFlag", SqlDbType.Bit).Value = inDeleteFlag;

                    SqlDataAdapter da = new SqlDataAdapter();
                    DataSet ds = new DataSet();
                    da.SelectCommand = cmd;
                    da.Fill(ds, "CategoryData");
                    foreach (DataRow row in ds.Tables["CategoryData"].Rows)
                    {
                        object category = new
                        {
                            CategoryId = Int32.Parse(row["CategoryId"].ToString()),
                            CategoryName = row["CategoryName"].ToString()
                        };
                        categoryList.Add(category);
                    } // end of foreach
                    conn.Close();
                    return categoryList;
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
        } // end of GetAllCategoriesMinimal

        //NEW
        public List<object> GetCategoriesWithBrandNameValue()
        {
            // Summary:
            // Get all the categories that have a brand
            // Returns the Id and Name
            List<object> categoryList = new List<object>();
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    string sqlText = "SELECT DISTINCT Category.CategoryId, Category.CategoryName"
                                     + " FROM Category"
                                     +
                                     " INNER JOIN Category_Brand ON Category.CategoryId = Category_Brand.FK_CategoryId";
                    cmd.CommandText = sqlText;
                    SqlDataAdapter da = new SqlDataAdapter();
                    DataSet ds = new DataSet();
                    da.SelectCommand = cmd;
                    da.Fill(ds, "CategoryData");
                    foreach (DataRow row in ds.Tables["CategoryData"].Rows)
                    {
                        object category = new
                        {
                            CategoryId = Int32.Parse(row["CategoryId"].ToString()),
                            CategoryName = row["CategoryName"].ToString()
                        };
                        categoryList.Add(category);
                    } // end of foreach
                    conn.Close();
                    return categoryList;
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
        } // end of GetCategoriesWithBrandNameValue

        //NEW
        public Category GetOneCategoryById(int inCategoryId)
        {
            // Summary:

            Category category = new Category();
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "SELECT * FROM Category WHERE CategoryId = @inCategoryId";
                    cmd.Parameters.Add("@inCategoryId", SqlDbType.Int).Value = inCategoryId;
                    conn.Open();
                    try
                    {
                        SqlDataAdapter da = new SqlDataAdapter();
                        da.SelectCommand = cmd;
                        DataSet ds = new DataSet();
                        da.Fill(ds, "CategoryData");
                        DataRow row = ds.Tables["CategoryData"].Rows[0];
                        category.CategoryId = Int32.Parse(row["CategoryId"].ToString());
                        category.CategoryName = row["CategoryName"].ToString();
                        category.Visibility = Int32.Parse(row["Visibility"].ToString());
                        category.Special = (bool)row["IsSpecial"];
                        category.Deleted = (bool)row["DeleteFlag"];
                        if (category.Visibility == 2)
                        {
                            //Parse the start date and end date only when the Visibility is
                            //With the start date and end date
                            category.StartDate = DateTime.Parse(row["DisplayStart"].ToString());
                            category.EndDate = DateTime.Parse(row["DisplayEnd"].ToString());
                        }
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.Contains("There is no row at position"))
                        {
                            throw new System.ArgumentException(
                                "There seems to be no Category with the specified ID of: " + inCategoryId);
                        }
                    }
                    finally
                    {
                        conn.Close();
                    }
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
            return category;
        } // end of GetOneCategoryById

        //NEW
        public int AddOneRootCategory(Category inCategory, string inCreatedBy)
        {
            // Inserts the category as a root/parent category
            // Summary:
            // Category table
            // [CategoryId],[ParentCategoryId],[CategoryName],[DisplayStart],[DisplayEnd],[DeleteFlag],
            // [Visibility],[CreatedBy],[UpdatedBy],[DeletedBy],[CreatedAt],[UpdatedAt],[DeletedAt]
            // Insert into these fields:
            // [CategoryName],[CreatedBy],[UpdatedBy],[Visibility]
            // Return the value
            int returnedId = 0;
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    string alternativeSqlText = "INSERT INTO Category (CategoryName, Visibility, CreatedBy, UpdatedBy, DisplayStart, DisplayEnd, IsSpecial)"
                                                +
                                                " VALUES (@inCategoryName, @inVisibility, @inCreatedBy, @inUpdatedBy, @inDisplayStart, @inDisplayEnd, @inSpecial);" +
                                                "SELECT SCOPE_IDENTITY();";
                    cmd.CommandText = "INSERT INTO Category (CategoryName, Visibility, CreatedBy, UpdatedBy)"
                                      +
                                      " VALUES (@inCategoryName, @inVisibility, @inCreatedBy, @inUpdatedBy); SELECT SCOPE_IDENTITY();";
                    cmd.Parameters.Add("@inCategoryName", SqlDbType.VarChar, 50).Value = inCategory.CategoryName;
                    cmd.Parameters.Add("@inCreatedBy", SqlDbType.NVarChar, 128).Value = inCreatedBy;
                    cmd.Parameters.Add("@inUpdatedBy", SqlDbType.NVarChar, 128).Value = inCreatedBy;
                    cmd.Parameters.Add("@inVisibility", SqlDbType.Int).Value = inCategory.Visibility;
                    cmd.Parameters.Add("@inSpecial", SqlDbType.Bit).Value = inCategory.Special;

                    if (inCategory.Visibility == 2)
                    {
                        cmd.CommandText = alternativeSqlText;
                        cmd.Parameters.Add("@inDisplayStart", SqlDbType.DateTime).Value = inCategory.StartDate;
                        cmd.Parameters.Add("@inDisplayEnd", SqlDbType.DateTime).Value = inCategory.EndDate;
                    }
                    conn.Open();
                    try
                    {
                        returnedId = Int32.Parse(cmd.ExecuteScalar().ToString());
                    }
                    catch (SqlException ex)
                    {
                        if (ex.Message.Contains("UC_CategoryName"))
                        {
                            throw new ArgumentException(
                                "Unable to insert into the database due to duplicate category name: " +
                                inCategory.CategoryName);
                        }
                    }
                    finally
                    {
                        conn.Close();
                    }
                    if (returnedId == 0)
                    {
                        throw new ArgumentException(
                            "Something really bad happened, the returnedId is 0, please contact to administrator.");
                    }
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
            return returnedId;
        } //end of AddOneRootCategory

        //NEW
        public bool UpdateCategory(Category inCategory, string inUpdatedBy)
        {
            // Summary: 
            //
            bool status = false;
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "UPDATE Category SET CategoryName = @inCategoryName,"
                                      + "UpdatedBy = @inUpdatedBy, UpdatedAt = GETDATE(),"
                                      + "DisplayStart = NULL, DisplayEnd = NULL, IsSpecial = @inSpecial,"
                                      + "Visibility = @inVisibility WHERE CategoryId = @inCategoryId";
                    if (inCategory.Visibility == 2)
                    {
                        cmd.CommandText = "UPDATE Category SET CategoryName = @inCategoryName,"
                                          + "UpdatedBy = @inUpdatedBy, UpdatedAt = GETDATE(),"
                                          +
                                          "DisplayStart = @inDisplayStart, DisplayEnd = @inDisplayEnd, IsSpecial = @inSpecial,"
                                          + "Visibility = @inVisibility WHERE CategoryId = @inCategoryId";
                        cmd.Parameters.Add("@inDisplayStart", SqlDbType.DateTime).Value = inCategory.StartDate;
                        cmd.Parameters.Add("@inDisplayEnd", SqlDbType.DateTime).Value = inCategory.EndDate;
                    }
                    cmd.Parameters.Add("@inCategoryId", SqlDbType.Int).Value = inCategory.CategoryId;
                    cmd.Parameters.Add("@inCategoryName", SqlDbType.VarChar, 50).Value = inCategory.CategoryName;
                    cmd.Parameters.Add("@inUpdatedBy", SqlDbType.NVarChar, 128).Value = inUpdatedBy;
                    cmd.Parameters.Add("@inVisibility", SqlDbType.Int).Value = inCategory.Visibility;
                    cmd.Parameters.Add("@inSpecial", SqlDbType.Bit).Value = inCategory.Special;

                    conn.Open();
                    try
                    {
                        int numOfRowsAffected = cmd.ExecuteNonQuery();
                        if (numOfRowsAffected == 1)
                        {
                            status = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.Contains("UC_CategoryName"))
                        {
                            throw new ArgumentException(
                                "Unable to insert into the database due to duplicate Category names.");
                        }
                    }
                    finally
                    {
                        conn.Close();
                    }
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
            return status;
        } // end of UpdateCategory

        //NEW - Transaction
        public bool DeleteOneCategory(int inCategoryId, string inDeletedBy)
        {
            // Summary:
            //Using an if else, make sure that the row count of Category_Brand and Category_Product is 0
            //before marking as deleted
            bool status = false;
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    conn.Open();
                    // Start a local transaction.
                    SqlTransaction transaction = conn.BeginTransaction("transaction");
                    cmd.Connection = conn;
                    cmd.Transaction = transaction;

                    string checkAndDeleteText =
                        "IF (" +
                        " (SELECT COUNT(*) AS 'RowCount' FROM Category_Brand WHERE Category_Brand.FK_CategoryId = @inCategoryId) = 0" +
                        " AND" +
                        " (SELECT COUNT(*) AS 'RowCount' FROM Category_Product WHERE Category_Product.FK_CategoryId = @inCategoryId) = 0" +
                        " )" +
                        " UPDATE Category SET DeleteFlag = 1, DeletedAt = GETDATE(), DeletedBy = @inDeletedBy, UpdatedBy = @inDeletedBy, UpdatedAt = GETDATE()" +
                        " WHERE CategoryId = @inCategoryId";

                    try
                    {
                        cmd.CommandText = checkAndDeleteText;
                        cmd.Parameters.Add("@inCategoryId", SqlDbType.Int).Value = inCategoryId;
                        cmd.Parameters.Add("@inDeletedBy", SqlDbType.NVarChar).Value = inDeletedBy;

                        int rowsAffected = cmd.ExecuteNonQuery();
                        status = (rowsAffected > 0);
                        //No errors, commit the database
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw;
                    }
                    finally
                    {
                        conn.Close();
                    }
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
            return status;
        } // end of DeleteOneCategory

        public bool UndeleteOneCategory(int inCategoryId, string inUpdatedBy)
        {
            // Summary:
            //before marking as deleted
            bool status = false;
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    conn.Open();
                    // Start a local transaction.
                    SqlTransaction transaction = conn.BeginTransaction("transaction");
                    cmd.Connection = conn;
                    cmd.Transaction = transaction;

                    string checkAndDeleteText =
                        "UPDATE Category SET DeleteFlag = 0, DeletedAt = NULL, DeletedBy = NULL, UpdatedBy = @inUpdatedBy, UpdatedAt = GETDATE()" +
                        " WHERE CategoryId = @inCategoryId";

                    try
                    {
                        cmd.CommandText = checkAndDeleteText;
                        cmd.Parameters.Add("@inCategoryId", SqlDbType.Int).Value = inCategoryId;
                        cmd.Parameters.Add("@inUpdatedBy", SqlDbType.NVarChar).Value = inUpdatedBy;

                        int rowsAffected = cmd.ExecuteNonQuery();
                        status = (rowsAffected > 0);
                        //No errors, commit the database
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw;
                    }
                    finally
                    {
                        conn.Close();
                    }
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
            return status;
        } // end of UndeleteOneCategory

        public List<Category> TestGetAllCategories(int inVisibility, string inSearchField, bool inSpecial, bool inDeleteFlag)
        {
            //Selects all the root categories from the Category table
            //Method caller has to input the DeleteFlag

            List<Category> categoryList = new List<Category>();
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    string defaultSqlText = "SELECT C.CategoryId, C.CategoryName, U1.FullName AS CreatedByName,"
                                  + " U2.FullName AS UpdatedByName, C.CreatedAt, C.UpdatedAt,"
                                  + " C.DisplayStart, C.DisplayEnd, C.Visibility,"
                                  + " SubQuery.SubCategoryCount FROM Category AS C"
                                  + " LEFT OUTER JOIN AspNetUsers AS U1 ON C.CreatedBy = U1.Id"
                                  + " LEFT OUTER JOIN AspNetUsers AS U2 ON C.UpdatedBy = U2.Id"
                                  + " INNER JOIN"
                                  + " (SELECT C.CategoryId, COUNT(B.ParentCategoryId) SubCategoryCount"
                                  + " FROM Category C LEFT JOIN Category B ON C.CategoryId = B.ParentCategoryId"
                                  + " GROUP BY C.CategoryId) AS SubQuery ON C.CategoryId = SubQuery.CategoryId"
                                  + " WHERE C.DeleteFlag = @inDeleteFlag AND C.IsSpecial = @inSpecial";

                    cmd.Parameters.Add("@inSpecial", SqlDbType.Bit).Value = inSpecial;
                    cmd.Parameters.Add("@inDeleteFlag", SqlDbType.Bit).Value = inDeleteFlag;
                    if (inVisibility != 3) //this means that user wants to filter my visibility too
                    {
                        defaultSqlText += " AND C.Visibility = @inVisibility";
                        cmd.Parameters.Add("@inVisibility", SqlDbType.Int).Value = inVisibility;
                    }
                    if (!inSearchField.IsNullOrWhiteSpace()) //if inSearchField is not null or white space
                    {
                        defaultSqlText += " AND C.CategoryName LIKE @inSearchField";
                        cmd.Parameters.Add("@inSearchField", SqlDbType.VarChar).Value = "%" + inSearchField + "%";
                    }
                    cmd.CommandText = defaultSqlText;
                    SqlDataAdapter da = new SqlDataAdapter();
                    DataSet ds = new DataSet();
                    da.SelectCommand = cmd;
                    da.Fill(ds, "CategoryData");
                    foreach (DataRow row in ds.Tables["CategoryData"].Rows)
                    {
                        Category category = new Category();
                        category.CategoryId = Int32.Parse(row["CategoryId"].ToString());
                        category.CategoryName = row["CategoryName"].ToString();
                        category.CreatedBy = row["CreatedByName"].ToString();
                        category.UpdatedBy = row["UpdatedByName"].ToString();
                        category.CreatedAt = DateTime.Parse(row["CreatedAt"].ToString());
                        category.UpdatedAt = DateTime.Parse(row["UpdatedAt"].ToString());
                        category.Visibility = Int32.Parse(row["Visibility"].ToString());
                        category.SubCategoryCount = Int32.Parse(row["SubCategoryCount"].ToString());
                        if (category.Visibility == 2)
                        {
                            //Parse the start date and end date only when the Visibility is
                            //With the start date and end date
                            category.StartDate = DateTime.Parse(row["DisplayStart"].ToString());
                            category.EndDate = DateTime.Parse(row["DisplayEnd"].ToString());
                        }
                        categoryList.Add(category);
                    } // end of foreach
                    conn.Close();
                    return categoryList;
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
        } // end of TestGetAllCategories
    } //end of Class CategoryManager
} // end of Namespace