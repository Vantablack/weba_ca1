﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WEBAAssignmentProject.BrandManagement.Domain;
using WEBAAssignmentProject.CategoryManagement.Domain;

namespace WEBAAssignmentProject.ProductManagement.Domain
{
    public class Product
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public Brand Brand { get; set; }
        public List<Pricing> Pricing { get; set; }
        public List<Attribute> Attributes { get; set; }
        public List<Category> Categories { get; set; }
        public List<Category> SpecialCategories { get; set; }
    } // end of class Product
} // end of Namespace