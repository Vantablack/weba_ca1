﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEBAAssignmentProject.ProductManagement.Domain
{
    public class Attribute
    {
        public int AttributeId { get; set; }
        public string AttributeName { get; set; }
        public string AtrributeContent { get; set; }
    } // end of Class Attribute
} // end of Namespace