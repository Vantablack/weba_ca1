﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEBAAssignmentProject.ProductManagement.Domain
{
    public class ProductPhoto
    {
        public int ProductPhotoId { get; set; }
        public byte[] PhotoImage { get; set; }
        public int PhotoContentLength { get; set; }
        public string PhotoContentType { get; set; }
        public string PhotoFileName { get; set; }
        public int ProductId { get; set; }
        public bool IsPrimary { get; set; }
    } // end of class ProductPhoto
} // end of Namespace