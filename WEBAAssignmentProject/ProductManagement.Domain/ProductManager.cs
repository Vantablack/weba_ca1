﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WEBAAssignmentProject.CategoryManagement.Domain;

namespace WEBAAssignmentProject.ProductManagement.Domain
{
    public class ProductManager
    {
        public int AddProduct(Product inProduct, string inCreatedBy)
        {
            //First inserts into the Product table, return the ID,
            //For each product pricing, add into the ProductPrice table,
            //while inserting, return the ProductPriceId,
            //and 
            int returnedProductId = 0;
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    conn.Open();
                    // Start a local transaction.
                    SqlTransaction transaction = conn.BeginTransaction("test-transaction");
                    // Must assign both transaction object and connection
                    // to Command object for a pending local transaction
                    cmd.Connection = conn;
                    cmd.Transaction = transaction;

                    string productSqlText = "INSERT INTO Product"
                                            + " (FK_BrandId, ProductName,"
                                            + " CreatedBy, UpdatedBy) OUTPUT INSERTED.ProductId"
                                            + " VALUES (@inBrandId, @inProductName,"
                                            + " @inCreatedBy, @inCreatedBy);";
                    string priceSqlText =
                        "INSERT INTO ProductPrice (FK_ProductId, Price) OUTPUT INSERTED.ProductPriceId VALUES (@inProductId, @inPrice);";

                    string attributeSqlText = "INSERT INTO AttributeValue (FK_AttributeId, Value) "
                                              + " OUTPUT INSERTED.AttributeValueId"
                                              + " VALUES (@inAttributeId, @inValue);";

                    string prodPriceAttrSqlText = "INSERT INTO ProductPriceAttribute (FK_ProductPriceId, FK_AttributeValueId)"
                                                  + " VALUES (@inProductPriceId, @inAttributeValueId);";
                    string prodCategorySqlText = "INSERT INTO Category_Product (FK_CategoryId, FK_ProductId)"
                                                 + " VALUES (@inCategoryId, @inProductId);";
                    try
                    {
                        //Add into the Product table first
                        cmd.CommandText = productSqlText;
                        //Parameters for the product
                        cmd.Parameters.Add("@inBrandId", SqlDbType.Int).Value = inProduct.Brand.BrandId;
                        cmd.Parameters.Add("@inProductName", SqlDbType.VarChar).Value = inProduct.ProductName;
                        cmd.Parameters.Add("@inCreatedBy", SqlDbType.NVarChar).Value = inCreatedBy;
                        cmd.Parameters.Add("@inUpdatedBy", SqlDbType.NVarChar).Value = inCreatedBy;
                        returnedProductId = (int)cmd.ExecuteScalar();

                        //Inserting shared attributes
                        cmd.CommandText = attributeSqlText;
                        List<int> attributeValues = new List<int>();
                        foreach (Attribute attribute in inProduct.Attributes)
                        {
                            cmd.Parameters.Clear();
                            cmd.Parameters.Add("@inAttributeId", SqlDbType.Int).Value = attribute.AttributeId;
                            cmd.Parameters.Add("@inValue", SqlDbType.VarChar).Value = attribute.AtrributeContent;
                            attributeValues.Add((int)cmd.ExecuteScalar());
                        }

                        cmd.CommandText = prodCategorySqlText;
                        //Adding of the normal categories
                        foreach (Category category in inProduct.Categories)
                        {
                            cmd.Parameters.Clear();
                            cmd.Parameters.Add("@inCategoryId", SqlDbType.Int).Value = category.CategoryId;
                            cmd.Parameters.Add("@inProductId", SqlDbType.Int).Value = returnedProductId;
                            cmd.ExecuteNonQuery();
                        }

                        //Adding of the special categories
                        if (inProduct.SpecialCategories != null)
                        {
                            foreach (Category specialCategory in inProduct.SpecialCategories)
                            {
                                cmd.Parameters.Clear();
                                cmd.Parameters.Add("@inCategoryId", SqlDbType.Int).Value = specialCategory.CategoryId;
                                cmd.Parameters.Add("@inProductId", SqlDbType.Int).Value = returnedProductId;
                                cmd.ExecuteNonQuery();
                            }
                        }

                        //Getting the attribute values
                        cmd.CommandText = "SELECT * FROM Attribute WHERE AttributeName = 'Weight';";
                        int weightAttrId = (int)cmd.ExecuteScalar();
                        cmd.CommandText = "SELECT * FROM Attribute WHERE AttributeName = 'Weight Unit';";
                        int weightUnitAttrId = (int)cmd.ExecuteScalar();
                        cmd.CommandText = "SELECT * FROM Attribute WHERE AttributeName = 'Quantity';";
                        int quantityAttrId = (int)cmd.ExecuteScalar();

                        foreach (Pricing pricing in inProduct.Pricing)
                        {
                            cmd.CommandText = priceSqlText;
                            //clear parameters
                            cmd.Parameters.Clear();
                            cmd.Parameters.Add("@inProductId", SqlDbType.Int).Value = returnedProductId;
                            cmd.Parameters.Add("@inPrice", SqlDbType.Int).Value = pricing.Price;
                            int returnedPriceId = (int)cmd.ExecuteScalar();

                            //add quantity, weight(optional) and weightunit (optional)
                            cmd.CommandText = attributeSqlText;
                            if (pricing.Weight != 0)
                            {
                                cmd.Parameters.Clear();
                                //Adding weight
                                cmd.Parameters.Add("@inAttributeId", SqlDbType.Int).Value = weightAttrId;
                                cmd.Parameters.Add("@inValue", SqlDbType.Int).Value = pricing.Weight;
                                attributeValues.Add((int)cmd.ExecuteScalar());
                                //Adding weight unit
                                cmd.Parameters.Clear();
                                cmd.Parameters.Add("@inAttributeId", SqlDbType.Int).Value = weightUnitAttrId;
                                cmd.Parameters.Add("@inValue", SqlDbType.VarChar).Value = pricing.WeightType;
                                attributeValues.Add((int)cmd.ExecuteScalar());
                            }
                            //Adding quantity
                            cmd.Parameters.Clear();
                            cmd.Parameters.Add("@inAttributeId", SqlDbType.Int).Value = quantityAttrId;
                            cmd.Parameters.Add("@inValue", SqlDbType.Int).Value = pricing.Quantity;
                            attributeValues.Add((int)cmd.ExecuteScalar());

                            //foreach attribute, add into the link table
                            cmd.CommandText = prodPriceAttrSqlText;
                            foreach (int attributeValue in attributeValues)
                            {
                                cmd.Parameters.Clear();
                                cmd.Parameters.Add("@inProductPriceId", SqlDbType.Int).Value = returnedPriceId;
                                cmd.Parameters.Add("@inAttributeValueId", SqlDbType.Int).Value = attributeValue;
                                cmd.ExecuteNonQuery();
                            }
                        }
                        // Attempt to commit the transaction.
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        if (ex.Message.Contains("UC_ProductName"))
                        {
                            throw new System.ArgumentException("Existing product name inside database. | " +
                                                               ex.StackTrace);
                        }
                        throw;
                    }
                    finally
                    {
                        conn.Close();
                    }
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
            return returnedProductId;
        } // end of AddProduct

        public bool AddProductPhoto(List<ProductPhoto> inProductPhotoList, string inOfficerId)
        {
            bool status = false;
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    conn.Open();
                    cmd.Connection = conn;
                    string insertSqlText = "INSERT INTO ProductPhoto (FK_ProductId, PhotoContentLength, PhotoContentType,"
                                           + " PhotoFileName, PhotoImage, IsPrimaryPhoto, CreatedBy, UpdatedBy) VALUES"
                                           +
                                           " (@inProductId, @inPhotoContentLength, @inPhotoContentType, @inPhotoFileName,"
                                           + " @inPhotoImage, @inIsPrimaryPhoto, @inCreatedBy, @inCreatedBy)";

                    SqlTransaction transaction = conn.BeginTransaction("transaction");
                    // Must assign both transaction object and connection
                    // to Command object for a pending local transaction
                    cmd.CommandText = insertSqlText;
                    cmd.Transaction = transaction;
                    try
                    {
                        foreach (ProductPhoto productPhoto in inProductPhotoList)
                        {
                            cmd.Parameters.Clear();
                            cmd.Parameters.Add("@inProductId", SqlDbType.Int).Value = productPhoto.ProductId;
                            cmd.Parameters.Add("@inPhotoContentLength", SqlDbType.Int).Value =
                                productPhoto.PhotoContentLength;
                            cmd.Parameters.Add("@inPhotoContentType", SqlDbType.VarChar, 100).Value =
                                productPhoto.PhotoContentType;
                            cmd.Parameters.Add("@inPhotoFileName", SqlDbType.VarChar, 100).Value =
                                productPhoto.PhotoFileName;
                            cmd.Parameters.Add("@inPhotoImage", SqlDbType.VarBinary).Value = productPhoto.PhotoImage;
                            cmd.Parameters.Add("@inIsPrimaryPhoto", SqlDbType.Bit).Value = productPhoto.IsPrimary;
                            cmd.Parameters.Add("@inCreatedBy", SqlDbType.NVarChar, 128).Value = inOfficerId;
                            if (cmd.ExecuteNonQuery() == 0)
                            {
                                throw new Exception("Number of rows affected is 0, something is terribly wrong.");
                            }
                        }
                        transaction.Commit();
                        status = true;
                    }
                    catch (Exception ex)
                    {
                        // Attempt to roll back the transaction.
                        transaction.Rollback();
                        status = false;
                    }
                    finally
                    {
                        conn.Close();
                    }
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
            return status;
        } // end of AddProductPhoto

        public void AddAttribute(Attribute inAttribute)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    string sqlText = "INSERT INTO Attribute (AttributeName) VALUES (@inAttributeName);";
                    cmd.CommandText = sqlText;
                    cmd.Parameters.Add("@inAttributeName", SqlDbType.VarChar, 50).Value = inAttribute.AttributeName;
                    try
                    {
                        conn.Open();
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.Contains("UC_AttributeName"))
                        {
                            throw new Exception("Attribute name exists in database.");
                        }
                    }
                    finally
                    {
                        conn.Close();
                    }
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
        } // end of AddAttribute

        public List<Attribute> GetAllAttributes()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            List<Attribute> attributeList = new List<Attribute>();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    string sqlText = "SELECT * FROM Attribute;";
                    cmd.CommandText = sqlText;
                    try
                    {
                        conn.Open();
                        SqlDataAdapter da = new SqlDataAdapter();
                        DataSet ds = new DataSet();
                        da.SelectCommand = cmd;
                        da.Fill(ds, "AttributeData");
                        foreach (DataRow dataRow in ds.Tables["AttributeData"].Rows)
                        {
                            Attribute attribute = new Attribute();
                            attribute.AttributeId = Int32.Parse(dataRow["AttributeId"].ToString());
                            attribute.AttributeName = dataRow["AttributeName"].ToString();
                            attributeList.Add(attribute);
                        }
                    }
                    finally
                    {
                        conn.Close();
                    }
                    return attributeList;
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
        } // end of GetAllAttributes

        public List<object> GetAllProductByCategoryIdNameValue(int inCategoryId, int inDeleteFlag)
        {
            List<object> productList = new List<object>();
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    conn.Open();
                    cmd.Connection = conn;
                    string getProductSqlText = "SELECT Product.ProductId, Product.ProductName" +
                                               " FROM Category_Product INNER JOIN" +
                                               " Product ON Category_Product.FK_ProductId = Product.ProductId" +
                                               " WHERE Category_Product.FK_CategoryId = @inCategoryId" +
                                               " AND Product.DeleteFlag = @inDeleteFlag;";
                    string getAll = "SELECT Product.ProductId, Product.ProductName" +
                                    " FROM Category_Product INNER JOIN" +
                                    " Product ON Category_Product.FK_ProductId = Product.ProductId" +
                                    " WHERE Category_Product.FK_CategoryId = @inCategoryId;";
                    try
                    {
                        if (inDeleteFlag != 3)
                        {
                            cmd.CommandText = getProductSqlText;
                            cmd.Parameters.Add("@inCategoryId", SqlDbType.Int).Value = inCategoryId;
                            cmd.Parameters.Add("@inDeleteFlag", SqlDbType.Bit).Value = inDeleteFlag;
                        }
                        else
                        {
                            cmd.CommandText = getAll;
                            cmd.Parameters.Add("@inCategoryId", SqlDbType.Int).Value = inCategoryId;
                        }
                        SqlDataAdapter da = new SqlDataAdapter();
                        DataSet ds = new DataSet();
                        da.SelectCommand = cmd;
                        da.Fill(ds, "ProductData");
                        foreach (DataRow dataRow in ds.Tables["ProductData"].Rows)
                        {
                            object product = new
                            {
                                ProductId = Int32.Parse(dataRow["ProductId"].ToString()),
                                ProductName = dataRow["ProductName"].ToString()
                            };
                            productList.Add(product);
                        }
                    }
                    finally
                    {
                        conn.Close();
                    }
                    return productList;
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
        } // end of GetAllProductByCategoryId

        public List<object> GetAllProductByBrandIdNameValue(int inBrandId)
        {
            List<object> productList = new List<object>();
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    conn.Open();
                    cmd.Connection = conn;
                    string getProductSqlText = "SELECT ProductId, ProductName, FK_BrandId" +
                                               " FROM  Product" +
                                               " WHERE (FK_BrandId = @inBrandId);";
                    try
                    {
                        cmd.CommandText = getProductSqlText;
                        cmd.Parameters.Add("@inBrandId", SqlDbType.Int).Value = inBrandId;
                        SqlDataAdapter da = new SqlDataAdapter();
                        DataSet ds = new DataSet();
                        da.SelectCommand = cmd;
                        da.Fill(ds, "ProductData");
                        foreach (DataRow dataRow in ds.Tables["ProductData"].Rows)
                        {
                            object product = new
                            {
                                ProductId = Int32.Parse(dataRow["ProductId"].ToString()),
                                ProductName = dataRow["ProductName"].ToString()
                            };
                            productList.Add(product);
                        }
                    }
                    finally
                    {
                        conn.Close();
                    }
                    return productList;
                } // end of using (SqlCommand)
            } // end of using (SqlConnection)
        } // end of GetAllProductByCategoryId
    } // end of class ProductManager
} // end of Namespace