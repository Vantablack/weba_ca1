﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEBAAssignmentProject.ProductManagement.Domain
{
    public class Pricing
    {
        public int Weight { get; set; }
        public string WeightType { get; set; }
        public int Price { get; set; }
        public int Quantity { get; set; }
    } // end of Class Pricing
} // end of Namespace