﻿using WEBAAssignmentProject.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.EntityFramework;

namespace WEBAAssignmentProject.Controllers
{
    public class SeedController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private ApplicationDbContext context;
        //Copied from AccountController so that the AddUser() action methods can work
        public SeedController()
        {
            context = new ApplicationDbContext();
        }
        //Copied from AccountController so that the AddUser() action methods can work
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        //Copied from AccountController so that the AddUser() action methods can work
        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }
        // GET: Seed
        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Account/AddUser
        public ActionResult AddUser()
        {
            //ViewBag.Name = new SelectList(context.Roles.ToList(), "Name", "Name");
            return View();
        }

        //
        // POST: /Account/AddUser
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddUser(AddUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                //http://blog.falafel.com/customize-mvc-5-application-users-using-asp-net-identity-2-0/
                var user = new ApplicationUser { UserName = model.UserName, Email = model.Email, FullName = model.FullName };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    //The AddUser() Action method here is slightly different from the AccountController because, I need to
                    //seed one user with administrator role into the database. Which means, I need to create the first role record,
                    //which is administrator first.
                    IdentityRole role = new IdentityRole();
                    role.Name = "administrator";
                    context.Roles.Add(role);
                    context.SaveChanges();
                    //Assign Role to user Here (administrator user)
                    await this.UserManager.AddToRoleAsync(user.Id, "administrator");
                    //Ends Here
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                    return RedirectToAction("ViewUsers", "Seed");
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }
        public ActionResult ViewUsers()
        {
            var userList = context.Users
              .Where(x => x.Roles.Select(y => y.UserId).Contains(x.Id))
              .ToList();
            return View(userList);
        }
        //AddErrors (copied from the AccountController)
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
    }
}