﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WEBAAssignmentProject.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace WEBAAssignmentProject.Controllers
{
    public class RoleController : Controller
    {

        ApplicationDbContext context;

        public RoleController()
        {

            context = new ApplicationDbContext();
        }
        // GET: Role
        //[Authorize(Roles = "administrator")]
        public ActionResult Index()
        {
            var roleList = context.Roles.ToList();
            return View(roleList);
        }
        public ActionResult Create()
        {
            var role = new IdentityRole();
            return View(role);
        }//end of Create() action method
        [HttpPost]
        // [Authorize(Roles = "administrator")]
        public ActionResult Create(IdentityRole inRole)
        {
            context.Roles.Add(inRole);
            context.SaveChanges();
            //Tell the web browser to call the RoleController's Index() action method so that
            //the user can see a list of roles (including the newly created role.
            return RedirectToAction("Index");
        }
    }
}