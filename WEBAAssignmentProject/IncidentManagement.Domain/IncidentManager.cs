﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WEBAAssignmentProject.IncidentManagement.Domain
{
    public class IncidentManager
    {
        public List<object> GetAllIncidents()
        {
            DataSet ds = new DataSet();
            //I created 1 base objects, incidentObject
            object incidentObject; ;

            List<object> incidentList = new List<object>();
            string sqlText = "";
            using (SqlConnection cn = new SqlConnection())
            {
                cn.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = cn; //setup the 
                    cn.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        //Setup the the SQL statement which is to be sent to the database
                        sqlText = "SELECT IncidentId, Title, Description, " +
                        " CreatedAt, FullName as 'CreatedBy' " +
                        " FROM Incident INNER JOIN ASPNetUsers ON CreatedBy=AspNetUsers.Id  AND DeletedAt IS NULL ";

                        cmd.CommandText = sqlText;

                        da.Fill(ds, "IncidentData");
                        //The above code will obtain the student record and store it in a DataTable, StudentData.
                        cmd.CommandText = sqlText;
                    }//using SqlDataAdapter da
                    cn.Close();

                }//using SQLCommand cmd
            }//using SQLConnection cn


            foreach (DataRow incidentDataRow in ds.Tables["IncidentData"].Rows)
            {


                DateTime createdAt = Convert.ToDateTime(incidentDataRow["CreatedAt"].ToString());
                string createdAtInString = createdAt.ToString("dd/MM/yyyy HH:mm:ss.fff tt");

                incidentObject = new
                {
                    IncidentId = Int32.Parse(incidentDataRow["IncidentId"].ToString()),
                    Title = incidentDataRow["Title"].ToString(),
                    Description = incidentDataRow["Description"].ToString(),
                    CreatedAt = createdAtInString,
                    CreatedBy = incidentDataRow["CreatedBy"].ToString(),
                };//end of studentObject creation

                incidentList.Add(incidentObject);
            }//foreach


            return incidentList;

        }
        public List<object> GetIncidentsByUserId(string inUserId)
        {
            DataSet ds = new DataSet();
            //I created 1 base objects, incidentObject
            object incidentObject; ;

            List<object> incidentList = new List<object>();
            string sqlText = "";
            using (SqlConnection cn = new SqlConnection())
            {
                cn.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = cn; //setup the 
                    cn.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        //Setup the the SQL statement which is to be sent to the database
                        sqlText = "SELECT IncidentId, Title, Description, " +
                        " CreatedAt, FullName CreatedBy " +
                        " FROM Incident INNER JOIN ASPNetUsers ON CreatedBy=AspNetUsers.Id AND AspNetUsers.Id=@inCurrentUserId AND DeletedAt IS NULL ";

                        cmd.CommandText = sqlText;
                        cmd.Parameters.Add("@inCurrentUserId", SqlDbType.VarChar, 200).Value = inUserId;

                        da.Fill(ds, "IncidentData");
                        //The above code will obtain the student record and store it in a DataTable, StudentData.
                        cmd.CommandText = sqlText;
                    }//using SqlDataAdapter da
                    cn.Close();

                }//using SQLCommand cmd
            }//using SQLConnection cn


            foreach (DataRow incidentDataRow in ds.Tables["IncidentData"].Rows)
            {


                DateTime createdAt = Convert.ToDateTime(incidentDataRow["CreatedAt"].ToString());
                string createdAtInString = createdAt.ToString("dd/MM/yyyy HH:mm:ss.fff tt");

                incidentObject = new
                {
                    IncidentId = Int32.Parse(incidentDataRow["IncidentId"].ToString()),
                    Title = incidentDataRow["Title"].ToString(),
                    Description = incidentDataRow["Description"].ToString(),
                    CreatedAt = createdAtInString,
                    CreatedBy = incidentDataRow["CreatedBy"].ToString(),
                };//end of studentObject creation

                incidentList.Add(incidentObject);
            }//foreach


            return incidentList;

        }
        public int AddNewIncident(string inTitle, string inDescription, string inCreatedBy)
        {
            int newRecordId = 0;
            SqlCommand cmd = new SqlCommand();
            SqlConnection cn = new SqlConnection();
            SqlDataAdapter da = new SqlDataAdapter(); 
           
            cmd.Connection = cn;//tell the cmd to use the cn
            cmd.CommandText = "INSERT INCIDENT (Title,Description,CreatedBy,UpdatedBy ) VALUES" +
                  "(@inTitle,@inDescription,@inCreatedBy,@inUpdatedBy);SELECT SCOPE_IDENTITY();";
            cmd.Parameters.Add("@inTitle", SqlDbType.VarChar, 200).Value = inTitle;
            cmd.Parameters.Add("@inDescription", SqlDbType.VarChar, 2000).Value = inDescription;
            cmd.Parameters.Add("@inCreatedBy", SqlDbType.VarChar, 100).Value = inCreatedBy;
            cmd.Parameters.Add("@inUpdatedBy", SqlDbType.VarChar, 100).Value = inCreatedBy;
            cn.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
            cn.Open();
            try
            {
                newRecordId = Int32.Parse(cmd.ExecuteScalar().ToString());
            }
            catch (SqlException sqlEx)
            {

            }
            cn.Close();


            return newRecordId;
        }
    }
}